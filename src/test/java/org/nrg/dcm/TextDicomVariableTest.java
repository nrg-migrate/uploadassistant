/*
 * org.nrg.dcm.TextDicomVariableTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.dcm;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import javax.swing.event.DocumentEvent;

import org.dcm4che2.data.DicomObject;
import org.junit.Test;
import org.nrg.dcm.edit.Variable;
import org.nrg.upload.data.ValueListener;
import org.nrg.upload.data.SessionVariable.InvalidValueException;

public class TextDicomVariableTest {

	/**
	 * Test method for {@link org.nrg.dcm.TextDicomVariable#setValue(java.lang.String)}.
	 */
	@Test
	public void testSetValue() throws InvalidValueException {
		final String val = "foo";
		final Variable var = mock(Variable.class);
		final TextDicomVariable tdv = new TextDicomVariable(var, mock(DicomObject.class));
		tdv.setValue(val);
		verify(var).setValue(val);
		assertEquals(val, tdv.getEditor().getText());
	}

	/**
	 * Test method for {@link org.nrg.dcm.TextDicomVariable#changedUpdate(javax.swing.event.DocumentEvent)}.
	 */
	@Test
	public void testChangedUpdate() {
		final Variable var = mock(Variable.class);
		final ValueListener listener = mock(ValueListener.class);
		final DocumentEvent docev = mock(DocumentEvent.class);
		final TextDicomVariable tdv = new TextDicomVariable(var, mock(DicomObject.class));
		tdv.addListener(listener);
		tdv.changedUpdate(docev);
		verify(var).setValue(anyString());
		verify(listener).hasChanged(tdv);
	}

	/**
	 * Test method for {@link org.nrg.dcm.TextDicomVariable#insertUpdate(javax.swing.event.DocumentEvent)}.
	 */
	@Test
	public void testInsertUpdate() {
		final Variable var = mock(Variable.class);
		final ValueListener listener = mock(ValueListener.class);
		final DocumentEvent docev = mock(DocumentEvent.class);
		final TextDicomVariable tdv = new TextDicomVariable(var, mock(DicomObject.class));
		tdv.addListener(listener);
		tdv.insertUpdate(docev);
		verify(var).setValue(anyString());
		verify(listener).hasChanged(tdv);
	}

	/**
	 * Test method for {@link org.nrg.dcm.TextDicomVariable#removeUpdate(javax.swing.event.DocumentEvent)}.
	 */
	@Test
	public void testRemoveUpdate() {
		final Variable var = mock(Variable.class);
		final ValueListener listener = mock(ValueListener.class);
		final DocumentEvent docev = mock(DocumentEvent.class);
		final TextDicomVariable tdv = new TextDicomVariable(var, mock(DicomObject.class));
		tdv.addListener(listener);
		tdv.removeUpdate(docev);
		verify(var).setValue(anyString());
		verify(listener).hasChanged(tdv);
	}
}
