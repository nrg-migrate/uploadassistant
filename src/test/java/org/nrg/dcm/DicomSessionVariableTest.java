/*
 * org.nrg.dcm.DicomSessionVariableTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.dcm;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.awt.Component;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.dcm4che2.data.DicomObject;
import org.junit.Test;
import org.nrg.dcm.edit.MultipleInitializationException;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.Value;
import org.nrg.dcm.edit.Variable;
import org.nrg.upload.data.ValueListener;
import org.nrg.upload.data.ValueValidator;
import org.nrg.upload.data.SessionVariable.InvalidValueException;

import com.google.common.collect.ImmutableSet;

public class DicomSessionVariableTest {
	private DicomSessionVariable makeDicomSessionVariable(Variable v, DicomObject o) {
		return new DicomSessionVariable(v, o) {	
			public void refresh() {}		
			public Component getEditor() { return null; }
		};
	}
	
	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#getSessionVariable(org.nrg.dcm.edit.Variable, org.dcm4che2.data.DicomObject)}.
	 */
	@Test
	public void testGetSessionVariable() {
		final String name = "foo";
		final Variable v = mock(Variable.class);
		when(v.getName()).thenReturn(name);
		final DicomSessionVariable dsv = DicomSessionVariable.getSessionVariable(v, mock(DicomObject.class));
		assertEquals(name, dsv.getName());
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#getValue()}.
	 */
	@Test
	public void testGetValue() throws InvalidValueException {
		final String value = "foo";
		final Variable v = mock(Variable.class);
		when(v.getValue()).thenReturn(value);
		
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertEquals(value, tdv.getValue());
	}
	
	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#getValue()}.
	 */
	@Test
	public void testGetNoValue() throws InvalidValueException {
		final Variable v = mock(Variable.class);
		when(v.getValue()).thenReturn(null);
		when(v.getInitialValue()).thenReturn(null);
		
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertNull(tdv.getValue());
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#getValue()}.
	 * @throws ScriptEvaluationException 
	 */
	@Test
	public void testGetValueFromInitialValue() throws InvalidValueException,ScriptEvaluationException {
		final String value = "foo";
		final Variable v = mock(Variable.class);
		final Value iv = mock(Value.class);
		final DicomObject dobj = mock(DicomObject.class);
		when(v.getValue()).thenReturn(null);
		when(v.getInitialValue()).thenReturn(iv);
		when(iv.on(dobj)).thenReturn(value);
		
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, dobj);
		assertEquals(value, tdv.getValue());
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#editTo(java.lang.String)}.
	 */
	@Test
	public void testEditTo() {
		final String newValue = "yak";
		final ValueListener listener = mock(ValueListener.class);
		final Variable v = mock(Variable.class);
		
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		tdv.addListener(listener);
		tdv.editTo(newValue);
		verify(v).setValue(newValue);
		verify(listener).hasChanged(tdv);
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#editTo(java.lang.String)}.
	 */
	@Test
	public void testEditToInvalid() {
		final String bad = "bad";
		final ValueListener listener = mock(ValueListener.class);
		final ValueValidator validator = mock(ValueValidator.class);
		final Variable variable = mock(Variable.class);
		when(validator.isValid(bad)).thenReturn(false);
		
		final DicomSessionVariable tdv = makeDicomSessionVariable(variable, mock(DicomObject.class));
		tdv.addValidator(validator);
		tdv.addListener(listener);
		tdv.editTo(bad);
		verify(listener).isInvalid(eq(tdv), eq(variable), anyString());
		verify(variable, never()).setValue(anyString());
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#isHidden()}.
	 */
	@Test
	public void testIsHiddenFalse() {
		final Variable v = mock(Variable.class);
		when(v.isHidden()).thenReturn(false);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertFalse(tdv.isHidden());
		verify(v).isHidden();
	}
	
	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#isHidden()}.
	 */
	@Test
	public void testIsHiddenTrue() {
		final Variable v = mock(Variable.class);
		when(v.isHidden()).thenReturn(true);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertTrue(tdv.isHidden());
		verify(v).isHidden();
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#setIsHidden(boolean)}.
	 */
	@Test
	public void testSetIsHidden() {
		final Variable v = mock(Variable.class);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		tdv.setIsHidden(true);
		verify(v).setIsHidden(true);
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#setValue(java.lang.String)}.
	 * @throws InvalidValueException 
	 */
	@Test
	public void testSetValue() throws InvalidValueException {
		final String oldValue = "foo";
		final String newValue = "bar";
		final Variable v = mock(Variable.class);
		when(v.getValue()).thenReturn(oldValue);
		
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertEquals(oldValue, tdv.setValue(newValue));
		verify(v).setValue(newValue);
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#setInitialValue(org.nrg.dcm.edit.Value)}.
	 */
	@Test
	public void testSetInitialValue() throws MultipleInitializationException {
		final Value initial = mock(Value.class);
		final Variable v = mock(Variable.class);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		tdv.setInitialValue(initial);
		verify(v).setInitialValue(initial);
	}
	
	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#setInitialValue(org.nrg.dcm.edit.Value)}.
	 * @throws MultipleInitializationException 
	 */
	@Test(expected = MultipleInitializationException.class)
	public void testSetInitialValueMultiple() throws MultipleInitializationException {
		final Value initial = mock(Value.class);
		final Variable v = mock(Variable.class);
		doThrow(new MultipleInitializationException(v, initial)).when(v).setInitialValue(initial);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		tdv.setInitialValue(initial);
	}


	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#getTags()}.
	 */
	@Test
	public void testGetTagsNoInitialValue() {
		final Variable v = mock(Variable.class);
		when(v.getInitialValue()).thenReturn(null);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertTrue(tdv.getTags().isEmpty());
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#getTags()}.
	 */
	@Test
	public void testGetTagsWithInitialValue() {
		final SortedSet<Long> tags = new TreeSet<Long>(ImmutableSet.of(1L, 2L, 3L, 4L));
		final Variable v = mock(Variable.class);
		final Value iv = mock(Value.class);
		when(v.getInitialValue()).thenReturn(iv);
		when(iv.getTags()).thenReturn(tags);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertEquals(tags, tdv.getTags());
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#getVariables()}.
	 */
	@Test
	public void testGetVariablesNoInitialValue() {
		final Variable v = mock(Variable.class);
		when(v.getInitialValue()).thenReturn(null);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertTrue(tdv.getVariables().isEmpty());
	}

	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#getVariables()}.
	 */
	@Test
	public void testGetVariablesWithInitialValue() {
		final Set variables = ImmutableSet.of("some", "arbitrary", "objects", 4);
		final Variable v = mock(Variable.class);
		final Value iv = mock(Value.class);
		when(v.getInitialValue()).thenReturn(iv);
		when(iv.getVariables()).thenReturn(variables);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		assertEquals(variables, tdv.getVariables());
	}


	/**
	 * Test method for {@link org.nrg.dcm.DicomSessionVariable#hasChanged(org.nrg.upload.data.SessionVariable)}.
	 */
	@Test
	public void testHasChanged() {
		final ValueListener listener = mock(ValueListener.class);
		final Variable v = mock(Variable.class);
		final DicomSessionVariable tdv = makeDicomSessionVariable(v, mock(DicomObject.class));
		tdv.addListener(listener);
		tdv.hasChanged(tdv);
		verify(listener).hasChanged(tdv);
	}
}
