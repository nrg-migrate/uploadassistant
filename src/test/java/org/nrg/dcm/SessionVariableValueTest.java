/*
 * org.nrg.dcm.SessionVariableValueTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.dcm;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Map;
import java.util.Set;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.junit.Test;
import org.nrg.dcm.edit.Variable;
import org.nrg.upload.data.SessionVariable;

import com.google.common.collect.ImmutableSet;

public class SessionVariableValueTest {
	/**
	 * Test method for {@link org.nrg.dcm.SessionVariableValue#getTags()}.
	 */
	@Test
	public void testGetTags() {
		final SessionVariable var = mock(SessionVariable.class);
		final SessionVariableValue val = new SessionVariableValue(var);
		assertTrue(val.getTags().isEmpty());
	}
	
	/**
	 * Test method for {@link org.nrg.dcm.SessionVariableValue#getTags()}.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testGetTagsDicomSessionVariable() {
		final Set tags = ImmutableSet.of(Tag.SOPClassUID, Tag.AccessionNumber);
		final Variable v1 = mock(Variable.class);
		final Set vars = ImmutableSet.of(v1);
		final DicomSessionVariable dicomVar = mock(DicomSessionVariable.class);
		when(dicomVar.getTags()).thenReturn(tags);
		when(dicomVar.getVariables()).thenReturn(vars);
		
		final SessionVariableValue svv = new SessionVariableValue(dicomVar);
		assertEquals(tags, svv.getTags());
	}

	/**
	 * Test method for {@link org.nrg.dcm.SessionVariableValue#getVariables()}.
	 */
	@Test
	public void testGetVariables() {
		final SessionVariable var = mock(SessionVariable.class);
		final SessionVariableValue val = new SessionVariableValue(var);
		assertTrue(val.getVariables().isEmpty());
	}

	/**
	 * Test method for {@link org.nrg.dcm.SessionVariableValue#getVariables()}.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testGetVariablesDicomSessionVariable() {
		final Set tags = ImmutableSet.of(Tag.SOPClassUID, Tag.AccessionNumber);
		final Variable v1 = mock(Variable.class);
		final Set vars = ImmutableSet.of(v1);
		final DicomSessionVariable dicomVar = mock(DicomSessionVariable.class);
		when(dicomVar.getTags()).thenReturn(tags);
		when(dicomVar.getVariables()).thenReturn(vars);
		
		final SessionVariableValue svv = new SessionVariableValue(dicomVar);
		assertEquals(vars, svv.getVariables());
	}

	/**
	 * Test method for {@link org.nrg.dcm.SessionVariableValue#on(org.dcm4che2.data.DicomObject)}.
	 */
	@Test
	public void testOnDicomObject() {
		final String testVal = "foo";
		final SessionVariable var = mock(SessionVariable.class);
		when(var.getValue()).thenReturn(testVal);
		final SessionVariableValue svv = new SessionVariableValue(var);
		final DicomObject dobj = mock(DicomObject.class);
		assertEquals(testVal, svv.on(dobj));
	}

	/**
	 * Test method for {@link org.nrg.dcm.SessionVariableValue#on(java.util.Map)}.
	 */
	@Test
	public void testOnMap() {
		final String testVal = "bar";
		final SessionVariable var = mock(SessionVariable.class);
		when(var.getValue()).thenReturn(testVal);
		final SessionVariableValue svv = new SessionVariableValue(var);
		final Map<?,?> m = mock(Map.class);
		assertEquals(testVal, svv.on(m));
	}
}
