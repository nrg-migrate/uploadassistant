/*
 * org.nrg.io.UploadStatisticsReporterTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.io;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.netbeans.spi.wizard.ResultProgressHandle;

public class UploadStatisticsReporterTest {
    /**
     * Test method for {@link org.nrg.io.UploadStatisticsReporter#addToSend(long)}.
     */
    @Test
    public void testAddToSend() {
        final ResultProgressHandle progress = mock(ResultProgressHandle.class);
        final UploadStatisticsReporter reporter = new UploadStatisticsReporter(progress);
        reporter.addToSend(5);
        reporter.addToSend(1024);
        reporter.addToSend(Integer.MAX_VALUE);
        reporter.addToSend(1024L * Integer.MAX_VALUE);
        verify(progress, times(4)).setBusy(matches("Preparing\\.\\.\\..*"));
    }

    /**
     * Test method for {@link org.nrg.io.UploadStatisticsReporter#addSent(long)}.
     */
    @Test
    public void testAddSent() {
        final ResultProgressHandle progress = mock(ResultProgressHandle.class);
        final UploadStatisticsReporter reporter = new UploadStatisticsReporter(progress);
        reporter.addToSend(2048);
        verify(progress).setBusy("Preparing...2.00 kB");
        reporter.addSent(1);
        reporter.addSent(100);
        verify(progress, times(2)).setProgress(anyString(), eq(0), eq(2));
        reporter.addSent(923);
        verify(progress).setProgress(anyString(), eq(1), eq(2));
        reporter.addSent(1025);
        verify(progress).setBusy("Preparing...2.00 kB");
    }

    @Test
    public void testAddSentHuge() {
        final ResultProgressHandle progress = mock(ResultProgressHandle.class);
        final UploadStatisticsReporter reporter = new UploadStatisticsReporter(progress);
        reporter.addToSend(1025L * Integer.MAX_VALUE);
        verify(progress).setBusy("Preparing...2.00 TB");
        reporter.addSent(1);
        verify(progress).setBusy("1.00 B/2.00 TB ");
    }
    
    /**
     * Test method for {@link org.nrg.io.UploadStatisticsReporter#toString()}.
     */
    @Test
    public void testToString() {
        final ResultProgressHandle progress = mock(ResultProgressHandle.class);
        final UploadStatisticsReporter reporter = new UploadStatisticsReporter(progress);
        reporter.addToSend(1024 * 1024);
        assertEquals("Preparing...1.00 Mb".toLowerCase(), reporter.toString().toLowerCase().trim());
        reporter.addSent(1);
        assertEquals("1.00 b/1.00 Mb".toLowerCase(), reporter.toString().toLowerCase().trim());
        reporter.addSent(1023);
        assertEquals("1.00 kb/1.00 Mb".toLowerCase(), reporter.toString().toLowerCase().trim());
    }
}
