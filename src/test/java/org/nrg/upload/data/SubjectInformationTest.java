/*
 * org.nrg.upload.data.SubjectInformationTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */

package org.nrg.upload.data;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.nrg.net.HttpURLConnectionProcessor;
import org.nrg.net.RestServer;
import org.nrg.net.StringResponseProcessor;
import org.w3c.dom.Document;

public class SubjectInformationTest {

	// private final Logger logger =
	// LoggerFactory.getLogger(SubjectInformationTest.class);
	private static final String EXPECTED_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
			+ "<xnat:Subject label=\"SUBJ01_MR1\" project=\"PROJ01\" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"/>";

	private SubjectInformation info;
	private Project project;
	private RestServer xnat;

	@Before
	public void setUp() throws Exception {
		project = new Project("PROJ01", null, null);

		xnat = mock(RestServer.class);

		info = new SubjectInformation(xnat, project);
		info.setLabel("SUBJ01_MR1");
	}

	@Test
	public void shouldCreateXML() throws Exception {
		String xml = xmlToString(info.buildXML());
		assertEquals(EXPECTED_XML, xml);
	}

	@Test
	public void shouldCreateSubjectOnServerAndReturnSubject() throws Exception {

		mockHttpResponse("http://localhost:8080/xnat/REST/projects/PROJ01/subjects/Demo_S00003", EXPECTED_XML).doPost(
				eq("/REST/projects/PROJ01/subjects"), any(HttpURLConnectionProcessor.class));

		Subject subject = info.uploadTo();

		// subject checks
		assertEquals("SUBJ01_MR1", subject.getLabel());
		assertEquals("Demo_S00003", subject.getId());
	}

	@Test(expected=SubjectInformation.UploadSubjectException.class)
	public void shouldWrapException() throws Exception {
		doThrow(new IOException()).when(xnat).doPost(anyString(), any(HttpURLConnectionProcessor.class));
		
		info.uploadTo();
	}

	private String xmlToString(Document document) throws Exception {
		StringWriter writer = new StringWriter();

		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(writer);
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			throw e;
		} catch (TransformerException e) {
			throw e;
		}
		return writer.toString();
	}

	/**
	 * Using the mock RestServer, we inject "response" into the Response
	 * Processor.
	 */
	private RestServer mockHttpResponse(final String response, final String expectedRequest) throws IOException {
		return doAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) throws IOException {
				// create a mock connection that provides an InputStream seeded
				// with the "response"
				HttpURLConnection connection = mock(HttpURLConnection.class);
				when(connection.getInputStream()).thenReturn(new ByteArrayInputStream(response.getBytes()));
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				when(connection.getOutputStream()).thenReturn(out);

				StringResponseProcessor processor = (StringResponseProcessor) invocation.getArguments()[1];
				processor.prepare(connection);
				processor.process(connection);

				// ensure the data sent to the server matches what we expect
				// TODO consider moving the assert out of the mocking code
				if (expectedRequest != null) {
					assertEquals(expectedRequest, out.toString());
				}

				return null;
			}
		}).when(xnat);
	}
}
