/*
 * org.nrg.dcm.DicomSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.dcm;


import java.util.Collections;
import java.util.Set;

import javax.swing.JOptionPane;

import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.edit.MultipleInitializationException;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.Value;
import org.nrg.dcm.edit.Variable;
import org.nrg.upload.data.AbstractSessionVariable;
import org.nrg.upload.data.SessionVariable;
import org.nrg.upload.data.ValueListener;

import com.google.common.base.Strings;

public abstract class DicomSessionVariable
        extends AbstractSessionVariable implements SessionVariable, ValueListener {
    private final Variable v;
    private final DicomObject sample;
    private String message = null;

    public static DicomSessionVariable getSessionVariable(final Variable v, final DicomObject sample) {
        return new TextDicomVariable(v, sample);
    }

    DicomSessionVariable(final Variable v, final DicomObject sample) {
        super(v.getName());
        this.sample = sample;
        this.v = v;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#getDescription()
     */
    public String getDescription() {
        final String description = v.getDescription();
        return Strings.isNullOrEmpty(description) ? v.getName() : description;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#getExportField()
     */
    public String getExportField() {
        return v.getExportField();
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#getValue()
     */
    public String getValue() {
        final String value = v.getValue();
        if (null == value) {
            final Value iv = v.getInitialValue();
            if (null == iv) {
                return null;
            } else {
                try {
                    return iv.on(sample);
                } catch (ScriptEvaluationException e) {
                    //non-ideal, but somewhat protects against HTTP issues in GetURL
                    JOptionPane.showMessageDialog(null, "An serious error was encountered while preparing the DICOM.\nPlease contact your site administrator with the following error\nmessage before uploading this data, as it could contain incorrect data.\n\n" + e.getMessage(), "DICOM Script Error", JOptionPane.ERROR_MESSAGE);

                    return null;
                }
            }
        } else {
            return value;
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#getValueMessage()
     */
    public String getValueMessage() {
        return message;
    }

    /**
     * Gets the underlying DicomEdit script variable.
     *
     * @return script variable
     */
    public Variable getVariable() {
        return v;
    }

    protected void editTo(final String value) {
        try {
            message = validate(value);
            v.setValue(value);
            fireHasChanged();
        } catch (InvalidValueException e) {
            fireIsInvalid(v, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#isHidden()
     */
    public boolean isHidden() {
        return v.isHidden();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.AbstractSessionVariable#setDescription(java.lang.String)
     */
    @Override
    public String setDescription(final String description) {
        // Set the description only if it's not set in the source variable.
        if (Strings.isNullOrEmpty(v.getDescription())) {
            return super.setDescription(description);
        } else {
            return v.getDescription();
        }
    }

    public void setIsHidden(final boolean isHidden) {
        v.setIsHidden(isHidden);
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#setValue(java.lang.String)
     */
    public String setValue(final String value) throws InvalidValueException {
        message = validate(value);
        final String old = v.getValue();
        v.setValue(value);
        return old;
    }

    public void setInitialValue(final Value value) throws MultipleInitializationException {
        v.setInitialValue(value);
    }

    public Set<?> getTags() {
        final Value iv = v.getInitialValue();
        return null == iv ? Collections.emptySet() : iv.getTags();
    }

    public Set<?> getVariables() {
        final Value iv = v.getInitialValue();
        return null == iv ? Collections.emptySet() : iv.getVariables();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.ValueListener#hasChanged(org.nrg.upload.data.SessionVariable)
     */
    public void hasChanged(final SessionVariable variable) {
        fireHasChanged();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.ValueListener#isInvalid(org.nrg.upload.data.SessionVariable, java.lang.Object, java.lang.String)
     */
    public void isInvalid(SessionVariable variable, Object value, String message) {
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return super.toString() + "/" + v + " " + getName() + " = " + getValue() + " [init " + v.getInitialValue() + "]";
    }
}
