/*
 * org.nrg.dcm.Study
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.dcm;

import com.google.common.base.*;
import com.google.common.collect.*;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.json.JSONException;
import org.json.JSONObject;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.Summary;
import org.nrg.dcm.edit.DicomUtils;
import org.nrg.dcm.edit.ScriptApplicator;
import org.nrg.dcm.edit.Variable;
import org.nrg.io.HttpUploadException;
import org.nrg.io.UploadStatisticsReporter;
import org.nrg.io.dcm.ZipSeriesUploader;
import org.nrg.net.HttpException;
import org.nrg.net.JSONRequestConnectionProcessor;
import org.nrg.net.RestServer;
import org.nrg.upload.data.*;
import org.nrg.upload.ui.AssignSessionVariablesPage;
import org.nrg.upload.ui.SelectProjectPage;
import org.nrg.upload.ui.SelectSubjectPage;
import org.nrg.upload.ui.UploadResultPanel;
import org.nrg.util.AutoArchive;
import org.nrg.util.MapRegistry;
import org.nrg.util.Registry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ChoiceFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

import static org.nrg.upload.ui.Constants.*;

public class Study extends MapEntity implements Entity, Session {

    public static final int MAX_TAG = Collections.max(new ArrayList<Integer>() {{
        add(Tag.AccessionNumber);
        add(Tag.StudyDate);
        add(Tag.StudyDescription);
        add(Tag.StudyID);
        add(Tag.StudyInstanceUID);
        add(Tag.StudyTime);
    }});

    public static final String PREVENT_ANON = "prevent_anon";
    public static final String PREVENT_AUTO_COMMIT = "prevent_auto_commit";
    public static final String SOURCE = "SOURCE";

    private final Logger logger = LoggerFactory.getLogger(Study.class);
    private final Registry<Series> series = new MapRegistry<>(new TreeMap<Series, Series>());
    private final Date dateTime;

    public Study(final String uid, final Date dateTime, final String id, final String accessionNumber, final String description) {
        put(Tag.StudyInstanceUID, uid);
        this.dateTime = dateTime;
        if (null != dateTime) {
            put(Tag.StudyDate, new SimpleDateFormat("yyyyMMdd").format(dateTime));
            put(Tag.StudyTime, new SimpleDateFormat("HHmmss").format(dateTime));
        }
        put(Tag.StudyID, id);
        put(Tag.AccessionNumber, accessionNumber);
        put(Tag.StudyDescription, description);
    }

    public Study(final DicomObject o) {
        this(o.getString(Tag.StudyInstanceUID),
                DicomUtils.getDateTime(o, Tag.StudyDate, Tag.StudyTime),
                o.getString(Tag.StudyID),
                o.getString(Tag.AccessionNumber),
                o.getString(Tag.StudyDescription));
    }

    public Study(final DicomObject o, final String suggestedModality) {
        this(o.getString(Tag.StudyInstanceUID),
                DicomUtils.getDateTime(o, Tag.StudyDate, Tag.StudyTime),
                o.getString(Tag.StudyID),
                o.getString(Tag.AccessionNumber),
                o.getString(Tag.StudyDescription));
        put(Tag.Modality, suggestedModality);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getAccession()
     */
    public String getAccession() {
        return (String) get(Tag.AccessionNumber);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getDateTime()
     */
    public Date getDateTime() {
        return dateTime;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getTimeZone()
     */
    public TimeZone getTimeZone() {
        // DICOM does not store timezone information so return null
        return null;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getDescription()
     */
    public String getDescription() {
        return (String) get(Tag.StudyDescription);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getFileCount()
     */
    public int getFileCount() {
        int count = 0;
        for (final Series s : series) {
            count += s.getFileCount();
        }
        return count;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getFormat()
     */
    public String getFormat() {
        return "DICOM";
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getID()
     */
    public String getID() {
        return (String) get(Tag.StudyID);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getModalities()
     */
    public Set<String> getModalities() {
        final Set<String> modalities = Sets.newLinkedHashSet();
        for (final Series s : series) {
            modalities.addAll(s.getModalities());
        }
        return modalities;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getScanCount()
     */
    public int getScanCount() {
        return series.size();
    }

    public Series getSeries(final DicomObject o, final File f) {
        final Series s = series.get(new Series(this, o));
        s.addFile(f, o);
        return s;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.Entity#getSeries()
     */
    public Collection<Series> getSeries() {
        return series.getAll();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getSize()
     */
    public long getSize() {
        long size = 0;
        for (final Series s : series) {
            size += s.getSize();
        }
        return size;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.Entity#getStudies()
     */
    public Collection<Study> getStudies() {
        return Collections.singleton(this);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Study)) {
            return false;
        }
        if (o == this) {
            return true;
        }

        final Study that = (Study) o;
        final Object thisStudyInstanceUid = get(Tag.StudyInstanceUID);
        final Object thatStudyInstanceUid = that.get(Tag.StudyInstanceUID);

        if (!com.google.common.base.Objects.equal(thisStudyInstanceUid, thatStudyInstanceUid)) {
            return false;
        }

        final Object thisModality = get(Tag.Modality);
        if (thisModality != null) {
            final Object thatModality = that.get(Tag.Modality);
            return com.google.common.base.Objects.equal(thisModality, thatModality);
        } else {
            return true;
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.MapEntity#hashCode()
     */
    @Override
    public int hashCode() {
        final Object modality = get(Tag.Modality);
        return modality == null
                ? com.google.common.base.Objects.hashCode(get(Tag.StudyInstanceUID))
                : com.google.common.base.Objects.hashCode(get(Tag.StudyInstanceUID), get(Tag.Modality));
    }

    /**
     * Provides a study identifier that is as unique and verbose as possible.
     *
     * @return The study identifier.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("DICOM ");
        final Object modality = get(Tag.Modality);
        if (modality != null) {
            builder.append(modality).append(" ");
        }
        builder.append("study ");
        final Object studyId = get(Tag.StudyID);
        builder.append(studyId);
        final Object accessionNumber = get(Tag.AccessionNumber);
        if (null != accessionNumber) {
            builder.append(" (").append(accessionNumber).append(")");
        }
        final Object description = get(Tag.StudyDescription);
        if (null != description) {
            builder.append(" ").append(description);
        }
        if (null == studyId && null == accessionNumber) {
            builder.append(" [").append(get(Tag.StudyInstanceUID)).append("]");
        }
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#uploadTo(java.util.Map, org.nrg.upload.data.UploadFailureHandler, org.netbeans.spi.wizard.ResultProgressHandle)
     */
    public boolean uploadTo(final Map<?, ?> params, final UploadFailureHandler failureHandler, final ResultProgressHandle progress) {
        final long uploadStart = new Date().getTime();
        try {
            int fileCount = getFileCount();
            progress.setProgress(0, fileCount);
            progress.setBusy("Building session manifest");

            final List<Series> uploads = Lists.newArrayList(Iterables.filter(series, new Predicate<Series>() {
                public boolean apply(final Series s) {
                    return s.isUploadAllowed();
                }
            }));
            if (uploads.isEmpty()) {
                progress.failed("No files were selected for upload", true);
                return false;
            }

            final Project project = (Project) params.get(SelectProjectPage.PRODUCT_NAME);
            final Subject subject = (Subject) params.get(SelectSubjectPage.PRODUCT_NAME);
            final String session = ((SessionVariable) params.get(SessionVariableNames.SESSION_LABEL)).getValue();
            String visit = null;
            if (params.get(SessionVariableNames.VISIT_LABEL) != null) {
                visit = ((SessionVariable) params.get(SessionVariableNames.VISIT_LABEL)).getValue();
            }

            String protocol = null;
            if (params.get(SessionVariableNames.PROTOCOL_LABEL) != null) {
                protocol = ((SessionVariable) params.get(SessionVariableNames.PROTOCOL_LABEL)).getValue();
            }

            // See if we're applying any metadata modification scripts. If so, any parameters
            // we're sending in the manifest should be the post-modification values.
            final Iterable<ScriptApplicator> applicators;
            try {
                applicators = project.getDicomScriptApplicators();
            } catch (RuntimeException exception) {
                logger.error("problem retrieving script applicator", exception);
                // Analytics.enter(UploadAssistant.class, "Error retrieving script applicator", exception);
                throw exception;    // TODO: better handling;
            } catch (Exception exception) {
                logger.error("problem retrieving script applicator", exception);
                // Analytics.enter(UploadAssistant.class, "Error retrieving script applicator", exception);
                throw new RuntimeException(exception);  // TODO: better handling
            }

            // Build the URL for POSTing zips
            final String adminEmail = (String) params.get(XNAT_ADMIN_EMAIL_WIZ_PARAM);
            final URL baseURL = (URL) params.get(XNAT_URL_WIZ_PARAM);
            final URL dataPostURL;
            try {
                final StringBuilder buffer = new StringBuilder(baseURL.toString());
                buffer.append("/REST/services/import?import-handler=DICOM-zip");
                buffer.append("&PROJECT_ID=").append(project);
                buffer.append("&SUBJECT_ID=").append(subject);
                buffer.append("&EXPT_LABEL=").append(session);
                if (!Strings.isNullOrEmpty(visit)) {
                    buffer.append("&VISIT=").append(visit);
                }
                if (!Strings.isNullOrEmpty(protocol)) {
                    buffer.append("&PROTOCOL=").append(protocol);
                }
                buffer.append("&rename=true&" + PREVENT_ANON + "=true&" + PREVENT_AUTO_COMMIT + "=true&" + SOURCE + "=applet");
                if (params.containsKey(Project.AUTO_ARCHIVE)) {
                    AutoArchive autoArchive = (AutoArchive) params.get(Project.AUTO_ARCHIVE);
                    buffer.append("&").append(Project.AUTO_ARCHIVE).append("=").append(autoArchive);
                }
                dataPostURL = new URL(buffer.toString());
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
            final boolean useFixedSize = (Boolean) params.get(FIXED_SIZE_STREAMING_WIZ_PARAM);
            final int nThreads = (Integer) params.get(N_UPLOAD_THREADS_WIZ_PARAM);

            // enterUploadAnalytics(project, subject, session, fileCount, dataPostURL, useFixedSize, nThreads);

            progress.setBusy("Preparing upload...");
            logger.trace("creating thread pool and executors");
            final ExecutorService executor = Executors.newFixedThreadPool(nThreads);
            final CompletionService<Set<String>> completionService = new ExecutorCompletionService<>(executor);
            final Map<Future<Set<String>>, ZipSeriesUploader> uploaders = Maps.newHashMap();
            logger.trace("submitting uploaders for {}", uploads);
            final UploadStatisticsReporter stats = new UploadStatisticsReporter(progress);
            for (final Series s : uploads) {
                stats.addToSend(s.getSize());
                final ZipSeriesUploader uploader = new ZipSeriesUploader(dataPostURL, useFixedSize, s, applicators, stats,
                        (RestServer) params.get(XNAT_REST_API_WIZ_PARAM));
                uploaders.put(completionService.submit(uploader), uploader);
            }

            final Set<String> uris = Sets.newLinkedHashSet();
            final Map<Series, Throwable> failures = Maps.newLinkedHashMap();
            while (progress.isRunning() && !uploaders.isEmpty()) {
                // Analytics.enter(UploadAssistant.class, "Upload operation commenced");
                final Future<Set<String>> future;
                try {
                    future = completionService.take();
                    logger.trace("retrieved completed future {}", future);
                } catch (InterruptedException e) {
                    logger.debug("upload completion poll interrupted", e);
                    // Analytics.enter(UploadAssistant.class, Level.DEBUG, "upload completion poll interrupted");
                    continue;
                }
                try {
                    final Set<String> us = future.get();
                    logger.debug("{} completed -> {}", uploaders.get(future), us);
                    uris.addAll(us);
                    uploaders.remove(future);
                } catch (InterruptedException e) {
                    logger.info("upload interrupted or timed out, retrying");
                    completionService.submit(uploaders.get(future));
                } catch (ExecutionException exception) {
                    executor.shutdownNow();
                    final Throwable cause = exception.getCause();
                    //noinspection ThrowableResultOfMethodCallIgnored
                    failures.put(uploaders.remove(future).getSeries(), cause);
                    future.cancel(true);
                    final UploadAbortedException aborted;
                    if (cause instanceof UploadAbortedException) {
                        aborted = (UploadAbortedException) cause;
                    } else {
                        aborted = new UploadAbortedException(cause);
                    }
                    logger.info("upload aborted: shutting down executor", cause);
                    // Analytics.enter(UploadAssistant.class, Level.WARN, "upload aborted: shutting down executor", cause);

                    for (final Map.Entry<Future<Set<String>>, ZipSeriesUploader> me : uploaders.entrySet()) {
                        me.getKey().cancel(true);
                        //noinspection ThrowableResultOfMethodCallIgnored
                        failures.put(me.getValue().getSeries(), aborted);
                    }
                    String message = buildFailureMessage(failures);
                    progress.failed(message, false);
                    // Analytics.enter(UploadAssistant.class, Level.WARN, message, exception);
                    return false;
                }
            }

            if (!uploaders.isEmpty()) {
                logger.error("progress failed before uploaders complete: {}", uploaders);
                // Analytics.enter(UploadAssistant.class, Level.ERROR, String.format("Progress failed before uploaders complete: %s", uploaders));
                return false;
            } else if (1 == uris.size()) {
                final String uri = uris.iterator().next();
                try {
                    final URL url = new URL(baseURL.toString() + uri);
                    return closeSession(url, params, progress, failures);
                } catch (MalformedURLException e) {
                    progress.failed("<p>The XNAT server provided an invalid response</p>" + "<p>" + uri + "</p>" + "<p>Please contact the system manager (" + adminEmail + ") for help.</p>", false);
                    return false;
                }
            } else {
                logger.error("Server reports unexpected session count {}: {}", uris.size(), uris);
                progress.failed("<p>The XNAT server reported receiving an unexpected number of sessions: (" + uris.size() + ")</p>" + "<p>Please contact the system manager (" + adminEmail + ") for help.</p>", false);
                // Analytics.enter(UploadAssistant.class, Level.ERROR, sb.toString());
                return false;
            }
        } finally {
            long duration = (new Date().getTime() - uploadStart) / 1000;
            logger.info("upload operation complete after {} sec", duration);
            // Analytics.enter(UploadAssistant.class, "Upload operation completed after " + duration + " seconds");
        }
    }

    //    private void enterUploadAnalytics(Project project, Subject subject, String session, int fileCount, URL dataPostURL, boolean useFixedSize, int nThreads) {
    //        Map<String, String> map = new HashMap<String, String>();
    //        map.put("project", project.toString());
    //        map.put("subject", subject.toString());
    //        map.put("session", session);
    //        map.put("fileCount", Integer.toString(fileCount));
    //        map.put("url", dataPostURL.toString());
    //        map.put("useFixedSize", Boolean.toString(useFixedSize));
    //        map.put("nThreads", Integer.toString(nThreads));
    //        Analytics.enter(UploadAssistant.class, map);
    //    }

    private JSONObject buildCommitEntity(final Map<?, ?> params) {
        final JSONObject entity = new JSONObject();
        final Collection<?> vars = (Collection<?>) params.get(AssignSessionVariablesPage.PRODUCT_NAME);
        if (null == vars) {
            logger.error("session variables not assigned in {}", params);
        } else {
            for (final Object o : vars) {
                if (o instanceof SessionVariable) {
                    final SessionVariable v = (SessionVariable) o;
                    final String path = v.getExportField();
                    if (!Strings.isNullOrEmpty(path)) {
                        try {
                            entity.put(path, v.getValue());
                        } catch (JSONException exception) {
                            String message = "unable to assign session variable " + path;
                            logger.error(message, exception);
                            // Analytics.enter(UploadAssistant.class, Level.ERROR, message, exception);
                        }
                    }
                }
            }
        }
        logger.trace("Built commit entity: {}", entity);
        // Analytics.enter(UploadAssistant.class, String.format("Built commit entity: %s", entity));
        return entity;
    }

    /**
     * The RestServer URL includes the web application part of the path.
     * If the given path starts with the web application path, returns the path
     * minus the web application context; otherwise return the full path. Either
     * way, any leading /'s are removed.
     *
     * @param url  The URL to be inspected.
     * @param path The relative path to validate.
     * @return The relative path to the REST server URL, stripped of leading slashes.
     */
    public static String getWebAppRelativePath(final URL url, final String path) {
        final StringBuilder buffer = new StringBuilder(path);
        while ('/' == buffer.charAt(0)) {
            buffer.deleteCharAt(0);
        }
        final String context = url.getPath();
        boolean pathHasContext = true;
        for (int i = 0; i < context.length(); i++) {
            if (context.charAt(i) != path.charAt(i)) {
                pathHasContext = false;
                break;
            }
        }
        if (pathHasContext) {
            buffer.delete(0, context.length());
        }
        while ('/' == buffer.charAt(0)) {
            buffer.deleteCharAt(0);
        }
        return buffer.toString();
    }

    public static URL buildSessionViewURL(final URL url, final String relativePath) {
        final String[] components = relativePath.split("/");
        if (!"data".equals(components[0]) && !"REST".equals(components[0])) {
            LoggerFactory.getLogger(Study.class).warn("Strange session path {}: first component is neither \"data\" nor \"REST\"", relativePath);
        }
        if ("prearchive".equals(components[1])) {
            // prearchive sessions need some extra help for nice display
            try {
                return new URL(url.toString() + "?screen=XDATScreen_uploaded_xnat_imageSessionData.vm");
            } catch (MalformedURLException e) {
                LoggerFactory.getLogger(Study.class).error("can't build prearchive session view url for " + url, e);
                return url;
            }
        } else {
            // archived sessions are viewable using REST url
            return url;
        }
    }

    private boolean closeSession(final URL url, final Map<?, ?> params, final ResultProgressHandle progress, final Map<?, ?> failures) {
        final String adminEmail = (String) params.get(XNAT_ADMIN_EMAIL_WIZ_PARAM);
        final String session = ((SessionVariable) params.get(SessionVariableNames.SESSION_LABEL)).getValue();

        // Close session and return result
        try {
            if (failures.isEmpty()) {
                progress.setBusy("Committing session");
                logger.trace("committing session {}", url);
                final RestServer xnat = (RestServer) params.get(XNAT_REST_API_WIZ_PARAM);
                final JSONRequestConnectionProcessor handler = new JSONRequestConnectionProcessor(buildCommitEntity(params));

                String queryParams = "?action=commit&SOURCE=applet";
                //add visit
                if (null != params.get(SessionVariableNames.VISIT_LABEL) && !Strings.isNullOrEmpty(((AssignedSessionVariable) params.get(SessionVariableNames.VISIT_LABEL)).getValue())) {
                    queryParams += "&VISIT=" + ((AssignedSessionVariable) params.get(SessionVariableNames.VISIT_LABEL)).getValue();
                }
                //add protocol
                if (null != params.get(SessionVariableNames.PROTOCOL_LABEL) && !Strings.isNullOrEmpty(((AssignedSessionVariable) params.get(SessionVariableNames.PROTOCOL_LABEL)).getValue())) {
                    queryParams += "&PROTOCOL=" + ((AssignedSessionVariable) params.get(SessionVariableNames.PROTOCOL_LABEL)).getValue();
                }
                xnat.doPost(getWebAppRelativePath(xnat.getURL(), url.getPath()) + queryParams, handler);

                String response = handler.getResponseEntity();
                String resultPath = getWebAppRelativePath(xnat.getURL(), response);

                final URL result = new URL(xnat.getURL() + "/" + resultPath);

                // TODO: build summary, notify user
                final UploadResultPanel resultPanel = new UploadResultPanel(session, buildSessionViewURL(result, resultPath));
                progress.finished(Summary.create(resultPanel, url));
                return true;
            } else {
                progress.failed(buildFailureMessage(failures), false);
                return false;
            }
        } catch (JSONException e) {
            logger.error("unable to write commit request entity", e);
            return false;
        } catch (HttpException e) {
            logger.error("session commit failed", e);
            switch (e.getResponseCode()) {
                case HttpURLConnection.HTTP_NOT_FOUND: {
                    final StringBuilder sb = new StringBuilder("<h3>Resource not found (404)</h3>");
                    sb.append("<p>The server at ");
                    appendServer(sb, url);
                    sb.append(" is accessible but reports that the session resource ");
                    sb.append(url.getPath());
                    sb.append(" does not exist.</p>");
                    sb.append("<p>Contact the administrator ");
                    sb.append("<").append(adminEmail).append(">");
                    sb.append(" for help.</p>");
                    progress.failed(sb.toString(), true);
                    return false;
                }

                case HttpURLConnection.HTTP_INTERNAL_ERROR: {
                    final StringBuilder sb = new StringBuilder("<h3>Internal Server Error (500)</h3>");
                    sb.append("<p>The server at ");
                    appendServer(sb, url);
                    sb.append(" is accessible but was unable to commit the requested session");
                    sb.append(" due to an internal error.</p>");
                    sb.append("<p>Please contact the administrator ");
                    sb.append("<").append(adminEmail).append(">");
                    sb.append(" for help.");
                    sb.append(" A detailed description of the problem should be available");
                    sb.append(" in the DICOM receiver log or the XNAT logs.</p>");
                    progress.failed(sb.toString(), true);
                    return false;
                }

                case HttpURLConnection.HTTP_CONFLICT: {
                    final StringBuilder sb = new StringBuilder("<h3>Session data conflict</h3>");
                    sb.append("<p>The server at ");
                    appendServer(sb, url);
                    sb.append(" reports a conflict between the uploaded data and a session in the archive.</p>");
                    sb.append("<p>All or part of this session was previously uploaded. Go to the prearchive page ");
                    sb.append("to archive the data just uploaded as a new session, or to merge it into an existing session.");
                    progress.failed(sb.toString(), true);
                    return false;
                }

                default: {
                    progress.failed("<h3>Unexpected error " + e.getResponseCode() + ": " + e.getMessage() + "</h3>" + "<p>Unable to commit uploaded session</p>" + "<p>Please contact your XNAT administrator " + "<" + adminEmail + ">" + " for help.</p>", true);
                    return false;
                }
            }
        } catch (IOException e) {
            logger.error("Session commit failed", e);
            final StringBuilder sb = new StringBuilder("<h3>Communications error</h3>");
            sb.append("<p>The server at ");
            appendServer(sb, url);
            sb.append(" is inaccessible (");
            sb.append(e.getMessage());
            sb.append("). Please contact your XNAT administrator ");
            sb.append("<").append(adminEmail).append(">");
            sb.append(" for help.</p>");
            progress.failed(sb.toString(), false);
            return false;
        } catch (Throwable t) {
            logger.error("Session commit failed", t);
            progress.failed("<h3>Error in applet</h3>" + "<p>An error in the uploader (" + t + " prevented the session from being committed." + " Please contact your XNAT administrator " + "<" + adminEmail + ">" + " for help.</p>", false);
            return false;
        }
    }

    public static StringBuilder appendServer(final StringBuilder sb, final URL url) {
        sb.append(url.getProtocol()).append("://");
        sb.append(url.getHost());
        final int httpPort = url.getPort();
        if (-1 != httpPort && httpPort != url.getDefaultPort()) {
            sb.append(":").append(httpPort);
        }
        return sb;
    }

    public static StringBuilder appendServer(final StringBuilder sb, final RestServer xnat) {
        return appendServer(sb, xnat.getURL());
    }

    private static <T> StringBuilder buildHTMLFailureMessage(final StringBuilder sb, final Map<?, T> failures) {
        final Multimap<T, Object> inverse = LinkedHashMultimap.create();
        Multimaps.invertFrom(Multimaps.forMap(failures), inverse);
        final Multimap<Object, ?> causes = org.nrg.util.Utils.consolidateKeys(inverse, 4);
        final MessageFormat format = new MessageFormat("{0} not uploaded: {1}");
        format.setFormatByArgumentIndex(0, new ChoiceFormat(new double[]{0, 1, 2},
                new String[]{"No items", "One item", "{0,number} items"}));
        for (final Object key : causes.keySet()) {
            final Collection<?> items = causes.get(key);
            final Object message;
            if (key instanceof HttpUploadException) {
                final HttpUploadException e = (HttpUploadException) key;
                final StringBuilder m = new StringBuilder("HTTP error ");
                m.append(e.getStatusCode()).append(" - ");
                m.append(e.getMessage()).append("<br>");
                m.append(e.getEntity());
                message = m;
            } else {
                message = key;
            }
            sb.append("<p>").append(format.format(new Object[]{items.size(), message}));
            sb.append("</p><br>");
        }
        return sb;
    }

    public static <T> String buildFailureMessage(final Map<?, T> failures) {
        final StringBuilder sb = new StringBuilder("<html>");
        buildHTMLFailureMessage(sb, failures);
        return sb.append("</html>").toString();
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.Session#getVariables()
     */
    public List<SessionVariable> getVariables(final Map<?, ?> params) {
        final Project project = ((Project) params.get(SelectProjectPage.PRODUCT_NAME));
        final LinkedHashSet<Variable> dvs = Sets.newLinkedHashSet();
        try {
            // This replaces variables in later scripts with similarly-name variables from
            // earlier scripts. Therefore scripts whose variables should take precedence
            // must appear earlier in the list.
            final Iterable<ScriptApplicator> applicators = project.getDicomScriptApplicators();
            for (final ScriptApplicator a : applicators) {
                for (final Variable v : dvs) {
                    a.unify(v);
                }
                dvs.addAll(a.getVariables().values());
            }
        } catch (Throwable t) {
            logger.warn("unable to load script", t);
            return Collections.emptyList();
        }
        final DicomObject o = series.isEmpty() ? null : series.get(0).getSampleObject();
        final List<SessionVariable> vs = Lists.newArrayList();
        for (final Variable dv : dvs) {
            vs.add(DicomSessionVariable.getSessionVariable(dv, o));
        }
        return vs;
    }
}
