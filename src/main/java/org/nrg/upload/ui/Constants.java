package org.nrg.upload.ui;

import netscape.javascript.JSObject;

import java.awt.*;

public class Constants {
    public static final String XNAT_REST_API_WIZ_PARAM = "*xnat-rest-server*";
    public static final String XNAT_URL_WIZ_PARAM = "*xnat-url*";
    public static final String XNAT_ADMIN_EMAIL_WIZ_PARAM = "*xnat-admin-email*";
    public static final String FIXED_SIZE_STREAMING_WIZ_PARAM = "*fixed-size-streaming*";
    public static final String N_UPLOAD_THREADS_WIZ_PARAM = "*n-upload-threads*";
    public static final String EXPECTED_MODALITY_LABEL = "*expected-modality*";
    public static final String XNAT_URL = "xnat-url";
    public static final String XNAT_USER = "xnat-user";
    public static final String XNAT_SAVE_INFO = "xnat-save-info";
    public static final int SPACING = 4;
    public static final Cursor LINK_CURSOR = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

    public static JSObject getContext() {
        return _context;
    }

    public static void setJSContext(final JSObject context) {
        _context = context;
    }

    public static String getWindowName() {
        return _windowName;
    }

    public static void setWindowName(final String windowName) {
        _windowName = windowName;
    }

    private static JSObject _context = null;
    private static String _windowName = "_self";
}
