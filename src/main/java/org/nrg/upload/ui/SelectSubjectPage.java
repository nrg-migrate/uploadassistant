/*
 * org.nrg.upload.ui.SelectSubjectPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.ui;

import org.json.JSONException;
import org.netbeans.spi.wizard.WizardPage;
import org.nrg.net.RestServer;
import org.nrg.upload.data.Project;
import org.nrg.upload.data.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public class SelectSubjectPage extends WizardPage {
    private static final long serialVersionUID = 1L;

    public static final String PRODUCT_NAME = "subject";
    private static final String STEP_DESCRIPTION = "Select subject";
    private static final String LONG_DESCRIPTION = "Select the subject for the session to be uploaded";

    private final Logger logger = LoggerFactory.getLogger(SelectSubjectPage.class);
    private final JList<Subject> list;
    private final DefaultListModel<Subject> listModel = new DefaultListModel<>();
    private final Dimension dimension;
    private final RestServer xnat;

    public static String getDescription() {
        return STEP_DESCRIPTION;
    }

    public SelectSubjectPage(final RestServer xnat, final Dimension dimension) {
        this.xnat = xnat;
        this.dimension = dimension;
        list = new JList<>(listModel);
        list.setName(PRODUCT_NAME);
        setLongDescription(LONG_DESCRIPTION);
    }

    /*
      * (non-Javadoc)
      * @see org.netbeans.spi.wizard.WizardPage#recycle()
      */
    protected void recycle() {
        removeAll();
        refreshSubjectList();
        validate();
    }

    /*
      * (non-Javadoc)
      * @see org.netbeans.spi.wizard.WizardPage#renderingPage()
      */
    protected void renderingPage() {
        setBusy(true);
        final Project project = (Project) getWizardData(SelectProjectPage.PRODUCT_NAME);
        final JScrollPane subjectList = new JScrollPane(list);
        if (null != dimension) {
            subjectList.setPreferredSize(dimension);
        }
        add(subjectList);
        if (null != project) {
            refreshSubjectList();
            final JButton newSubject = new JButton("Create new subject");

            final JDialog newSubjectDialog = new NewSubjectDialog(this, xnat, project);
            newSubject.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    newSubjectDialog.setVisible(true);
                }
            });
            newSubject.setEnabled(true);
            add(newSubject);
        }
        setBusy(false);
    }

    /*
      * (non-Javadoc)
      * @see org.netbeans.spi.wizard.WizardPage#validateContents(java.awt.Component, java.lang.Object)
      */
    protected String validateContents(final Component component, final Object o) {
        return null == getWizardData(PRODUCT_NAME) ? "" : null;
    }

    /**
     * Refreshes the list of subjects in the current project.
     *
     * @param selection item in the subjects list to be selected after refresh
     */
    void refreshSubjectList(final Object selection) {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        final Project project = (Project) getWizardData(SelectProjectPage.PRODUCT_NAME);
        putWizardData(PRODUCT_NAME, null);
        final Callable<Object> doRefresh = new Callable<Object>() {
            public Object call() throws IOException, JSONException {
                for (; ; ) {
                    listModel.removeAllElements();
                    try {
                        final SortedSet<Subject> subjects = new TreeSet<>(new Subject.SubjectComparator());
                        for (final Subject subject : project.getSubjects()) {
                            subjects.add(subject);
                        }
                        for (final Subject subject : subjects) {
                            listModel.addElement(subject);
                        }
                        break;
                    } catch (InterruptedException retry) {
                        logger.info("subject retrieval interrupted, retrying", retry);
                    } catch (ExecutionException e) {
                        final Throwable cause = e.getCause();
                        if (cause instanceof IOException) {
                            final Object[] options = {"Retry", "Cancel"};
                            final int n = JOptionPane.showOptionDialog(SelectSubjectPage.this,
                                    "Unable to contact " + xnat,
                                    "Network error",
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.ERROR_MESSAGE,
                                    null,
                                    options,
                                    options[0]);
                            if (JOptionPane.NO_OPTION == n) {
                                throw new IOException(cause);
                            } else {
                                logger.error("error getting subject list; retrying at user request", cause);
                            }
                        } else if (cause instanceof JSONException) {
                            final Object[] options = {"Retry", "Cancel"};
                            final int n = JOptionPane.showOptionDialog(SelectSubjectPage.this,
                                    "Received invalid response from " + xnat,
                                    "Server error",
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.ERROR_MESSAGE,
                                    null,
                                    options,
                                    options[0]);
                            if (JOptionPane.NO_OPTION == n) {
                                throw new IOException(cause);
                            } else {
                                logger.error("error getting subject list; retrying at user request", cause);
                            }
                        } else {
                            logger.error("error getting subject list for " + project, cause);
                            logger.info("will retry in 1000 ms");
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ignore) {
                                JOptionPane.showMessageDialog(null, "Got an exception: " + ignore.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        selectSubject(selection);
                    }
                });
                return selection;
            }
        };
        project.submit(doRefresh);
        setCursor(Cursor.getDefaultCursor());
    }

    /**
     * Refreshes the list of subjects in the current project, re-selecting
     * the current selection after refresh is complete.
     */
    void refreshSubjectList() {
        refreshSubjectList(getWizardData(PRODUCT_NAME));
    }

    private void selectSubject(Object selection) {
        if (listModel.contains(selection)) {
            list.setSelectedValue(selection, true);
        }
    }
}
