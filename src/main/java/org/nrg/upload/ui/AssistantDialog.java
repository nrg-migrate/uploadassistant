package org.nrg.upload.ui;

import javax.swing.*;
import java.awt.*;

public class AssistantDialog extends JDialog {
	public AssistantDialog(final Frame owner, final String title, final boolean modal) {
		super(owner, title, modal);
	}

	protected JLabel makeLabel(final String text) {
		final JLabel label = new JLabel(text);
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setHorizontalTextPosition(SwingConstants.RIGHT);
		return label;
	}

	protected JLabel makeMessage(final String message) {
		return makeMessage(message, null);
	}

	protected JLabel makeMessage(final String message, final Color color) {
        final JLabel label = new JLabel(message);
        label.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
        label.setHorizontalAlignment(SwingConstants.LEFT);
        label.setHorizontalTextPosition(SwingConstants.LEFT);
		if (color != null) {
			label.setForeground(color);
		}
		return label;
    }

    protected JTextField makeTextField() {
        JTextField text = new JTextField(VALUE_WIDTH);
        text.setMinimumSize(text.getPreferredSize());
        return text;
    }

	protected GridBagConstraints makeLabelConstraints(final int row) {
		final GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = row;
		gbc.insets = new Insets(4, 8, 0, 0);
		return gbc;
	}

	protected GridBagConstraints makeValueConstraints(final int row) {
		final GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 1;
		gbc.gridwidth = 2;
		gbc.gridy = row;
		gbc.insets = new Insets(2, 2, 2, 2);
		return gbc;
	}

	protected GridBagConstraints makeButtonConstraints(final int col, final int row) {
		final GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = col;
		gbc.gridy = row;
		gbc.insets = new Insets(2, 2, 2, 2);
		return gbc;
	}

	protected GridBagConstraints makeMessageConstraints(final int row) {
		return makeMessageConstraints(row, null);
	}

	protected GridBagConstraints makeMessageConstraints(final int row, final Dimension padding) {
		final GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbc.gridy = row;
		if (padding != null) {
			gbc.ipadx = (int) padding.getWidth();
			gbc.ipady = (int) padding.getHeight();
		}
		return gbc;
	}

    private static final int VALUE_WIDTH = 24;
}
