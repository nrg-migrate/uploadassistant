package org.nrg.upload.ui;

import netscape.javascript.JSObject;
import org.nrg.awt.MouseClickTriggeredCallable;
import org.nrg.js.JSEval;
import org.nrg.util.Messages;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class AppletLinkSource implements LinkSource {
    @Override
    public JLabel getLinkForResource(final Component component, final String label, final URL url) {
        final JLabel link = new JLabel(Messages.getMessage(Messages.UPLOADRESULTPANEL_DEST_LINK, url, label));
        link.setCursor(Constants.LINK_CURSOR);
        final JSObject context = Constants.getContext();
        if (null != context) {
            final JSEval eval = new JSEval(context, String.format("this.open(\"%s\",'%s');", url, Constants.getWindowName()));
            link.addMouseListener(new MouseClickTriggeredCallable(eval));
        } else {
            UIUtils.handleUrlClick(component, url.toString());
        }
        return link;
    }
}
