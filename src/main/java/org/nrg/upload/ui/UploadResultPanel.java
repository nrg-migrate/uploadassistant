/*
 * org.nrg.upload.ui.UploadResultPanel
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.ui;

import org.nrg.util.Messages;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

public final class UploadResultPanel extends JPanel {
    public UploadResultPanel(final String label, final URL url) {
        final boolean isArchived = url.getPath().split("/")[3].equalsIgnoreCase("archive");
        final JLabel link = getLinkSource().getLinkForResource(this, label, url);

        final Box box = new Box(BoxLayout.Y_AXIS);
        add(box);
        box.add(new JLabel(Messages.getMessage(Messages.UPLOADRESULTPANEL_SUCCESS, Messages.getMessage(isArchived ? Messages.VOCABULARY_ARCHIVE : Messages.VOCABULARY_PREARCHIVE))));
        box.add(Box.createVerticalStrut(Constants.SPACING));
        box.add(link);
    }

    private LinkSource getLinkSource() {
        final Container container = getTopLevelAncestor();
        if (container instanceof JApplet) {
            return new AppletLinkSource();
        } else {
            return new ApplicationLinkSource();
        }
    }
}
