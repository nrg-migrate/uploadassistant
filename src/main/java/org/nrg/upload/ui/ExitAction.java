package org.nrg.upload.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;

public class ExitAction extends AbstractAction {
    public ExitAction(final Component component) {
        super("Exit");
        _component = component;
        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        _log.info("Now performing the action {}", e.getActionCommand());

        //  Find the active window before creating and dispatching the event
        Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();

        if (window != null) {
            int confirm = JOptionPane.showOptionDialog(_component,
                    "Are you sure you want to close this application?",
                    "Please Confirm", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, null, null);
            if (confirm == JOptionPane.YES_OPTION) {
                WindowEvent windowClosing = new WindowEvent(window, WindowEvent.WINDOW_CLOSING);
                window.dispatchEvent(windowClosing);
            }
        }
    }

    private static final Logger _log = LoggerFactory.getLogger(ExitAction.class);
    private final Component _component;
}
