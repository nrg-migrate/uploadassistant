package org.nrg.upload.ui;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public interface LinkSource {
    JLabel getLinkForResource(final Component component, String label, URL url);
}
