package org.nrg.upload.ui;

import org.nrg.util.Messages;

import javax.swing.*;
import java.awt.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class ApplicationLinkSource implements LinkSource {
    @Override
    public JLabel getLinkForResource(final Component component, final String label, final URL url) {
        final JLabel link = new JLabel(Messages.getMessage(Messages.UPLOADRESULTPANEL_DEST_LINK, url, label));
        link.setCursor(Constants.LINK_CURSOR);
        try {
            final URI uri = new URI(url.toString()); //url.toURI();
            link.addMouseListener(new UriClickListener(uri));
        } catch (URISyntaxException e) {
            UIUtils.handleAppletError(component, "An error occurred converting the URL " + url + " to a URI: " + e.getMessage(), "Error in URL", JOptionPane.ERROR_MESSAGE);
        }
        return link;
    }
}
