/*
 * org.nrg.upload.ui.SelectFilesPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.ui;

import org.netbeans.spi.wizard.WizardPage;

import javax.swing.*;

public class SelectFilesPage extends WizardPage {
	private static final long serialVersionUID = 1L;
	
	public static final String PRODUCT_NAME = "*file-chooser*";
	private static final String STEP_DESCRIPTION = "Select files";
	private static final String LONG_DESCRIPTION = "Select the directory containing the session to be uploaded";
	
	private final JFileChooser fc;
	
	public static String getDescription() {
		return STEP_DESCRIPTION; 
	}
	
	public SelectFilesPage() {
		fc = new JFileChooser();
		fc.setControlButtonsAreShown(false);
		fc.setMultiSelectionEnabled(true);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.setAcceptAllFileFilterUsed(false);
        fc.setFileHidingEnabled(true);
		fc.setName(PRODUCT_NAME);
        setLongDescription(LONG_DESCRIPTION);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.netbeans.spi.wizard.WizardPage#recycle()
	 */
	public void recycle() {
		removeAll();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.netbeans.spi.wizard.WizardPage#renderingPage()
	 */
	public void renderingPage() {
		this.add(fc);
		putWizardData(PRODUCT_NAME, fc);
	}
}
