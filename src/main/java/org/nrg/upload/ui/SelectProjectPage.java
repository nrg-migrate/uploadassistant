/*
 * org.nrg.upload.ui.SelectProjectPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.upload.ui;

import org.json.JSONException;
import org.netbeans.spi.wizard.WizardPage;
import org.nrg.net.RestServer;
import org.nrg.upload.data.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

public final class SelectProjectPage extends WizardPage implements ListSelectionListener {
	private static final long serialVersionUID = 1L;

	public static final String PRODUCT_NAME = "project";
	private static final String STEP_DESCRIPTION = "Select project";
	private static final String LONG_DESCRIPTION = "Select the project to which you will upload a session";

	private final Logger logger = LoggerFactory.getLogger(SelectProjectPage.class);

	private transient final RestServer xnat;
	private final JList list;
	private final Dimension dimension;

	public static String getDescription() {
		return STEP_DESCRIPTION;
	}

	public SelectProjectPage(final RestServer xnat, final Dimension dimension)
	throws IOException,JSONException {
		super();

		this.xnat = xnat;

		final String path = "/REST/projects?format=json&owner=true&member=true";

		// We could just load the JList with Project objects.  Instead, we load it with
		// labels and create the Projects dynamically upon selection, because the Project
		// constructor spins off threads to retrieve the contained subjects and sessions.
		// This is smart if we're creating one Project at a time, but troublesome when
		// creating lots of Projects.
		logger.trace("initializing SelectProjectPage; querying XNAT for project labels");
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		list = new JList<>(new Vector<>(xnat.getValues(path, "id")));
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(this);

		this.dimension = dimension;

		setOpaque(true);
		setLongDescription(LONG_DESCRIPTION);
        setCursor(Cursor.getDefaultCursor());

		logger.trace("SelectProjectPage ready with projects " + Arrays.toString(list.getComponents()));
	}

	/*
	 * (non-Javadoc)
	 * @see org.netbeans.spi.wizard.WizardPage#recycle()
	 */
	public void recycle() {
		removeAll();
	}

	/*
	 * (non-Javadoc)
	 * @see org.netbeans.spi.wizard.WizardPage#renderingPage()
	 */
	public void renderingPage() {
		logger.trace("rendering SelectProjectPage");
		final JScrollPane pane = new JScrollPane(list);
		if (null != dimension) {
			pane.setPreferredSize(dimension);
		}
		add(pane);
	}

	/*
	 * (non-Javadoc)
	 * @see org.netbeans.spi.wizard.WizardPage#validateContents(java.awt.Component, java.lang.Object)
	 */
	protected String validateContents(final Component component, final Object o) {
		if (1 != list.getSelectedIndices().length) {
			return "";
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(final ListSelectionEvent e) {
		if (!e.getValueIsAdjusting()) {	// wait until selection is complete
			final Project old = (Project)getWizardData(PRODUCT_NAME);
			if (null != old) {
				old.dispose();
			}
			putWizardData(PRODUCT_NAME, new Project((String)list.getSelectedValue(), xnat, getWizardDataMap()));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#toString()
	 */
	public String toString() {
		final Project old = (Project)getWizardData(PRODUCT_NAME);
		return null == old ? "" : old.toString();
	}
}
