/*
 * org.nrg.upload.ui.UploadAssistantApplet
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.upload.ui;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.nrg.net.ApplicationAuthenticator;
import org.nrg.net.JSESSIONIDCookie;
import org.nrg.net.RestServer;
import org.nrg.upload.data.AssignedSessionVariable;
import org.nrg.upload.data.Project;
import org.nrg.upload.data.SessionVariableNames;
import org.nrg.upload.data.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import static org.nrg.upload.data.SessionVariableNames.PREDEF_SESSION;

@SuppressWarnings("Duplicates")
public class UploadAssistantApplication extends JFrame implements WizardResultReceiver {

    public static void main(String[] args) {
        if (_log.isDebugEnabled()) {
            _log.debug("I have entered the main() method for the UploadAssistantApplication class.");
        }
        try {
            final UploadAssistantApplication application = new UploadAssistantApplication();
            application.start();
            application.setVisible(true);
        } catch (IOException e) {
            _log.error("An error occurred running the upload assistant application", e);
        }
    }

    /**
     * Default constructor.
     *
     * @throws IOException When something goes wrong reading data.
     */
    public UploadAssistantApplication() throws IOException {
        // TODO: Get the resource from a properties bundle.
        super("XNAT Upload Assistant");

        if (_log.isDebugEnabled()) {
            _log.debug("I have entered the constructor for the UploadAssistantApplication class.");
        }

        _preferences = Preferences.userNodeForPackage(UploadAssistantApplication.class);
        _authenticator = new ApplicationAuthenticator(this, _preferences);
        _closeHandler = new ExitAction(this);

        setDefaultLookAndFeelDecorated(true);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setIconImage(new ImageIcon(getClass().getResource("/org/nrg/upload/xnat.png")).getImage());
        setSize(800, 500);
        setResizable(false);

        // We need an alias for this to use inside the object initialization blocks below.
        setJMenuBar(new JMenuBar() {{
            add(new JMenu("File") {{
                add(new JMenuItem("Exit") {{
                    addActionListener(_closeHandler);
                }});
            }});
        }});

        HttpURLConnection.setFollowRedirects(false);

        configureLookAndFeel();
        configureLogging();

        Properties properties = new Properties(System.getProperties());
        loadApplicationProperties(properties);
        loadCustomProperties(properties);

        try {
            if (_preferences.keys().length == 0) {
                _log.debug("Zero-length preferences store found, initializing.");
            } else {
                // TODO: This may need to get fancier if we need to use the non-string data types.
                for (final String key : _preferences.keys()) {
                    properties.setProperty(key, _preferences.get(key, ""));
                }
            }
        } catch (BackingStoreException e) {
            _log.error("Unable to access the user preference settings", e);
        }

        System.setProperties(properties);

        if (_log.isDebugEnabled()) {
            _log.debug("I am about to initialize the REST server object.");
        }

        boolean authenticated;
        while (!(authenticated = _authenticator.getCredentials())) {
            int selected = JOptionPane.showOptionDialog(this, "Are you sure you want to quit the XNAT upload assistant?", "Cancel?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Try again", "Quit"}, "Try again");
            if (selected == JOptionPane.NO_OPTION) {
                break;
            }
        }

        if (!authenticated) {
            _xnat = null;
            terminate("No credentials were set for accessing XNAT. Terminating.");
        } else {
            Authenticator.setDefault(_authenticator);
            _xnat = initializeRestServer();
            if (_xnat == null) {
                terminate("Something went wrong trying to authenticate. Please check your credentials and server address.");
            }
        }
    }

    private void terminate(final String message) {
        JOptionPane.showMessageDialog(this, message);
        WindowEvent windowClosing = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        dispatchEvent(windowClosing);
    }

    /**
     */
    private void start() {
        _log.trace("START {}", this);

        final Dimension dimension = new Dimension(300, 300);

        try {
            _log.trace("{} initializing pages", this);
            final List<WizardPage>    pages  = Lists.newArrayList();
            final Map<String, Object> params = buildParamsMap(_xnat);

            final String projectName = getParameter(XNAT_PROJECT);
            if (!Strings.isNullOrEmpty(projectName)) {
                _log.trace("project: {}", projectName);
                params.put(SelectProjectPage.PRODUCT_NAME, new Project(projectName, _xnat, params));
            } else {
                pages.add(new SelectProjectPage(_xnat, dimension));
                _log.trace("project");
            }

            final String subjectName = getParameter(XNAT_SUBJECT);
            if (!Strings.isNullOrEmpty(subjectName)) {
                final Project project = (Project) params.get(SelectProjectPage.PRODUCT_NAME);
                if (null == project) {
                    // TODO: use a placeholder?
                    throw new UnsupportedOperationException("can't assign subject without project");
                } else {
                    for (final Subject subject : project.getSubjects()) {
                        if (subjectName.equals(subject.getLabel()) || subjectName.equals(subject.getId())) {
                            params.put(SelectSubjectPage.PRODUCT_NAME, subject);
                            break;
                        }
                    }
                }
            }
            if (null == params.get(SelectSubjectPage.PRODUCT_NAME)) {
                pages.add(new SelectSubjectPage(_xnat, dimension));
            }
            _log.trace("subject");


            final String visitLabel = getParameter(XNAT_VISIT);
            //TODO: we want to verify this visit... and if there isn't a visit, we want to check the project to see if it has a protocol
            //so we can get a list of visits and let the user associate the session with a visit.
            //For now, we'll just assume the visit is a valid, existing label for a pVisitdata and pass it through to the importer.
            if (!Strings.isNullOrEmpty(visitLabel)) {
                _log.trace("visit: {}", visitLabel);
                params.put(SessionVariableNames.VISIT_LABEL, new AssignedSessionVariable(SessionVariableNames.VISIT_LABEL, visitLabel));
            }

            final String protocolLabel = getParameter(XNAT_PROTOCOL);
            //TODO: we want to verify this experiment's protocol is valid for this visit... and if there isn't a visit, we want to check the project to see
            //if it has a protocol so we can get a list of visits and let the user associate the session with a visit.
            //For now, we'll just assume the protocol is valid and pass it through to the importer.
            if (!Strings.isNullOrEmpty(protocolLabel)) {
                _log.trace("protocol: {}", protocolLabel);
                params.put(SessionVariableNames.PROTOCOL_LABEL, new AssignedSessionVariable(SessionVariableNames.PROTOCOL_LABEL, protocolLabel));
            }

            final String expectedModality = getParameter(EXPECTED_MODALITY);
            if (!Strings.isNullOrEmpty(expectedModality)) {
                _log.trace("expected modality: {}", expectedModality);
                params.put(Constants.EXPECTED_MODALITY_LABEL, expectedModality);

            }

            final String sessionLabel = getParameter(XNAT_SESSION);
            if (!Strings.isNullOrEmpty(sessionLabel)) {
                _log.trace("session: {}", sessionLabel);
                params.put(PREDEF_SESSION, new AssignedSessionVariable(SessionVariableNames.SESSION_LABEL, sessionLabel));
            }

            final String warnOnDupeSessionLabels = getParameter(SessionVariableNames.WARN_ON_DUPE_SESSION_LABELS);
            if (!Strings.isNullOrEmpty(warnOnDupeSessionLabels)) {
                _log.trace("Warn on dupe session labels: {}", warnOnDupeSessionLabels);
                params.put(SessionVariableNames.WARN_ON_DUPE_SESSION_LABELS, new AssignedSessionVariable(SessionVariableNames.WARN_ON_DUPE_SESSION_LABELS, warnOnDupeSessionLabels));
            }

            final String allowOverwriteOnDupeSessionLabels = getParameter(SessionVariableNames.ALLOW_OVERWRITE_ON_DUPE_SESSION_LABELS);
            if (!Strings.isNullOrEmpty(allowOverwriteOnDupeSessionLabels)) {
                _log.trace("Allow overwrite on dupe session labels: {}", allowOverwriteOnDupeSessionLabels);
                params.put(SessionVariableNames.ALLOW_OVERWRITE_ON_DUPE_SESSION_LABELS, new AssignedSessionVariable(SessionVariableNames.ALLOW_OVERWRITE_ON_DUPE_SESSION_LABELS, allowOverwriteOnDupeSessionLabels));
            }
            final String allowAppendOnDupeSessionLabels = getParameter(SessionVariableNames.ALLOW_APPEND_ON_DUPE_SESSION_LABELS);
            if (!Strings.isNullOrEmpty(allowAppendOnDupeSessionLabels)) {
                _log.trace("Allow append on dupe session labels: {}", allowAppendOnDupeSessionLabels);
                params.put(SessionVariableNames.ALLOW_APPEND_ON_DUPE_SESSION_LABELS, new AssignedSessionVariable(SessionVariableNames.ALLOW_APPEND_ON_DUPE_SESSION_LABELS, allowAppendOnDupeSessionLabels));
            }

            // Allow the system configuration to override the session date confirmation page, but default to showing it.
            final String scanDate = getParameter(XNAT_SCAN_DATE);
            if (UIUtils.getConfirmSessionDatePage()) {
                if ("no_session_date".equals(scanDate)) {
                    params.put(ConfirmSessionDatePage.PRODUCT_NAME, "no_session_date");
                } else {
                    Date date = UIUtils.parseDate(scanDate);
                    if (date == null) {
                        pages.add(new ConfirmSessionDatePage());
                        _log.trace("confirm date");
                    } else {
                        params.put(ConfirmSessionDatePage.PRODUCT_NAME, date);
                    }
                }
            }
            pages.add(new SelectFilesPage());
            _log.trace("files");
            if (StringUtils.isBlank(projectName)) {
                pages.add(new SelectSessionPage(_xnat));
            } else {
                pages.add(new SelectSessionPage(_xnat, projectName));
            }
            _log.trace("session");
            pages.add(new AssignSessionVariablesPage());
            _log.trace("session variables");
            _log.trace("{} creating wizard", this);
            final Wizard wizard = WizardPage.createWizard(pages.toArray(new WizardPage[pages.size()]), new UploadWizardResultProducer(Executors.newCachedThreadPool()));
            _log.trace("{} installing displayer", this);
            final WizardDisplayer displayer = WizardDisplayer.installInContainer(this, null, wizard, null, params, this);
            displayer.setCloseHandler(_closeHandler);
        } catch (Throwable t) {
            displayError(t);
        }

        _log.trace("{} ready", this);
    }

    /**
     * Implementation of the {@link WizardResultReceiver#cancelled(Map)} method.
     *
     * @see WizardResultReceiver#cancelled(Map)
     */
    @Override
    public void cancelled(@SuppressWarnings("rawtypes") Map args) {
        _log.info("The user requested to cancel.");
        _closeHandler.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Exit"));
    }

    /**
     * Implementation of the {@link WizardResultReceiver#finished(Object)} method.
     *
     * @see WizardResultReceiver#finished(Object)
     */
    @Override
    public void finished(Object args) {
        _log.info("The wizard is finished.");
    }

    /**
     * Initializes the configuration for the REST server.
     *
     * @return An initialized {@link RestServer} object.
     */
    private RestServer initializeRestServer() {
        try {
            String xnatUrl = getParameter(Constants.XNAT_URL);
            if (StringUtils.isBlank(xnatUrl)) {
                if (!_authenticator.getCredentials()) {
                    return null;
                }
                xnatUrl = _authenticator.getUrl();
            }

            _log.info("Starting with REST services at: " + xnatUrl);
            String           jsessionid      = getParameter(JSESSIONID);
            final RestServer xnat            = StringUtils.isBlank(jsessionid) ? new RestServer(xnatUrl, _authenticator) : new RestServer(xnatUrl, new JSESSIONIDCookie(jsessionid));
            String           userAuthMessage = xnat.getUserAuthMessage();    // hit the server with a non-public REST call to sort out authentication
            if (_log.isDebugEnabled()) {
                _log.debug("Received user auth message: " + userAuthMessage);
            }
            // If we made it here without an exception, we have a valid REST server object.
            return xnat;
        } catch (Throwable t) {
            displayError(t);
        }
        return null;
    }

    private void displayError(final Throwable t) {
        _log.error("applet startup failed", t);
        final StringWriter sw     = new StringWriter();
        final PrintWriter  writer = new PrintWriter(sw);
        writer.println("Applet startup failed; please contact " + getParameter(XNAT_ADMIN_EMAIL) + " for help.");
        writer.println("Error details: " + t.getMessage());
        t.printStackTrace(writer);
        final JTextArea text = new JTextArea(sw.toString());
        text.setEditable(false);
        add(text);
        validate();
    }

    /**
     * Load the UIManager system look and feel.
     */
    private void configureLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Loads logging resources, including loading logging properties from custom URLs specified by the
     * {@link #LOG4J_PROPS_URL} applet parameter.
     */
    private void configureLogging() {
        final String log4jProps = getParameter(LOG4J_PROPS_URL);
        if (StringUtils.isNotBlank(log4jProps)) {
            try {
                PropertyConfigurator.configure(new URL(log4jProps));
            } catch (MalformedURLException e) {
                _log.error("Unable to read remote log4j configuration file " + log4jProps, e);
            }
        }
    }

    /**
     * Loads applet properties from the <b>UploadAssistant.properties</b>properties file. This
     * can be overridden by placing a custom version of the properties file ahead of
     * the applet jar on the classpath.
     *
     * @param properties The system properties for the applet.
     */
    private void loadApplicationProperties(final Properties properties) {
        try (final InputStream applicationProperties = getClass().getResourceAsStream("application.properties")) {
            if (applicationProperties != null) {
                properties.load(applicationProperties);
            }
        } catch (IOException exception) {
            _log.info("Unable to find the application.properties resource for initialization", exception);
        }
    }

    /**
     * Loads properties from custom properties specified in the {@link #CUSTOM_PROPS_URL}
     * applet parameter. If this parameter is not specified, no properties are loaded.
     *
     * @param properties The system properties for the applet.
     */
    private void loadCustomProperties(Properties properties) {
        final String customProps = getParameter(CUSTOM_PROPS_URL);
        if (StringUtils.isNotBlank(customProps)) {
            InputStream customProperties = null;
            try {
                customProperties = getClass().getResourceAsStream(customProps);
                properties.load(customProperties);
            } catch (IOException exception) {
                _log.info("Unable to find the custom properties resource " + customProps + " for initialization", exception);
            } finally {
                if (customProperties != null) {
                    try {
                        customProperties.close();
                    } catch (IOException ignored) {
                        // Once again, whatever.
                    }
                }
            }
        }
    }

    /**
     * Builds a map of the incoming parameters for managing the wizard.
     *
     * @param xnat The XNAT REST server, as initialized in the {@link #initializeRestServer()} method.
     * @return The map of parameters for the wizard.
     */
    private Map<String, Object> buildParamsMap(final RestServer xnat) {
        final Map<String, Object> params = Maps.newLinkedHashMap();
        params.put(Constants.XNAT_REST_API_WIZ_PARAM, xnat);
        params.put(Constants.XNAT_URL_WIZ_PARAM, xnat.getURL());
        params.put(Constants.XNAT_ADMIN_EMAIL_WIZ_PARAM, getParameter(XNAT_ADMIN_EMAIL));
        params.put(Constants.FIXED_SIZE_STREAMING_WIZ_PARAM, Boolean.valueOf(getParameter(USE_FIXED_SIZE_STREAMING, "true")));
        params.put(Constants.N_UPLOAD_THREADS_WIZ_PARAM, Integer.valueOf(getParameter(N_UPLOAD_THREADS, "4")));
        return params;
    }

    /**
     * Creates a listener to handle the closing of the applet. This allows for a clean exit.
     */
    private transient final ActionListener _closeHandler;

    /**
     * Retrieves a parameter from the preferences or system properties.
     *
     * @param key The parameter value to retrieve.
     * @return The value corresponding to the requested key.
     */
    private String getParameter(final String key) {
        return _preferences.get(key, null);
    }

    /**
     * Sets the preference parameters specified in the submitted map. If the value of a particular preference is set to
     * null, the preference will be removed from the preferences store.
     *
     * @param preferences The map of preferences to be persisted.
     */
    public void setParameters(final Map<String, String> preferences) {
        for (final String preference : preferences.keySet()) {
            final String value = preferences.get(preference);
            if (value == null) {
                _preferences.remove(preference);
            } else {
                _preferences.put(preference, value);
            }
        }
        try {
            _preferences.flush();
        } catch (BackingStoreException e) {
            _log.error("Unable to access the user preference settings", e);
        }
    }

    /**
     * Retrieves a parameter from the applet start-up parameters.
     *
     * @param key          The name of the parameter to retrieve.
     * @param defaultValue The default value to be returned if the parameter is not found.
     * @return The value of the parameter if found, the default value otherwise.
     */
    private String getParameter(final String key, final String defaultValue) {
        final String v = getParameter(key);
        return null == v ? defaultValue : v;
    }

    private static final Logger _log = LoggerFactory.getLogger(UploadAssistantApplication.class);

    private static final String XNAT_PROJECT             = "xnat-project";
    private static final String XNAT_SUBJECT             = "xnat-subject";
    private static final String XNAT_SCAN_DATE           = "xnat-scan-date";
    private static final String XNAT_VISIT               = "xnat-visit";
    private static final String XNAT_PROTOCOL            = "xnat-protocol";
    private static final String EXPECTED_MODALITY        = "expected-modality";
    private static final String XNAT_SESSION             = "xnat-session-label";
    private static final String XNAT_ADMIN_EMAIL         = "xnat-admin-email";
    private static final String LOG4J_PROPS_URL          = "log4j-properties-url";
    private static final String CUSTOM_PROPS_URL         = "custom-properties-url";
    private static final String USE_FIXED_SIZE_STREAMING = "fixed-size-streaming";
    private static final String N_UPLOAD_THREADS         = "n-upload-threads";
    private static final String JSESSIONID               = "jsessionid";

    private final RestServer               _xnat;
    private final ApplicationAuthenticator _authenticator;
    private final Preferences              _preferences;
}
