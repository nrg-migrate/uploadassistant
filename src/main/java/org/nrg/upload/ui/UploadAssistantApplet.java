/*
 * org.nrg.upload.ui.UploadAssistantApplet
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.upload.ui;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.nrg.net.AppletAuthenticator;
import org.nrg.net.JSESSIONIDCookie;
import org.nrg.net.RestServer;
import org.nrg.upload.data.AssignedSessionVariable;
import org.nrg.upload.data.Project;
import org.nrg.upload.data.SessionVariableNames;
import org.nrg.upload.data.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.nrg.upload.data.SessionVariableNames.PREDEF_SESSION;

@SuppressWarnings("Duplicates")
public class UploadAssistantApplet extends JApplet implements WizardResultReceiver {

    /**
     * Default constructor.
     */
    public UploadAssistantApplet() {
        setLayout(new BorderLayout());
    }

    /**
     * Initializes the applet.
     *
     * @see Applet#init()
     */
    @Override
    public void init() {
        logger.trace("INIT {}", this);

        HttpURLConnection.setFollowRedirects(false);

        configureLookAndFeel();
        configureLogging();

        Properties properties = new Properties(System.getProperties());
        loadAppletProperties(properties);
        loadCustomProperties(properties);
        System.setProperties(properties);

        Authenticator authenticator;
        String user = getParameter(DEV_USER);
        String pass = getParameter(DEV_PASS);
        if (StringUtils.isNotBlank(user) && StringUtils.isNotBlank(pass)) {
            logger.warn("Using DevAuthenticator for applet credentials");
            authenticator = new DevAuthenticator(user, pass);
        } else {
            authenticator = new AppletAuthenticator(this);
        }
        Authenticator.setDefault(authenticator);
    }

    /**
     * Implementation of the {@link Applet#getAppletInfo()} method.
     *
     * @see java.applet.Applet#getAppletInfo()
     */
    @Override
    public String getAppletInfo() {
        return APPLET_INFO;
    }

    /**
     * Implementation of the {@link Applet#getParameterInfo()} method.
     *
     * @see java.applet.Applet#getParameterInfo()
     */
    @Override
    public String[][] getParameterInfo() {
        return PARAMETER_INFO;
    }

    /**
     * Implementation of the {@link Applet#start()} method.
     *
     * @see java.applet.Applet#start()
     */
    @Override
    public void start() {
        logger.trace("START {}", this);
        final RestServer xnat = initializeRestServer();
        if (xnat == null) {
            return;
        }

        final Dimension dimension = new Dimension(300, 300);

        try {
            logger.trace("{} initializing pages", this);
            final List<WizardPage> pages = Lists.newArrayList();
            final Map<String, Object> params = buildParamsMap(xnat);

            String userAuthMessage = xnat.getUserAuthMessage();    // hit the server with a non-public REST call to sort out authentication
            if (logger.isDebugEnabled()) {
                logger.debug("Received user auth message: " + userAuthMessage);
            }
            // Analytics.enter(UploadAssistantApplet.class, userAuthMessage);

            final String projectName = getParameter(XNAT_PROJECT);
            if (!Strings.isNullOrEmpty(projectName)) {
                logger.trace("project: {}", projectName);
                params.put(SelectProjectPage.PRODUCT_NAME, new Project(projectName, xnat, params));
            } else {
                pages.add(new SelectProjectPage(xnat, dimension));
                logger.trace("project");
            }

            final String subjectName = getParameter(XNAT_SUBJECT);
            if (!Strings.isNullOrEmpty(subjectName)) {
                final Project project = (Project) params.get(SelectProjectPage.PRODUCT_NAME);
                if (null == project) {
                    // TODO: use a placeholder?
                    throw new UnsupportedOperationException("can't assign subject without project");
                } else {
                    for (final Subject subject : project.getSubjects()) {
                        if (subjectName.equals(subject.getLabel()) || subjectName.equals(subject.getId())) {
                            params.put(SelectSubjectPage.PRODUCT_NAME, subject);
                            break;
                        }
                    }
                }
            }
            if (null == params.get(SelectSubjectPage.PRODUCT_NAME)) {
                pages.add(new SelectSubjectPage(xnat, dimension));
            }
            logger.trace("subject");


            final String visitLabel = getParameter(XNAT_VISIT);
            //TODO: we want to verify this visit... and if there isn't a visit, we want to check the project to see if it has a protocol
            //so we can get a list of visits and let the user associate the session with a visit.
            //For now, we'll just assume the visit is a valid, existing label for a pVisitdata and pass it through to the importer.
            if (!Strings.isNullOrEmpty(visitLabel)) {
                logger.trace("visit: {}", visitLabel);
                params.put(SessionVariableNames.VISIT_LABEL, new AssignedSessionVariable(SessionVariableNames.VISIT_LABEL, visitLabel));
            }

            final String protocolLabel = getParameter(XNAT_PROTOCOL);
            //TODO: we want to verify this experiment's protocol is valid for this visit... and if there isn't a visit, we want to check the project to see 
            //if it has a protocol so we can get a list of visits and let the user associate the session with a visit.
            //For now, we'll just assume the protocol is valid and pass it through to the importer.
            if (!Strings.isNullOrEmpty(protocolLabel)) {
                logger.trace("protocol: {}", protocolLabel);
                params.put(SessionVariableNames.PROTOCOL_LABEL, new AssignedSessionVariable(SessionVariableNames.PROTOCOL_LABEL, protocolLabel));
            }

            final String expectedModality = getParameter(EXPECTED_MODALITY);
            if (!Strings.isNullOrEmpty(expectedModality)) {
                logger.trace("expected modality: {}", expectedModality);
                params.put(Constants.EXPECTED_MODALITY_LABEL, expectedModality);

            }

            final String sessionLabel = getParameter(XNAT_SESSION);
            if (!Strings.isNullOrEmpty(sessionLabel)) {
                logger.trace("session: {}", sessionLabel);
                params.put(PREDEF_SESSION, new AssignedSessionVariable(SessionVariableNames.SESSION_LABEL, sessionLabel));
            }

            final String warnOnDupeSessionLabels = getParameter(SessionVariableNames.WARN_ON_DUPE_SESSION_LABELS);
            if (!Strings.isNullOrEmpty(warnOnDupeSessionLabels)) {
                logger.trace("Warn on dupe session labels: {}", warnOnDupeSessionLabels);
                params.put(SessionVariableNames.WARN_ON_DUPE_SESSION_LABELS, new AssignedSessionVariable(SessionVariableNames.WARN_ON_DUPE_SESSION_LABELS, warnOnDupeSessionLabels));
            }

            final String allowOverwriteOnDupeSessionLabels = getParameter(SessionVariableNames.ALLOW_OVERWRITE_ON_DUPE_SESSION_LABELS);
            if (!Strings.isNullOrEmpty(allowOverwriteOnDupeSessionLabels)) {
                logger.trace("Allow overwrite on dupe session labels: {}", allowOverwriteOnDupeSessionLabels);
                params.put(SessionVariableNames.ALLOW_OVERWRITE_ON_DUPE_SESSION_LABELS, new AssignedSessionVariable(SessionVariableNames.ALLOW_OVERWRITE_ON_DUPE_SESSION_LABELS, allowOverwriteOnDupeSessionLabels));
            }
            final String allowAppendOnDupeSessionLabels = getParameter(SessionVariableNames.ALLOW_APPEND_ON_DUPE_SESSION_LABELS);
            if (!Strings.isNullOrEmpty(allowAppendOnDupeSessionLabels)) {
                logger.trace("Allow append on dupe session labels: {}", allowAppendOnDupeSessionLabels);
                params.put(SessionVariableNames.ALLOW_APPEND_ON_DUPE_SESSION_LABELS, new AssignedSessionVariable(SessionVariableNames.ALLOW_APPEND_ON_DUPE_SESSION_LABELS, allowAppendOnDupeSessionLabels));
            }

            // Allow the system configuration to override the session date confirmation page, but default to showing it.
            final String scanDate = getParameter(XNAT_SCAN_DATE);
            if (UIUtils.getConfirmSessionDatePage()) {
                if ("no_session_date".equals(scanDate)) {
                    params.put(ConfirmSessionDatePage.PRODUCT_NAME, "no_session_date");
                } else {
                    Date date = UIUtils.parseDate(scanDate);
                    if (date == null) {
                        pages.add(new ConfirmSessionDatePage());
                        logger.trace("confirm date");
                    } else {
                        params.put(ConfirmSessionDatePage.PRODUCT_NAME, date);
                    }
                }
            }
            pages.add(new SelectFilesPage());
            logger.trace("files");
            if (StringUtils.isBlank(projectName)) {
                pages.add(new SelectSessionPage(xnat));
            } else {
                pages.add(new SelectSessionPage(xnat, projectName));
            }
            logger.trace("session");
            pages.add(new AssignSessionVariablesPage());
            logger.trace("session variables");
            logger.trace("{} creating wizard", this);
            final Wizard wizard = WizardPage.createWizard(pages.toArray(new WizardPage[pages.size()]), new UploadWizardResultProducer(Executors.newCachedThreadPool()));
            logger.trace("{} installing displayer", this);
            final WizardDisplayer displayer = WizardDisplayer.installInContainer(this, null, wizard, null, params, this);
            displayer.setCloseHandler(closeHandler);
        } catch (Throwable t) {
            logger.error("wizard installation failed", t);
            final StringWriter sw = new StringWriter();
            final PrintWriter writer = new PrintWriter(sw);
            writer.println("Applet startup failed; please contact " + getParameter(XNAT_ADMIN_EMAIL) + " for help.");
            writer.println("Error details:");
            t.printStackTrace(writer);
            final JTextArea text = new JTextArea(sw.toString());
            text.setEditable(false);
            add(text);
            validate();
        }

        logger.trace("{} setting JavaScript context", this);
        Constants.setJSContext(getJSContext());

        final String windowName = getParameter(WINDOW_NAME);
        if (StringUtils.isNotBlank(windowName)) {
            logger.trace("Setting window name: {}", windowName);
            Constants.setWindowName(windowName);
        }

        logger.trace("{} ready", this);
    }

    /**
     * Implementation of the {@link Applet#stop()} method.
     *
     * @see java.applet.Applet#stop()
     */
    @Override
    public void stop() {
        logger.trace("STOP {}", this);
    }

    /**
     * Implementation of the {@link WizardResultReceiver#cancelled(java.util.Map)} method.
     *
     * @see org.netbeans.api.wizard.WizardResultReceiver#cancelled(java.util.Map)
     */
    @Override
    public void cancelled(@SuppressWarnings("rawtypes") Map arg0) {
        try {
            getAppletContext().showDocument(new URL(getParameter(XNAT_URL)));
        } catch (MalformedURLException mue) {
            exit();
        }
    }

    /**
     * Implementation of the {@link WizardResultReceiver#finished(Object)} method.
     *
     * @see org.netbeans.api.wizard.WizardResultReceiver#finished(java.lang.Object)
     */
    @Override
    public void finished(Object arg0) {
    }

    /**
     * Initializes the configuration for the REST server.
     *
     * @return An initialized {@link org.nrg.net.RestServer} object.
     */
    private RestServer initializeRestServer() {
        RestServer xnat = null;
        try {
            String xnatUrl = getParameter(XNAT_URL);
            xnat = new RestServer(xnatUrl, new JSESSIONIDCookie(getParameter(JSESSIONID)));
            logger.info("Started applet running with REST services at: " + xnatUrl);
        } catch (Throwable t) {
            displayError(t);
        }
        return xnat;
    }

    private void displayError(final Throwable t) {
        logger.error("applet startup failed", t);
        final StringWriter sw = new StringWriter();
        final PrintWriter writer = new PrintWriter(sw);
        writer.println("Applet startup failed; please contact " + getParameter(XNAT_ADMIN_EMAIL) + " for help.");
        writer.println("Error details: " + t.getMessage());
        t.printStackTrace(writer);
        final JTextArea text = new JTextArea(sw.toString());
        text.setEditable(false);
        add(text);
        validate();
    }

    /**
     * Load the UIManager system look and feel.
     */
    private void configureLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Loads logging resources, including loading logging properties from custom URLs specified by the
     * {@link #LOG4J_PROPS_URL} applet parameter.
     */
    private void configureLogging() {
        final String log4jProps = getParameter(LOG4J_PROPS_URL);
        if (StringUtils.isNotBlank(log4jProps)) {
            try {
                PropertyConfigurator.configure(new URL(log4jProps));
            } catch (MalformedURLException e) {
                logger.error("Unable to read remote log4j configuration file " + log4jProps, e);
            }
        }
    }

    /**
     * Loads applet properties from the <b>applet.properties</b>properties file. This
     * can be overridden by placing a custom version of the properties file ahead of
     * the applet jar on the classpath.
     *
     * @param properties The system properties for the applet.
     */
    private void loadAppletProperties(Properties properties) {
        InputStream appletProperties = null;

        try {
            appletProperties = getClass().getResourceAsStream("/applet.properties");
            if (appletProperties != null) {
                properties.load(appletProperties);
            }
        } catch (IOException exception) {
            logger.info("Unable to find the applet.properties resource for initialization", exception);
        } finally {
            if (appletProperties != null) {
                try {
                    appletProperties.close();
                } catch (IOException e) {
                    // Whatever.
                }
            }
        }
    }

    /**
     * Loads properties from custom properties specified in the {@link #CUSTOM_PROPS_URL}
     * applet parameter. If this parameter is not specified, no properties are loaded.
     *
     * @param properties The system properties for the applet.
     */
    private void loadCustomProperties(Properties properties) {
        final String customProps = getParameter(CUSTOM_PROPS_URL);
        if (StringUtils.isNotBlank(customProps)) {
            InputStream customProperties = null;
            try {
                customProperties = getClass().getResourceAsStream(customProps);
                properties.load(customProperties);
            } catch (IOException exception) {
                logger.info("Unable to find the custom properties resource " + customProps + " for initialization", exception);
            } finally {
                if (customProperties != null) {
                    try {
                        customProperties.close();
                    } catch (IOException ignored) {
                        // Once again, whatever.
                    }
                }
            }
        }
    }

    /**
     * Builds a map of the incoming parameters for managing the wizard.
     *
     * @param xnat The XNAT REST server, as initialized in the {@link #initializeRestServer()} method.
     * @return The map of parameters for the wizard.
     */
    private Map<String, Object> buildParamsMap(final RestServer xnat) {
        final Map<String, Object> params = Maps.newLinkedHashMap();
        params.put(Constants.XNAT_REST_API_WIZ_PARAM, xnat);
        params.put(Constants.XNAT_URL_WIZ_PARAM, xnat.getURL());
        params.put(Constants.XNAT_ADMIN_EMAIL_WIZ_PARAM, getParameter(XNAT_ADMIN_EMAIL));
        params.put(Constants.FIXED_SIZE_STREAMING_WIZ_PARAM, Boolean.valueOf(getParameter(USE_FIXED_SIZE_STREAMING, "false")));
        params.put(Constants.N_UPLOAD_THREADS_WIZ_PARAM, Integer.valueOf(getParameter(N_UPLOAD_THREADS, "1")));
        return params;
    }

    /**
     * Retrieves the Javascript object context if available.
     *
     * @return The Javascript object if available. Returns null if not available (e.g. if running in a debugger or
     *         non-Javascript-enabled browser.
     */
    private JSObject getJSContext() {
        final Callable<JSObject> getWindow = new Callable<JSObject>() {
            public JSObject call() throws JSException {
                return JSObject.getWindow(UploadAssistantApplet.this);
            }
        };
        final ExecutorService es = Executors.newSingleThreadExecutor();
        try {
            return es.invokeAny(Collections.singleton(getWindow), 10, TimeUnit.SECONDS);
        } catch (Exception e) {
            logger.warn("Unable to retrieve JavaScript window context, possibly running in non-browser-hosted mode like appletviewer?");
            return null;
        }
    }

    /**
     * Cleans up the Javascript context for clean exit.
     */
    private void exit() {
        final JSObject context = getJSContext();
        if (null == context) {
            System.err.println("javascript close failed");
            // this usually means we're in a non-browser applet viewer
        } else {
            context.call("close");
        }
    }

    /**
     * Creates a listener to handle the closing of the applet. This allows for a clean exit.
     */
    private transient final ActionListener closeHandler = new ActionListener() {
        public void actionPerformed(final ActionEvent e) {
            try {
                getAppletContext().showDocument(new URL(getParameter(XNAT_URL)));
            } catch (MalformedURLException mue) {
                exit();
            }
        }
    };

    /**
     * Retrieves a parameter from the applet start-up parameters.
     *
     * @param key          The name of the parameter to retrieve.
     * @param defaultValue The default value to be returned if the parameter is not found.
     * @return The value of the parameter if found, the default value otherwise.
     */
    private String getParameter(final String key, final String defaultValue) {
        final String v = getParameter(key);
        return null == v ? defaultValue : v;
    }

    private static final Logger logger = LoggerFactory.getLogger(UploadAssistantApplet.class);

    private static final String XNAT_PROJECT = "xnat-project";
    private static final String XNAT_SUBJECT = "xnat-subject";
    private static final String XNAT_SCAN_DATE = "xnat-scan-date";
    private static final String XNAT_VISIT = "xnat-visit";
    private static final String XNAT_PROTOCOL = "xnat-protocol";
    private static final String EXPECTED_MODALITY = "expected-modality";
    private static final String XNAT_SCAN_TYPE = "xnat-scan-type";
    private static final String XNAT_SESSION = "xnat-session-label";
    private static final String XNAT_URL = "xnat-url";
    private static final String XNAT_DESCRIPTION = "xnat-description";
    private static final String XNAT_ADMIN_EMAIL = "xnat-admin-email";
    private static final String LOG4J_PROPS_URL = "log4j-properties-url";
    private static final String CUSTOM_PROPS_URL = "custom-properties-url";
    private static final String ENABLE_REMOTE_LOGGING = "enable-remote-logging";
    private static final String REMOTE_LOGGING_PATH = "remote-logging-path";
    private static final String USE_FIXED_SIZE_STREAMING = "fixed-size-streaming";
    private static final String N_UPLOAD_THREADS = "n-upload-threads";
    private static final String WINDOW_NAME = "window-name";
    private static final String JSESSIONID = "jsessionid";
    private static final String AUTHORS = "Author: Kevin A. Archie <karchie@wustl.edu>, Rick Herrick <rick.herrick@wustl.edu>";
    private static final String COPYRIGHT = String.format("Copyright (c) 2008-%d Washington University School of Medicine", Calendar.getInstance().get(Calendar.YEAR));
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String APPLET_INFO = Joiner.on(LINE_SEPARATOR).join(AUTHORS, COPYRIGHT);
    private static final String DEV_USER = "dev-user";
    private static final String DEV_PASS = "dev-pass";

    private static final String[][] PARAMETER_INFO = {
            {XNAT_URL, "URL", "Base URL for XNAT instance"},
            {XNAT_DESCRIPTION, "string", "Human-readable name of the XNAT instance"},
            {XNAT_ADMIN_EMAIL, "email address", "Email address for the XNAT administrator"},
            {SessionVariableNames.WARN_ON_DUPE_SESSION_LABELS, "boolean", "Indicates whether the applet should warn when session labels are specified that are duplicates of existing session labels in the target project; defaults to true."},
            {SessionVariableNames.ALLOW_OVERWRITE_ON_DUPE_SESSION_LABELS, "boolean", "Indicates whether the applet should allow the user to select the overwrite option when session labels are specified that are duplicates of existing session labels in the target project; defaults to false."},
            {USE_FIXED_SIZE_STREAMING, "boolean", "Should the applet use fixed-size streaming for data upload?"},
            {N_UPLOAD_THREADS, "integer", "Number of threads to use for uploading"},
            {LOG4J_PROPS_URL, "URL", "URL for log4j properties file"},
            {CUSTOM_PROPS_URL, "URL", "URL for custom properties file"},
            {ENABLE_REMOTE_LOGGING, "boolean", "Indicates whether the applet should use remote logging"},
            {REMOTE_LOGGING_PATH, "string", "Path to the REST service on the remote server to handle logging"},
            {XNAT_PROJECT, "string", "Indicates the project to which the uploaded resource should be added"},
            {XNAT_SUBJECT, "string", "Indicates the subject to which the uploaded resource should be added"},
            {XNAT_SCAN_DATE, "string", "Indicates the date the scan to be uploaded was acquired"},
            {XNAT_VISIT, "string", "The visit label for an existing pVisitData to which you would like to add these images"},
            {XNAT_PROTOCOL, "string", "The protocol label for an experiment to differentiate identical XSI types within the same pVisitData"},
            {EXPECTED_MODALITY, "string", "The modality you expect to upload. A warning message will appear if the image's modality doesn't match this string"},
            {XNAT_SCAN_TYPE, "string", "The type of scan to be uploaded"},
            {WINDOW_NAME, "string", "The name of the window running the applet. Defaults to applet."},
            {JSESSIONID, "string", "The JSESSIONID to authenticate the REST calls"},
    };

    private class DevAuthenticator extends Authenticator {
        public DevAuthenticator(String user, String pass) {
            _user = user;
            _pass = pass;
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(_user, _pass.toCharArray());
        }

        private final String _user, _pass;
    }
}
