/*
 * org.nrg.upload.ui.UploadWizardResultProducer
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.ui;

import org.netbeans.spi.wizard.DeferredWizardResult;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;
import org.nrg.upload.data.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class UploadWizardResultProducer implements WizardResultProducer {

    public UploadWizardResultProducer(final ExecutorService executorService) {
        _executorService = executorService;
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.WizardPage.WizardResultProducer#cancel(java.util.Map)
     */
    @SuppressWarnings("rawtypes")
    public boolean cancel(final Map arg0) { return true; }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.WizardPage.WizardResultProducer#finish(java.util.Map)
     */
    @SuppressWarnings("rawtypes")
    public Object finish(final Map arg0){
        return new UploadWizardResult();
    }

    private class UploadWizardResult extends DeferredWizardResult {
        private final Logger logger = LoggerFactory.getLogger(UploadWizardResult.class);

        private Future<Boolean> upload = null;

        UploadWizardResult() {
            super(true);
        }

        /*
         * (non-Javadoc)
         * @see org.netbeans.spi.wizard.DeferredWizardResult#start(java.util.Map, org.netbeans.spi.wizard.ResultProgressHandle)
         */
        @SuppressWarnings("rawtypes")
        @Override
        public void start(final Map wizardData, final ResultProgressHandle progress) {
            try {
                final Session session = ((Session)wizardData.get(SelectSessionPage.PRODUCT_NAME));
                logger.trace("Wizard parameter map {}", wizardData);
                // Analytics.enter(UploadAssistant.class, String.format("Upload commenced with wizard parameter map %s", wizardData));
                upload = _executorService.submit(new Callable<Boolean>() {
                    public Boolean call() {
                        return session.uploadTo(wizardData, new SwingUploadFailureHandler(), progress);
                    }
                });
                upload.get();
            } catch (final CancellationException ignored) {
                progress.failed("Upload canceled", false);
            } catch (final Throwable throwable) {
                System.out.println("upload failed: " + throwable);
                throwable.printStackTrace();
                logger.error("Something went wrong during upload", throwable);
                progress.failed(throwable.getMessage(), false);
            }
        }

        /*
         * (non-Javadoc)
         * @see org.netbeans.spi.wizard.DeferredWizardResult#abort()
         */
        @Override
        public void abort() {
            if (null != upload) { upload.cancel(true); }
        }
    }

    private final ExecutorService _executorService;
}
