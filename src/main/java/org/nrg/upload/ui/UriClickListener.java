package org.nrg.upload.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URI;

public class UriClickListener implements MouseListener {

    private final URI _uri;

    public UriClickListener(final URI uri) {
        _uri = uri;
    }

    @Override
    public void mouseClicked(final MouseEvent event) {
        try {
            final Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.BROWSE)) {
                desktop.browse(_uri);
            } else {
                UIUtils.handleUrlClick(event.getComponent(), _uri.toString());
            }
        } catch (IOException e) {
            UIUtils.handleAppletError(event.getComponent(), "An error occurred when the URI was clicked: " + e.getMessage(), "Error Opening Page", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void mousePressed(final MouseEvent event) {

    }

    @Override
    public void mouseReleased(final MouseEvent event) {

    }

    @Override
    public void mouseEntered(final MouseEvent event) {

    }

    @Override
    public void mouseExited(final MouseEvent event) {

    }
}
