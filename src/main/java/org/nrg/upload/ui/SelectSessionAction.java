/*
 * org.nrg.upload.ui.SelectSessionAction
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.ui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.nrg.upload.data.Session;

public final class SelectSessionAction extends AbstractAction implements Action {
	private static final long serialVersionUID = 1L;
	private final SessionSelectionListener listener;
	private final Session session;
	
	/**
	 * Creates a new select session action.
	 * @param listener    The listener for the action.
	 * @param session     The session to handle.
	 */
	public SelectSessionAction(final SessionSelectionListener listener, final Session session) {
		this.listener = listener;
		this.session = session;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(final ActionEvent e) {
		listener.isSelected(session);
	}
	
	public interface SessionSelectionListener {
		void isSelected(Session session);
	}

}
