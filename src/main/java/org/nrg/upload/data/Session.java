/*
 * org.nrg.upload.data.Session
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.data;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.netbeans.spi.wizard.ResultProgressHandle;

public interface Session {
	String getID();
	String getAccession();
	Date getDateTime();
	String getDescription();
	int getScanCount();
	int getFileCount();
	long getSize();
	Set<String> getModalities();
	String getFormat();
	List<SessionVariable> getVariables(Map<?,?> params);
	boolean uploadTo(Map<?,?> params, UploadFailureHandler handler, ResultProgressHandle progress);
	TimeZone getTimeZone();
}
