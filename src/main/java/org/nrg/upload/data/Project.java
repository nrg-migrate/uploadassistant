/*
 * org.nrg.upload.data.Project
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.upload.data;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.nrg.dcm.IndexedSessionLabelFunction;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.net.RestServer;
import org.nrg.net.xnat.*;
import org.nrg.upload.ui.SelectSessionPage;
import org.nrg.util.PrearchiveCode;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

public class Project {
    // IMPORTANT: This must stay in sync with the AUTO_ARCHIVE constant defined in org.nrg.xnat.archive.GradualDicomImporter.
    public static final String AUTO_ARCHIVE = "autoArchive";

	private final ExecutorService executor = Executors.newCachedThreadPool();
	private final String name;
	private final RestServer xnat;
	private final Future<Map<String,String>> subjects;
	private final Set<Subject> newSubjects = Sets.newLinkedHashSet();	// locally added subjects
	private final Future<Map<String,String>> sessions;
	private final Future<Iterable<org.nrg.dcm.edit.ScriptApplicator>> dicomScriptApplicator;
	private final Future<org.nrg.ecat.edit.ScriptApplicator> ecatScriptApplicator;
    private final Future<Set<String>> petTracers;
    private final Future<PrearchiveCode> prearchiveCode;

	public Project(final String name, final RestServer xnat, final Map<?,?> wizardParams) {
		this.name = name;
		this.xnat = xnat;
		sessions = executor.submit(new ProjectSessionLister(xnat, name));
		subjects = executor.submit(new ProjectSubjectLister(xnat, name));

		dicomScriptApplicator = executor.submit(new DicomScriptApplicatorRetriever(xnat, name,
				getDicomFunctions(sessions)));
		ecatScriptApplicator = executor.submit(new ECATScriptApplicatorRetriever(xnat, name,
				getEcatFunctions(sessions, wizardParams)));
        petTracers = executor.submit(new PETTracerRetriever(xnat, name));
        prearchiveCode = executor.submit(new ProjectPreArcCodeRetriever(xnat, name));
	}

	/**
	 * Adds a (newly created) subject to the project.
	 * @param subject The subject to add to the project.
	 */
	public void addSubject(final Subject subject) { newSubjects.add(subject); }

	private static Map<String, ScriptFunction>
	getDicomFunctions(final Future<Map<String,String>> sessions) {
		return Collections.singletonMap("makeSessionLabel",
				(ScriptFunction) new IndexedSessionLabelFunction(sessions));
	}

	private static Map<String,? extends org.nrg.ecat.edit.ScriptFunction>
	getEcatFunctions(final Future<Map<String,String>> sessions, final Map<?,?> wizardParams) {
		final Map<String,org.nrg.ecat.edit.ScriptFunction> m = Maps.newHashMap();
		m.put("makeSessionLabel", new org.nrg.ecat.IndexedSessionLabelFunction(sessions));
		m.put("formatSessionDate", new org.nrg.ecat.FormatSessionDateFunction(
				new Callable<Session>() {
					public Session call() throws Exception {
						return (Session)wizardParams.get(SelectSessionPage.PRODUCT_NAME);
					}
				}));
		return m;
	}

	public RestServer getRestServer() { return xnat; }

	public Collection<Subject> getSubjects()
	throws ExecutionException,InterruptedException {
		final Collection<Subject> s = Sets.newLinkedHashSet();
		for (final Map.Entry<String,String> me : subjects.get().entrySet()) {
			s.add(new Subject(me.getKey(), me.getValue()));
		}
		s.addAll(newSubjects);
		return s;
	}

	public boolean hasSubject(final String name) throws ExecutionException, InterruptedException {
		for (final Subject subject : getSubjects()) {
			if (name.equals(subject.getLabel())) {
				return true;
			}
		}
		return false;
	}

	public Map<String,String> getSessionLabels()
	throws InterruptedException,ExecutionException {
		return sessions.get();
	}

	public Iterable<org.nrg.dcm.edit.ScriptApplicator> getDicomScriptApplicators()
	throws InterruptedException,ExecutionException {
		return dicomScriptApplicator.get();
	}

	public org.nrg.ecat.edit.ScriptApplicator getEcatScriptApplicator()
	throws InterruptedException,ExecutionException {
		return ecatScriptApplicator.get();
	}

    public Set<String> getPETTracers() throws InterruptedException,ExecutionException {
        return petTracers.get();
    }

	public PrearchiveCode getPrearchiveCode() throws InterruptedException,ExecutionException {
	    return prearchiveCode.get();
	}

	public void dispose() {
		sessions.cancel(true);
		subjects.cancel(true);
		dicomScriptApplicator.cancel(true);
		ecatScriptApplicator.cancel(true);
	}

	/**
	 * Submits a job to this project's executor service.
	 * @param callable    The callable to passed to the executor.
	 * @param <T>         Return type for the callable method.
	 * @return Returns the reference to the asynchronous results.
	 */
	public <T> Future<T> submit(Callable<T> callable) {
		return executor.submit(callable);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() { return name; }
}
