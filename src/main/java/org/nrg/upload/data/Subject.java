/*
 * org.nrg.upload.data.Subject
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.data;

import com.google.common.base.Objects;
import org.apache.commons.lang.StringUtils;

import java.util.Comparator;


public final class Subject {
    private final String label, id;

    public Subject(final String label, final String id) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public String getId() {
        return id;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(final Object o) {
        return o instanceof Subject
                && Objects.equal(label, ((Subject) o).label)
                && Objects.equal(id, ((Subject) o).id);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return Objects.hashCode(label, id);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return label;
    }

    public static class SubjectComparator implements Comparator<Subject> {
        @Override
        public int compare(final Subject subject1, final Subject subject2) {
            if (subject1.equals(subject2)) {
                return 0;
            }

            final String name1 = subject1.getLabel();
            final String name2 = subject2.getLabel();

            final boolean isName1Blank = StringUtils.isBlank(name1);
            final boolean isName2Blank = StringUtils.isBlank(name2);

            if (isName1Blank && isName2Blank) {
                return 0;
            }
            if (isName1Blank) {
                return -1;
            }
            if (isName2Blank) {
                return 1;
            }

            return name1.compareTo(name2);
        }
    }
}
