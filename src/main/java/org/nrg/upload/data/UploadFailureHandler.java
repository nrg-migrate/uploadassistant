/*
 * org.nrg.upload.data.UploadFailureHandler
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.data;

public interface UploadFailureHandler {
    boolean shouldRetry(Object item, Object cause);
}
