/*
 * org.nrg.upload.data.AssignedSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.upload.data;

import javax.swing.JLabel;

public final class AssignedSessionVariable extends AbstractSessionVariable {
	private final String value;
	private final JLabel label;
	private final boolean hidden;
	
	public AssignedSessionVariable(final String name, final String value, final boolean hidden) {
		super(name);
		this.value = value;
		this.label = new JLabel(value);
		this.hidden = hidden;
	}
	
	public AssignedSessionVariable(final String name, final String value) {
		this(name, value, false);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.upload.data.SessionVariable#getEditor()
	 */
	public JLabel getEditor() { return label; }
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.upload.data.SessionVariable#getValue()
	 */
	public String getValue() { return value; }
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.upload.data.SessionVariable#getValueMessage()
	 */
	public String getValueMessage() { return null; }
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.upload.data.SessionVariable#isHidden()
	 */
	public boolean isHidden() { return hidden; }
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.upload.data.SessionVariable#refresh()
	 */
	public void refresh() {}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.upload.data.SessionVariable#setValue(java.lang.String)
	 */
	public String setValue(final String v) {
		throw new UnsupportedOperationException();
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		final StringBuilder sb = new StringBuilder(super.toString());
		sb.append(" ").append(getName()).append(" = ").append(value);
		return sb.toString();
	}
}
