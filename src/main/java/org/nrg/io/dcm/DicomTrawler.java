/*
 * org.nrg.io.dcm.DicomTrawler
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.io.dcm;

import org.apache.commons.lang.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.StopTagInputHandler;
import org.nrg.dcm.DicomUtils;
import org.nrg.dcm.Series;
import org.nrg.dcm.Study;
import org.nrg.io.Trawler;
import org.nrg.net.xnat.SeriesImportFilterApplicatorRetriever;
import org.nrg.upload.data.Session;
import org.nrg.upload.ui.SessionReviewPanel;
import org.nrg.util.EditProgressMonitor;
import org.nrg.util.MapRegistry;
import org.nrg.util.Registry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public final class DicomTrawler implements Trawler {

    public static final int MAX_TAG = Collections.max(new ArrayList<Integer>() {{
        add(Tag.AcquisitionNumber);
        add(Tag.SOPClassUID);
        add(Tag.SeriesDescription);
    }});

    public static final int APP_MAX_TAG = Collections.max(new ArrayList<Integer>() {{
        add(MAX_TAG);
        add(Series.MAX_TAG);
        add(SessionReviewPanel.MAX_TAG);
        add(Study.MAX_TAG);
        add(ZipSeriesUploader.MAX_TAG);
    }});

    private SeriesImportFilterApplicatorRetriever _filters;
    private final Logger logger = LoggerFactory.getLogger(DicomTrawler.class);

	/* (non-Javadoc)
	 * @see org.nrg.io.Trawler#trawl(java.util.Iterator, java.util.Collection)
	 */
	public Collection<Session> trawl(final Iterator<File> files, final Collection<File> remaining, EditProgressMonitor pm) {
		final Registry<Study> studies = new MapRegistry<>();
		while (files.hasNext()) {
			if (null != pm && pm.isCanceled()) {
				return new ArrayList<>();
			}
			final File f = files.next();
			if (f.isFile()) {
				final DicomObject object;
				try {
					object = DicomUtils.read(f, new StopTagInputHandler(APP_MAX_TAG + 1)); // We don't need anything higher than this tag.
				} catch (IOException e) {
					remaining.add(f);
					continue;
				} catch (Exception e) {
					remaining.add(f);
					continue;
				}
				assert null != object.getString(Tag.SOPClassUID);

                if (_filters != null) {
                    final String description = object.getString(Tag.SeriesDescription);
                    logger.debug("Found series import filters, testing series for inclusion/exclusion and suggested modality: {}", description);
                    if (_filters.shouldIncludeDicomObject(object)) {
                        logger.info("DICOM object {} allowed in by import filters, including in session", description);
                        final Study study;
                        if (_filters.shouldSeparatePetMr()) {
                            final String modality = _filters.findModality(object);
                            study = StringUtils.isBlank(modality) ? studies.get(new Study(object)) : studies.get(new Study(object, modality));
                        } else {
                            study = studies.get(new Study(object));
                        }
                        study.getSeries(object, f);
                    } else {
                        logger.info("Series description {} was excluded by series import filter restrictions, excluding from session", description);
                    }
                } else {
                    logger.info("Series import filters not found, including series in session");
                    final Study study = studies.get(new Study(object));
                    study.getSeries(object, f);
                }
            }
		}
		
		return new ArrayList<Session>(studies.getAll());
	}

    public void setSeriesImportFilters(final SeriesImportFilterApplicatorRetriever filters) {
        _filters = filters;
    }
}
