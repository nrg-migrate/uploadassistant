/**
 * Copyright (c) 2014 Washington University School of Medicine
 */
package org.nrg.io.dcm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.data.UID;
import org.dcm4che2.io.DicomCodingException;
import org.dcm4che2.io.DicomInputHandler;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.io.StopTagInputHandler;
import org.nrg.dcm.Series;
import org.nrg.dcm.edit.AttributeException;
import org.nrg.dcm.edit.ScriptApplicator;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.io.ByteStreams;

/**
 * @author Kevin A. Archie, karchie@wustl.edu
 */
@SuppressWarnings("Duplicates")
public class SeriesZipper {
    private final Logger logger = LoggerFactory.getLogger(SeriesZipper.class);
    
    private final Iterable<ScriptApplicator> applicators;
    private final StopTagInputHandler stopTagInputHandler;
    
    public SeriesZipper(final Iterable<ScriptApplicator> scriptApplicators) {
        this.applicators = ImmutableList.copyOf(scriptApplicators);
        this.stopTagInputHandler = makeStopTagInputHandler(this.applicators);
    }

    private static String removeCompressionSuffix(final String path) {
        return path.replaceAll("\\.[gG][zZ]$", "");
    }
    
    private static StopTagInputHandler makeStopTagInputHandler(final Iterable<ScriptApplicator> scriptApplicators) {
        long top = Tag.SOPInstanceUID;
        for (final ScriptApplicator a : scriptApplicators) {
            final long atop = 0xffffffffL & a.getTopTag();
            if (atop > top) {
                if (0xffffffffL == atop) {  // this means no stop tag
                    return null;
                } else {
                    top = atop;
                }
            }
        }
        return new StopTagInputHandler((int)(top+1));
    }
    
    public StopTagInputHandler getStopTagInputHandler() {
        return stopTagInputHandler;
    }
    
    public void buildSeriesZipFile(final File f, final Series series)
            throws IOException, AttributeException, ScriptEvaluationException {
        logger.debug("creating zip file {}", f);
        IOException ioexception = null;
        final FileOutputStream fos = new FileOutputStream(f);
        try {
            final ZipOutputStream zos = new ZipOutputStream(fos);
            try {
                logger.trace("adding {} files for series {}", series.getFileCount(), series);
                for (final File file : series.getFiles()) {
                    addFileToZip(file, zos, getStopTagInputHandler());
                }
            } catch (IOException e) {
                logger.trace("I/O exception building zipfile", e);
                throw ioexception = e;
            } finally {
                try {
                    zos.close();
                } catch (IOException e) {
                    if (null == ioexception) {
                        throw ioexception = e;
                    } else {
                        logger.error("unable to close series zip stream", e);
                        throw ioexception;
                    }
                }
            }
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                if (null == ioexception) {
                    throw ioexception = e;
                } else {
                    logger.error("unable to close series zip file", e);
                    throw ioexception;
                }
            }
        }
        logger.debug("zip file built");
    }
    
    public File buildSeriesZipFile(final Series series)
            throws IOException, AttributeException, ScriptEvaluationException {
        final File zipf = File.createTempFile("series", ".zip");
        try {
            buildSeriesZipFile(zipf, series);
            return zipf;
        } catch (IOException | AttributeException | ScriptEvaluationException | RuntimeException | Error e) {
            //noinspection ResultOfMethodCallIgnored
            zipf.delete();
            throw e;
        }
    }

    public void addFileToZip(final File f, final ZipOutputStream zos, final DicomInputHandler handler)
    throws AttributeException,IOException,ScriptEvaluationException {
        final long remainder;
        IOException ioexception = null;
        final FileInputStream fin = new FileInputStream(f);
        final BufferedInputStream bis = new BufferedInputStream(f.getName().endsWith(".gz") ? new GZIPInputStream(fin) : fin);
        try {
            final DicomInputStream dis = new DicomInputStream(bis);
            try {
                if (null != handler) {
                    dis.setHandler(handler);
                }
                final DicomObject o = dis.readDicomObject();
                for (final ScriptApplicator a : applicators) {
                    a.apply(f, o);
                }
                final String tsuid = o.getString(Tag.TransferSyntaxUID, UID.ImplicitVRLittleEndian);
                final TransferSyntax tsOriginal = TransferSyntax.valueOf(tsuid);
                if (tsOriginal.deflated() && null != handler) {
                    // Can't do a simple binary copy with deflated transfer syntax.
                    // Use no stop handler because we have to deserialize and serialize the entire object.
                    try {
                        dis.close();
                    } catch (IOException ignore) {}
                    addFileToZip(f, zos, null);
                    return;
                }
                final TransferSyntax ts = tsOriginal.deflated() ? TransferSyntax.ExplicitVRLittleEndian : tsOriginal;
                
                zos.putNextEntry(new ZipEntry(removeCompressionSuffix(f.getPath())));
                @SuppressWarnings("resource")
                final DicomOutputStream dos = new DicomOutputStream(zos);
                dos.setAutoFinish(false);          // Don't let DicomOutputStream finish the ZipOutputStream.
                final DicomObject fmi = new BasicDicomObject();
                fmi.initFileMetaInformation(o.getString(Tag.SOPClassUID), o.getString(Tag.SOPInstanceUID), ts.uid());
                dos.writeFileMetaInformation(fmi);
                dos.writeDataset(o, ts);
                if (null != handler) {
                    bis.reset();
                    remainder = ByteStreams.copy(bis, zos);
                } else {
                    remainder = 0;
                }
            } catch (IOException e) {
                throw ioexception = e;
            } finally {
                try {
                    dis.close();
                } catch (IOException e) {
                    throw ioexception = (null == ioexception) ? e : ioexception;
                }
            }
        } catch (DicomCodingException e) {
            logger.debug("error reading " + f, e);
            return;
        } catch (IOException e) {
            throw ioexception = (null == ioexception) ? e : ioexception;
        } finally {
            try {
                bis.close();
            } catch (IOException e) {
                if (null == ioexception) {
                    ioexception = e;
                } else {
                    logger.error("error closing stream buffer", e);
                }
            }
            try {
                fin.close();
            } catch (IOException e) {
                if (null == ioexception) {
                    ioexception = e;
                } else {
                    logger.error("error closing input DICOM file", e);
                }
            }
            if (null != ioexception) {
                throw ioexception;
            }
        }

        zos.closeEntry();
        logger.trace("added {}, {} bytes streamed", f, remainder);
    }
}
