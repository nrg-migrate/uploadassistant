/*
 * org.nrg.io.dcm.ZipSeriesUploader
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.io.dcm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.zip.ZipOutputStream;

import org.dcm4che2.data.Tag;
import org.nrg.dcm.Series;
import org.nrg.dcm.edit.AttributeException;
import org.nrg.dcm.edit.ScriptApplicator;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.io.HttpUploadException;
import org.nrg.io.HttpUtils;
import org.nrg.io.UploadStatisticsReporter;
import org.nrg.net.RestServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

public class ZipSeriesUploader implements Callable<Set<String>> {
    public static final int MAX_TAG = Collections.max(ImmutableList.of(Tag.SOPInstanceUID,
            Tag.TransferSyntaxUID, Tag.FileMetaInformationVersion, Tag.SOPClassUID));

    private static final Set<Integer> successCodes = ImmutableSet.of(HttpURLConnection.HTTP_OK,
            HttpURLConnection.HTTP_ACCEPTED);
    private static final int BUF_SIZE = 4096;
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String CONTENT_TYPE_ZIP = "application/zip";

    private final Logger logger = LoggerFactory.getLogger(ZipSeriesUploader.class);
    private final URL url;
    private final Series series;
    private final SeriesZipper zipper;
    private final UploadStatisticsReporter progress;
    private final boolean useFixedSizeStreaming;
    private final RestServer xnat;

    public ZipSeriesUploader(final URL url,
            final boolean useFixedSizeStreaming,
            final Series series,
            final Iterable<ScriptApplicator> applicators,
            final UploadStatisticsReporter progress,
            final RestServer xnat) {
        this.url = url;
        this.useFixedSizeStreaming = useFixedSizeStreaming;
        this.series = series;
        this.zipper = new SeriesZipper(applicators);
        this.progress = progress;
        this.xnat = xnat;
    }

    public Set<String> call()
    throws AttributeException,HttpUploadException,IOException,ScriptEvaluationException {
        return useFixedSizeStreaming ? sendFixedSize() : sendChunked();
    }

    public Series getSeries() { return series; }

    
    public Set<String> sendFixedSize()
    throws AttributeException,HttpUploadException,IOException,ScriptEvaluationException {
        final File tempzip = zipper.buildSeriesZipFile(series);
        try {
            IOException ioexception = null;
            final long zipsize = tempzip.length();
            float zipRatio = (float)series.getSize() / zipsize;

            logger.trace("creating connection to {}", url);
            final HttpURLConnection c = (HttpURLConnection)url.openConnection();
            c.setRequestMethod("POST");
            c.setDoOutput(true);
            c.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE_ZIP);
            xnat.addAuthorization(c);
            c.setFixedLengthStreamingMode((int)zipsize);
            c.connect();

            try {
                logger.trace("connection open; starting data send");
                assert ioexception == null;
                final FileInputStream fis = new FileInputStream(tempzip);
                try {
                    final OutputStream os = c.getOutputStream();
                    try {
                        final byte[] buf = new byte[BUF_SIZE];
                        int chunk;
                        for (int total = 0; (chunk = fis.read(buf)) != -1; total += chunk) {
                            logger.trace("copying {} / {}", total, zipsize);
                            long adjustedChunkSize = (long)(chunk * zipRatio);
                            progress.addSent(adjustedChunkSize);
                            os.write(buf, 0, chunk);
                        }
                    } catch (IOException e) {
                        throw ioexception = e;
                    } finally {
                        try {
                            os.close();
                        } catch (IOException e) {
                            throw ioexception = null == ioexception ? e : ioexception;
                        }
                    }
                } finally {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        throw ioexception = null == ioexception ? e : ioexception;
                    }
                }
                if (successCodes.contains(c.getResponseCode())) {
                    logger.debug("series upload complete");
                    return Sets.newLinkedHashSet(HttpUtils.readEntityLines(c));
                } else {
                    throw new HttpUploadException(c);
                }
            } finally {
                c.disconnect();
            }
        } finally {
            tempzip.delete();
        }
    }

    public Set<String> sendChunked()
    throws AttributeException,HttpUploadException,IOException,ScriptEvaluationException {
        logger.warn("XNAT may not support chunked mode! After this fails, set fixed-size-streaming to true.");
        final HttpURLConnection c = (HttpURLConnection)url.openConnection();
        c.setRequestMethod("POST");
        c.setDoInput(true);
        c.setDoOutput(true);
        c.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE_ZIP);
        xnat.addAuthorization(c);
        c.setChunkedStreamingMode(0);
        if (logger.isDebugEnabled()) {
            logger.debug("Connecting to {}", url);
            logger.debug("Request properties:");
            for (final Map.Entry<String,List<String>> m : c.getRequestProperties().entrySet()) {
                logger.debug("  {}", m);
            }
        }
        c.connect();

        try {
            logger.trace("connection open; starting data send");
            IOException ioexception = null;
            final ZipOutputStream zos = new ZipOutputStream(c.getOutputStream());
            try {
                for (final File file : series.getFiles()) {
                    progress.addSent(file.length());
                    logger.trace("sending {}", file);
                    zipper.addFileToZip(file, zos, zipper.getStopTagInputHandler());
                }
            } catch (IOException e) {
                throw ioexception = e;
            } finally {
                try {
                    zos.close();
                } catch (IOException e) {
                    throw null == ioexception ? e : ioexception;
                }
            }
            if (successCodes.contains(c.getResponseCode())) {
                return Sets.newLinkedHashSet(HttpUtils.readEntityLines(c));
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Response {} {} headers:", c.getResponseCode(), c.getResponseMessage());
                    for (final Map.Entry<String,List<String>> m : c.getHeaderFields().entrySet()) {
                        logger.debug("  {}", m);
                    }
                }
                throw new HttpUploadException(c);
            }
        } finally {
            c.disconnect();
        }
    }
}
