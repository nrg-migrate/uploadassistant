/*
 * org.nrg.ecat.EcatSession
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.ecat;

import com.google.common.base.Strings;
import com.google.common.collect.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONException;
import org.json.JSONObject;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.Summary;
import org.nrg.ecat.edit.ScriptApplicator;
import org.nrg.ecat.edit.Variable;
import org.nrg.net.HttpException;
import org.nrg.net.HttpURLConnectionProcessor;
import org.nrg.net.JSONRequestConnectionProcessor;
import org.nrg.net.RestServer;
import org.nrg.upload.data.*;
import org.nrg.upload.ui.AssignSessionVariablesPage;
import org.nrg.upload.ui.SelectProjectPage;
import org.nrg.upload.ui.SelectSubjectPage;
import org.nrg.upload.ui.UploadResultPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.nrg.upload.ui.Constants.XNAT_ADMIN_EMAIL_WIZ_PARAM;
import static org.nrg.upload.ui.Constants.XNAT_REST_API_WIZ_PARAM;

public final class EcatSession implements Session {
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String FORMAT = "ECAT";
    private static final String MODALITY = "PET";
    private static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";

    private final Logger logger = LoggerFactory.getLogger(EcatSession.class);
    private final MatrixDataFile first;
    private final Collection<MatrixDataFile> data;
    private long size = 0;
    
    private TimeZone timeZone = null;
    
    private static final String [] timezones = new String[] {
		System.getProperty("user.timezone"),
		"America/New_York", 
		"America/Chicago", 
		"America/Los_Angeles",
		"Pacific/Midway",
	    "US/Hawaii",
	    "US/Alaska",
	    "US/Pacific",
	    "America/Tijuana",
	    "US/Arizona",
	    "America/Chihuahua",
	    "US/Mountain",
	    "America/Guatemala",
	    "US/Central",
	    "America/Mexico_City",
	    "Canada/Saskatchewan",
	    "America/Bogota",
	    "US/Eastern",
	    "US/East-Indiana",
	    "Canada/Eastern",
	    "America/Caracas",
	    "America/Manaus",
	    "America/Santiago",
	    "Canada/Newfoundland",
	    "Brazil/East",
	    "America/Buenos_Aires",
	    "America/Godthab",
	    "America/Montevideo",
	    "Atlantic/South_Georgia",
	    "Atlantic/Azores",
	    "Atlantic/Cape_Verde",
	    "Africa/Casablanca",
	    "Europe/London",
	    "Europe/Berlin",
	    "Europe/Belgrade",
	    "Europe/Brussels",
	    "Europe/Warsaw",
	    "Africa/Algiers",
	    "Asia/Amman",
	    "Europe/Athens",
	    "Asia/Beirut",
	    "Africa/Cairo",
	    "Africa/Harare",
	    "Europe/Helsinki",
	    "Asia/Jerusalem",
	    "Europe/Minsk",
	    "Africa/Windhoek",
	    "Asia/Baghdad",
	    "Asia/Kuwait",
	    "Europe/Moscow",
	    "Africa/Nairobi",
	    "Asia/Tbilisi",
	    "Asia/Tehran",
	    "Asia/Muscat",
	    "Asia/Baku",
	    "Asia/Yerevan",
	    "Asia/Kabul",
	    "Asia/Yekaterinburg",
	    "Asia/Karachi",
	    "Asia/Calcutta",
	    "Asia/Colombo",
	    "Asia/Katmandu",
	    "Asia/Novosibirsk",
	    "Asia/Dhaka",
	    "Asia/Rangoon",
	    "Asia/Bangkok",
	    "Asia/Krasnoyarsk",
	    "Asia/Hong_Kong",
	    "Asia/Irkutsk",
	    "Asia/Kuala_Lumpur",
	    "Australia/Perth",
	    "Asia/Taipei",
	    "Asia/Tokyo",
	    "Asia/Seoul",
	    "Asia/Yakutsk",
	    "Australia/Adelaide",
	    "Australia/Darwin",
	    "Australia/Brisbane",
	    "Australia/Sydney",
	    "Pacific/Guam",
	    "Australia/Hobart",
	    "Asia/Vladivostok",
	    "Asia/Magadan",
	    "Pacific/Auckland",
	    "Pacific/Fiji",
	    "Pacific/Tongatapu"		
    };
    
    @SuppressWarnings("unchecked")
    public EcatSession(final Collection<MatrixDataFile> files) {
        final List<MatrixDataFile> list = Lists.newArrayList(files);
        Collections.sort(list);
        this.first = files.isEmpty() ? null : list.get(0);
        this.data = Collections.unmodifiableCollection(list);
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getAccession()
     */
    public String getAccession() { return null; }

    
    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getDateTime()
     */
    public Date getDateTime() {
    	
    	// The first time this is called, the user is prompted to enter the timezone in which this
    	// session was acquired. Subsequent calls re-use the timezone that was selected.
    	
    	if(timeZone == null){
    		final JComboBox<String> list = new JComboBox<>(timezones);
    		final JPanel panel = new JPanel();
    		final JLabel label = new JLabel("Select the Time Zone in which this scan was acquired");
    	    
    		panel.add(label);
    	    panel.add(list);
    	    
    		JOptionPane.showMessageDialog(null, panel, "Select the Time Zone in which this scan was acquired", JOptionPane.PLAIN_MESSAGE);
    		timeZone = TimeZone.getTimeZone(timezones[list.getSelectedIndex()]);
    	}
        DateTime original = new DateTime(first.getDate().getTime());
        DateTime zoned = original.withZone(DateTimeZone.forTimeZone(timeZone));
        return zoned.toDate();    	  
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getDescription()
     */
    public String getDescription() { return first.getDescription(); }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getFileCount()
     */
    public int getFileCount() { return data.size(); }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getFormat()
     */
    public String getFormat() { return FORMAT; }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getID()
     */
    public String getID() { return first.getPatientID(); }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getModalities()
     */
    public Set<String> getModalities() { return Collections.singleton(MODALITY); }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getScanCount()
     */
    public int getScanCount() { return data.size(); }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getTimeZone()
     */
    public TimeZone getTimeZone() { return timeZone; }
    
    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getSize()
     */
    public long getSize() {
        if (0 == size) {
            for (final MatrixDataFile f : data){
                size += f.getSize();
            }
        }
        return size;
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#getVariables(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    public List<SessionVariable> getVariables(Map<?,?> params) {
        final Project project = (Project)params.get(SelectProjectPage.PRODUCT_NAME);
        final List<Variable> evs;
        try {
            final ScriptApplicator applicator = project.getEcatScriptApplicator();
            if (null == applicator) {
                logger.info("no script available");
                return Collections.emptyList();
            } else {
                evs = applicator.getSortedVariables();
            }
        } catch (Throwable t) {
            logger.warn("unable to load script applicator", t);
            return Collections.emptyList();
        }
        final List<SessionVariable> sessionVars = Lists.newArrayList();
        sessionVars.add(new AssignedSessionVariable(SessionVariableNames.MODALITY_LABEL, MODALITY, true));
        for (final Variable ev : evs) {
            sessionVars.add(EcatSessionVariable.getSessionVariable(ev));
        }
        return sessionVars;
    }

    /**
     * Provides a study identifier that is as unique and verbose as possible.
     * @return The study identifier.
     * @see java.lang.Object#toString()
     */
    public String toString() {
    	Date sessionDate = getDateTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
    	if(timeZone != null){
    		sdf.setTimeZone(timeZone);
    	}
    		
        final StringBuilder builder = new StringBuilder("ECAT study ");
        final String studyId = getID();
        if (null != studyId) {
            builder.append(studyId);
        }
        final Object accessionNumber = getAccession();
        //noinspection ConstantConditions
        if (null != accessionNumber) {
            builder.append(" (").append(accessionNumber).append(")");
        }
        if (null != sessionDate) {
            builder.append(" ").append(sdf.format(sessionDate));
        }
        final Object description = getDescription();
        if (null != description) {
            builder.append(" ").append(description);
        }
        //noinspection ConstantConditions
        if (null == studyId && null == accessionNumber) {
            builder.append(" [NO ECAT ID]");
        }
        return builder.toString();
    }

    private static String makeTimestamp() {
        return new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date());        
    }
    
    /* (non-Javadoc)
     * @see org.nrg.upload.data.Session#uploadTo(java.util.Map, org.netbeans.spi.wizard.ResultProgressHandle)
     */
    public boolean uploadTo(final Map<?,?> params,
            final UploadFailureHandler failureHandler,
            final ResultProgressHandle progress) {
        if (data.isEmpty()) {
            progress.failed("No ECAT files available to upload", true);
            return false;
        }

        final Project project = (Project)params.get(SelectProjectPage.PRODUCT_NAME);
        final Subject subject = (Subject)params.get(SelectSubjectPage.PRODUCT_NAME);
        final SessionVariable session = (SessionVariable) params.get(SessionVariableNames.SESSION_LABEL);
        final String sessionID = session.getValue();

        String visit = null;
        if(params.get(SessionVariableNames.VISIT_LABEL) != null){
        	visit = ((SessionVariable) params.get(SessionVariableNames.VISIT_LABEL)).getValue();
        }

        String protocol = null;
        if(params.get(SessionVariableNames.PROTOCOL_LABEL) != null){
        	protocol = ((SessionVariable) params.get(SessionVariableNames.PROTOCOL_LABEL)).getValue();
        }

        final String timestamp = makeTimestamp();
        final RestServer xnat = project.getRestServer();

        final String sessionLabel = session.getValue();
        progress.setBusy("Creating session " + sessionLabel);
        
        /*
        final String path = String.format("REST/projects/%s/subjects/%s/experiments/%s",
                project, subject, sessionLabel);

        // create session
        final String sessionID;
        try {
            final StringResponseProcessor processor = new StringResponseProcessor();
            xnat.doPut(path + "?xsiType=xnat:petSessionData", processor);
            sessionID = processor.toString();
        } catch (ConflictHttpException e) {
            progress.failed(e.getMessage(), true);
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            progress.failed(e.getMessage(), true);	// TODO: try again?
            return false;
        }
        */
        
        StringBuffer path = new StringBuffer(String.format("/data/services/import?dest=/prearchive/projects/%s/%s/%s",
        		project, timestamp, session.getValue()));

        if(!Strings.isNullOrEmpty(visit)){
        	path.append("&VISIT=").append(visit);
        }
        if(!Strings.isNullOrEmpty(protocol)){
        	path.append("&PROTOCOL=").append(protocol);
        }
        if(!Strings.isNullOrEmpty(timeZone.getID())){
        	path.append("&TIMEZONE=").append(timeZone.getID());
        }
        path.append("&SOURCE=applet");

        // add scans to session, and the data file to each scan
        int i = 0;
        final int size = data.size();
        for (final Iterator<MatrixDataFile> iterator = data.iterator(); iterator.hasNext(); ) {
            final MatrixDataFile dataFile = iterator.next();
            final File f = dataFile.getFile();
            while (true) {
                try {
                    final String scan = String.format("%s&SUBJECT_ID=%s&overwrite=append&xnat:petSessionData/scans/scan/ID=%d", path, subject.getLabel(), ++i);
                    progress.setProgress(String.format("Uploading scan %d/%d", i, size), i - 1, size);
                    final HttpURLConnectionProcessor processor = new ModifyingUploadProcessor(f, progress, project.toString(), subject.getLabel(), sessionID);
                    logger.trace("uploading {} as scan {}", f, i);
                    final long start = new Date().getTime();
                    xnat.doPost(scan, processor);
                    logger.trace("upload successful: {} bytes in {} seconds", size, (new Date().getTime() - start) / 1000L);
                    break;
                } catch (Throwable t) {
                    if (failureHandler.shouldRetry(dataFile.getFile(), t)) {
                        logger.info("upload failed, retrying", t);
                    } else {
                        final Map<File, Object> failures = Maps.newLinkedHashMap();
                        failures.put(f, t);
                        final StringBuilder message = new StringBuilder("user canceled operation after errors:");
                        message.append(LINE_SEPARATOR);
                        buildFailureMessage(message, failures);
                        progress.failed(message.toString(), true);
                        while (iterator.hasNext()) {
                            failures.put(iterator.next().getFile(), getUserCanceledFailure());
                        }
                        return false;
                    }
                }
            }
        }

        final Map<?,?> failures = Maps.newHashMap();
        closeSession(String.format("/data/prearchive/projects/%s/%s/%s", project, timestamp, session.getValue()), params, progress, failures);
        
        /*
       // Commit session
        try {
            progress.setBusy(String.format("Committing session %s (%s)", sessionLabel, sessionID));
            xnat.doPut(path + "?pullDataFromHeaders=true");
            xnat.doPut(path + "?fixScanTypes=true");
            xnat.doPut(path + "?triggerPipelines=true");

            // TODO: commit
            final URL url = new URL(String.format("%s/REST/experiments/%s?format=html", xnat.getURL(), sessionID));
            final UploadResultPanel urp = new UploadResultPanel(session.getValue(), url);
            final StringBuilder setValuesURL = new StringBuilder(path);
            boolean paramsAdded = false;
            final Collection<?> vars = (Collection<?>)params.get(AssignSessionVariablesPage.PRODUCT_NAME);
            if (null == vars) {
                logger.error("session variables not assigned: {}", params);
            } else {
                for (final Object v : vars) {
                    logger.trace("checking session variable {}", v);
                    if (v instanceof SessionVariable) {
                        final SessionVariable xv = (SessionVariable)v;
                        final String exportField = xv.getExportField();
                        if (!Strings.isNullOrEmpty(exportField)) {
                            if (paramsAdded) {
                                setValuesURL.append("&");
                            } else {
                                setValuesURL.append("?");
                                paramsAdded = true;
                            }
                            setValuesURL.append(exportField);
                            setValuesURL.append("=");
                            setValuesURL.append(URLEncoder.encode(xv.getValue(), URL_ENCODE_SCHEME));
                        }
                    }
                }
            }
            if (paramsAdded) {
                logger.trace("Setting variables: {}", setValuesURL);
                xnat.doPut(setValuesURL.toString());
            }

            progress.finished(Summary.create(urp, null));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            progress.failed(e.getMessage(), true);	// TODO: try again?
            return false;
        }
        */
        
        return true;
    }

    // TODO: this is duplicate of method in org.nrg.dcm.Study
    private JSONObject buildCommitEntity(final Map<?, ?> params) {
        final JSONObject entity = new JSONObject();
        final Collection<?> vars = (Collection<?>) params.get(AssignSessionVariablesPage.PRODUCT_NAME);
        if (null == vars) {
            logger.error("session variables not assigned in {}", params);
        } else {
            for (final Object o : vars) {
                if (o instanceof SessionVariable) {
                    final SessionVariable v = (SessionVariable) o;
                    final String path = v.getExportField();
                    if (!Strings.isNullOrEmpty(path)) {
                        try {
                            entity.put(path, v.getValue());
                        } catch (JSONException exception) {
                            String message = "unable to assign session variable " + path;
                            logger.error(message, exception);
                            // Analytics.enter(UploadAssistant.class, Level.ERROR, message, exception);
                        }
                    }
                }
            }
        }
        logger.trace("Built commit entity: {}", entity);
        // Analytics.enter(UploadAssistant.class, String.format("Built commit entity: %s", entity));
        return entity;
    }

    // TODO: this is (almost) duplicate of method in org.nrg.dcm.Study
    private boolean closeSession(final String path, final Map<?, ?> params, final ResultProgressHandle progress, final Map<?, ?> failures) {
        final String adminEmail = (String) params.get(XNAT_ADMIN_EMAIL_WIZ_PARAM);
        final String session = ((SessionVariable) params.get(SessionVariableNames.SESSION_LABEL)).getValue();
        final RestServer xnat = (RestServer) params.get(XNAT_REST_API_WIZ_PARAM);

        // Close session and return result
        try {
            if (failures.isEmpty()) {
                progress.setBusy("Committing session");
                logger.trace("committing session {}", path);
                final JSONRequestConnectionProcessor handler = new JSONRequestConnectionProcessor(buildCommitEntity(params));
                
                String queryParams = "?action=commit&SOURCE=applet";
                //add visit
                if(null != params.get(SessionVariableNames.VISIT_LABEL) && !Strings.isNullOrEmpty(((AssignedSessionVariable)params.get(SessionVariableNames.VISIT_LABEL)).getValue())){
                    queryParams += "&VISIT=" + ((AssignedSessionVariable)params.get(SessionVariableNames.VISIT_LABEL)).getValue();
                }
                //add protocol
                if(null != params.get(SessionVariableNames.PROTOCOL_LABEL) && !Strings.isNullOrEmpty(((AssignedSessionVariable)params.get(SessionVariableNames.PROTOCOL_LABEL)).getValue())){
                    queryParams += "&PROTOCOL=" + ((AssignedSessionVariable)params.get(SessionVariableNames.PROTOCOL_LABEL)).getValue();
                }
                //add timeZone
                if(null != timeZone){
                	queryParams += "&TIMEZONE=" + timeZone.getID();
                }
                xnat.doPost(path + queryParams, handler);

                String response = handler.getResponseEntity();
                String resultPath = org.nrg.dcm.Study.getWebAppRelativePath(xnat.getURL(), response);
              
                final URL result = new URL(xnat.getURL() + "/" + resultPath);

                // TODO: build summary, notify user
                final UploadResultPanel resultPanel = new UploadResultPanel(session, org.nrg.dcm.Study.buildSessionViewURL(result, resultPath));
                progress.finished(Summary.create(resultPanel, path));
                return true;
            } else {
                progress.failed(org.nrg.dcm.Study.buildFailureMessage(failures), false);
                return false;
            }
        } catch (JSONException e) {
            logger.error("unable to write commit request entity", e);
            return false;
        } catch (HttpException e) {
            logger.error("session commit failed", e);
            switch (e.getResponseCode()) {
                case HttpURLConnection.HTTP_NOT_FOUND: {
                    final StringBuilder sb = new StringBuilder("<h3>Resource not found (404)</h3>");
                    sb.append("<p>The server at ");
                    org.nrg.dcm.Study.appendServer(sb, xnat);
                sb.append(" is accessible but reports that the session resource ");
                    sb.append(path);
                    sb.append(" does not exist.</p>");
                    sb.append("<p>Contact the administrator ");
                    sb.append("<").append(adminEmail).append(">");
                    sb.append(" for help.</p>");
                    progress.failed(sb.toString(), true);
                    return false;
                }

                case HttpURLConnection.HTTP_INTERNAL_ERROR: {
                    final StringBuilder sb = new StringBuilder("<h3>Internal Server Error (500)</h3>");
                    sb.append("<p>The server at ");
                    org.nrg.dcm.Study.appendServer(sb, xnat);
                    sb.append(" is accessible but was unable to commit the requested session");
                    sb.append(" due to an internal error.</p>");
                    sb.append("<p>Please contact the administrator ");
                    sb.append("<").append(adminEmail).append(">");
                    sb.append(" for help.");
                    sb.append(" A detailed description of the problem should be available");
                    sb.append(" in the DICOM receiver log or the XNAT logs.</p>");
                    progress.failed(sb.toString(), true);
                    return false;
                }

                case HttpURLConnection.HTTP_CONFLICT: {
                    final StringBuilder sb = new StringBuilder("<h3>Session data conflict</h3>");
                    sb.append("<p>The server at ");
                    org.nrg.dcm.Study.appendServer(sb, xnat);
                    sb.append(" reports a conflict between the uploaded data and a session in the archive.</p>");
                    sb.append("<p>All or part of this session was previously uploaded. Go to the prearchive page ");
                    sb.append("to archive the data just uploaded as a new session, or to merge it into an existing session.");
                    progress.failed(sb.toString(), true);
                    return false;
                }

                default: {
                    progress.failed("<h3>Unexpected error " + e.getResponseCode() + ": " + e.getMessage() + "</h3><p>Unable to commit uploaded session</p><p>Please contact your XNAT administrator <" + adminEmail + "> for help.</p>", true);
                    return false;
                }
            }
        } catch (IOException e) {
            logger.error("Session commit failed", e);
            final StringBuilder sb = new StringBuilder("<h3>Communications error</h3>");
            sb.append("<p>The server at ");
            org.nrg.dcm.Study.appendServer(sb, xnat);
            sb.append(" is inaccessible (");
            sb.append(e.getMessage());
            sb.append("). Please contact your XNAT administrator ");
            sb.append("<").append(adminEmail).append(">");
            sb.append(" for help.</p>");
            progress.failed(sb.toString(), false);
            return false;
        } catch (Throwable t) {
            logger.error("Session commit failed", t);
            progress.failed("<h3>Error in applet</h3><p>An error in the uploader (" + t + " prevented the session from being committed. Please contact your XNAT administrator <" + adminEmail + "> for help.</p>", false);
            return false;
        }
    }

    private Object getUserCanceledFailure() {
        return "User canceled upload";
    }

    private static <T> StringBuilder buildFailureMessage(final StringBuilder sb, final Map<File,T> failures) {
        final Multimap<T,File> inverse = LinkedHashMultimap.create();
        Multimaps.invertFrom(Multimaps.forMap(failures), inverse);
        final Multimap<Object,File> causes = org.nrg.util.Utils.consolidateKeys(inverse, 4);
        for (final Object key : causes.keySet()) {
            final Collection<File> files = causes.get(key);
            sb.append(files.size()).append(" files not uploaded: ").append(key);
            sb.append(LINE_SEPARATOR);
        }
        return sb;
    }
}
