/*
 * org.nrg.ecat.ModifyingUploadProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.ecat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.netbeans.spi.wizard.ResultProgressHandle;
import org.nrg.ecat.var.Variable;
import org.nrg.net.HttpURLConnectionProcessor;
import org.nrg.upload.ui.ResultProgressListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

import static org.nrg.ecat.MainHeader.*;

public class ModifyingUploadProcessor implements HttpURLConnectionProcessor {
	private static final String CONTENT_TYPE_HEADER = "Content-Type";
	private static final String CONTENT_TYPE = "application/zip";
	private static final Variable[] toClear = {
		PATIENT_AGE,
		PATIENT_HEIGHT,
		PATIENT_WEIGHT,
		PATIENT_BIRTH_DATE,
	};
	private static final SimpleDateFormat TIMESTAMP = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	private final Logger logger = LoggerFactory.getLogger(ModifyingUploadProcessor.class);
	private final String name;
	private final InputStream in;
    private final ResultProgressListener progress;
	private final Collection<HeaderModification> modifications;
	
	/**
	 * Creates a processor that modifies the file during upload.
	 * @param file        The file to be uploaded.
     * @param progress    The handle to the result progress control.
     * @param project     The project to which the file is being uploaded.
     * @param subject     The subject to which the file is being uploaded.
     * @param session     The session to which the file is being uploaded.
     * @throws IOException When an error occurs reading or writing the file.
	 */
	public ModifyingUploadProcessor(final File file, final ResultProgressHandle progress,
			final String project, final String subject, final String session)
	throws IOException {
		in = new FileInputStream(file);
        final int size = (int) file.length();
		if (size < 0) {
			throw new UnsupportedOperationException("cannot upload files with size beyond integer range");
		}
		name = file.getName();
		this.progress = new ResultProgressListener(progress, 0, size);
		modifications = new ArrayList<>();
		modifications.add(STUDY_DESCRIPTION.createValueModification(project));
		modifications.add(PATIENT_NAME.createValueModification(subject));
		modifications.add(PATIENT_ID.createValueModification(session));
		for (final Variable v : toClear) {
			modifications.add(v.createClearModification());
		}
	}

    /**
     * Handles the response from the given successful connection.
     * @param connection    The connection to prepare.
     * @throws IOException  When something goes wrong.
	 * @see HttpURLConnectionProcessor#prepare(HttpURLConnection)
	 */
	@SuppressWarnings({"ConstantConditions", "Duplicates"})
    public void prepare(final HttpURLConnection connection) throws IOException {

	    /*
	     * I modeled this after ZipSeriesUploader.sendFixedSize() when we realized the previous version
	     * would run the JVM out of heap space. PLEASE REVIEW THIS CODE. I'm not 100% sure everything is
	     * getting closed properly in the nested mass of try, catch, finally clauses. Also, progress
	     * is not being updated during the stream copy to the HTTPConnection. That needs to be addressed.
	     * JDW 2013-03-26
	     */
	    
	    final File tempzip = File.createTempFile("scan-upload", ".zip");
	    try {
	    	logger.debug("creating zip file {}", tempzip);
	    	IOException ioexception = null;
            final FileOutputStream fos = new FileOutputStream(tempzip); 
	        try {
	            final ZipOutputStream zout = new ZipOutputStream(fos);
	            try {
					// getEntryName() converts existing name to a random string with the same extension. This is to hide
					// PHI in cases where, e.g., patient names are included in the ECAT file name.
	                final ZipEntry ze = new ZipEntry(getEntryName());
	                zout.putNextEntry(ze);
	                MatrixData.copyWithModifications(zout, in, modifications, progress);
	                zout.closeEntry();
	                zout.flush();
	            } catch (IOException e) {
	                throw ioexception = e;
	            } finally {
	                try {
	                    zout.close();
	                } catch (IOException e) {
	                    if (null == ioexception) {
	                        throw ioexception = e;
	                    } else {
	                        logger.error("unable to close ZipOutputStream", e);
	                        throw ioexception;
	                    }
	                }
	            }
	        } catch (final Throwable t) {
	            logger.error("copy operation failed", t);
	        } finally {
	            try {
	            fos.close();
	            } catch (IOException e) {
	            	throw ioexception = null == ioexception ? e : ioexception;
	            }
	        }
	        
	        logger.debug("zip file complete");
	        final int zipsize = (int) tempzip.length();  //TODO: Uh Oh... this is a naive and stupid cast. 
            //progress.addToSend(zipsize - series.getSize());
	        connection.setDoOutput(true);
		    connection.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE);
		    connection.setFixedLengthStreamingMode(zipsize);
		    connection.connect();
		    
		   
            logger.trace("connection open; starting data send");
            assert ioexception == null;
            final FileInputStream fis = new FileInputStream(tempzip);
            try {
                final OutputStream os = connection.getOutputStream();
                try {
                    ByteStreams.copy(fis, os);
                } catch (IOException e) {
                    throw ioexception = e;
                } finally {
                    try {
                        os.close();
                    } catch (IOException e) {
                        throw ioexception = null == ioexception ? e : ioexception;
                    }
                }
            } finally {
                try {
                    fis.close();
                } catch (IOException e) {
                    throw ioexception = null == ioexception ? e : ioexception;
                }
            }
		    
		    
            
	    } finally {
	        in.close();
            //noinspection ResultOfMethodCallIgnored
            tempzip.delete();
	    }
	}

	private String getEntryName() {
		return Long.toString(name.hashCode() & 0xffffffffL, 36) + "_" + TIMESTAMP.format(new Date()) + getExtension();
	}

	private String getExtension() {
		final int index = name.lastIndexOf(".");
		if (index < 0) {
			return ".v";
		}
		return name.substring(index);
	}

    /**
     * Handles the response from the given successful connection.
     * @param connection    The connection to prepare.
	 * @see HttpURLConnectionProcessor#process(HttpURLConnection)
	 */
	public void process(final HttpURLConnection connection) {}
}
