/*
 * org.nrg.ecat.EcatSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.ecat;

import java.util.Collections;
import java.util.Set;

import org.nrg.ecat.edit.MultipleInitializationException;
import org.nrg.ecat.edit.Value;
import org.nrg.ecat.edit.Variable;
import org.nrg.upload.data.AbstractSessionVariable;
import org.nrg.upload.data.SessionVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class EcatSessionVariable extends AbstractSessionVariable implements SessionVariable {
    private final Logger logger = LoggerFactory.getLogger(EcatSessionVariable.class);
    private final Variable _variable;
    private String _message = null;

    /**
     * @param variable The _variable to set for the ECAT session.
     */
    public EcatSessionVariable(final Variable variable) {
        super(variable.getName());
        _variable = variable;
    }

    public static SessionVariable getSessionVariable(final Variable v) {
        return new TextEcatVariable(v);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#getExportField()
     */
    public String getExportField() {
        return _variable.getExportField();
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#getValue()
     */
    @Override
    public String getValue() {
        logger.trace("getting value for " + this);
        final Object value = _variable.getValue();
        if (null == value) {
            final Value iv = _variable.getInitialValue();
            logger.trace("no value set; evaluating initial value " + iv);
            if (null == iv) {
                return null;
            } else {
                try {
                    final Object ivo = iv.on(Collections.emptyMap());
                    logger.trace("initial value = " + ivo);
                    return null == ivo ? null : ivo.toString();
                } catch (Throwable t) {
                    logger.warn("unable to evaluate initial value " + iv, t);
                    return null;
                }
            }
        } else {
            return value.toString();
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#getValueMessage()
     */
    @Override
    public String getValueMessage() {
        return _message;
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#isHidden()
     */
    @Override
    public boolean isHidden() {
        return _variable.isHidden();
    }

    /* (non-Javadoc)
     * @see org.nrg.upload.data.SessionVariable#setValue(java.lang.String)
     */
    @Override
    public String setValue(String value) throws InvalidValueException {
        final Object old = _variable.getValue();
        _variable.setValue(value);
        return null == old ? null : old.toString();
    }

    public void setInitialValue(final Value value) throws MultipleInitializationException {
        _variable.setInitialValue(value);
    }

    public void setIsHidden(final boolean isHidden) {
        _variable.setIsHidden(isHidden);
    }


    public Set<?> getVariables() {
        final Value iv = _variable.getInitialValue();
        return null == iv ? Collections.emptySet() : iv.getVariables();
    }

    protected void editTo(final String value) {
        try {
            _message = validate(value);
            _variable.setValue(value);
            fireHasChanged();
        } catch (InvalidValueException e) {
            fireIsInvalid(_variable, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return super.toString() + " (" + _variable + ")";
    }
}
