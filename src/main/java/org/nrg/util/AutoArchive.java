/**
 * AutoArchive
 * (C) 2011 Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 *
 * Created on 12/19/11 by rherri01
 */
package org.nrg.util;

public enum AutoArchive {
    Default,
    Append,
    Overwrite
}
