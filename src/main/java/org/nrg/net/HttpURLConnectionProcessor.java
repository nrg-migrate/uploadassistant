/*
 * org.nrg.net.HttpURLConnectionProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import java.net.HttpURLConnection;

public interface HttpURLConnectionProcessor {
	/**
	 * Prepares a request for the given connection.
	 * @param connection    The connection to prepare.
	 * @throws Exception    When something goes wrong.
	 */
	void prepare(HttpURLConnection connection) throws Exception;
	
	/**
	 * Handles the response from the given successful connection.
	 * @param connection    The connection to prepare.
	 * @throws Exception    When something goes wrong.
	 */
	void process(HttpURLConnection connection) throws Exception;
}