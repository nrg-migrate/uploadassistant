/*
 * org.nrg.net.ConflictHttpException
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

public class ConflictHttpException extends HttpException {
	private static final long serialVersionUID = 1L;
	
	private static final int CODE = 409;
	private static final String MESSAGE = "Conflict";
	
	/**
	 * Creates an HTTP exception for resource conflicts.
	 * @param message     The message to be displayed for the error.
	 */
	public ConflictHttpException(final String message) {
		super(CODE, MESSAGE, message);
	}
}
