/*
 * org.nrg.net.xnat.ProjectSubjectLister
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net.xnat;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;

import org.json.JSONException;
import org.nrg.net.RestServer;

public final class ProjectSubjectLister implements Callable<Map<String,String>> {
	private final RestServer xnat;
	private final String uri;

	public ProjectSubjectLister(final RestServer xnat, final String project) {
		this.xnat = xnat;

		final StringBuilder sb = new StringBuilder("/REST/projects/");
		sb.append(project);
		sb.append("/subjects?format=json");
		sb.append("&columns=DEFAULT");	// for 1.4rc3 compatibility
		uri = sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	public Map<String,String> call() throws IOException, JSONException {
		return xnat.getAliases(uri, "label", "ID");
	}
}
