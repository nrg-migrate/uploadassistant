/*
 * org.nrg.net.xnat.BaseScriptApplicatorRetreiver
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.net.xnat;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.concurrent.Callable;

import org.nrg.net.HttpURLConnectionProcessor;
import org.nrg.net.RestServer;

class BaseScriptApplicatorRetreiver<ApplicatorT> implements Callable<ApplicatorT> {
    private final RestServer xnat;
    private final String path;
    private final ScriptApplicatorFactory<ApplicatorT> factory;

    public static interface ScriptApplicatorFactory<A> {
        A createScriptApplicator(final InputStream in) throws Exception;
    }

    BaseScriptApplicatorRetreiver(final RestServer xnat,
            final ScriptApplicatorFactory<ApplicatorT> factory,
            final String path) {
        this.xnat = xnat;
        this.factory = factory;
        this.path = path;
    }

    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    public final ApplicatorT call() throws Exception {
        final ConnectionProcessor<ApplicatorT> processor = new ConnectionProcessor<ApplicatorT>(factory);
        xnat.doGet(path, processor);
        return processor.getApplicator();
    }


    public static final class ConnectionProcessor<ApplicatorT> implements HttpURLConnectionProcessor {
        private final ScriptApplicatorFactory<ApplicatorT> factory;
        private ApplicatorT applicator = null;

        public ConnectionProcessor(final ScriptApplicatorFactory<ApplicatorT> factory) {
            this.factory = factory;
        }

        /*
         * (non-Javadoc)
         * @see org.nrg.net.HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
         */
        public void prepare(final HttpURLConnection c) {}

        /*
         * (non-Javadoc)
         * @see org.nrg.net.HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
         */
        public void process(final HttpURLConnection c) throws Exception {
            applicator = factory.createScriptApplicator(c.getInputStream());
        }

        public ApplicatorT getApplicator() { return applicator; }
    }
}
