/**
 * ProjectPreArcCodeRetriever
 * (C) 2011 Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 *
 * Created on 12/12/11 by rherri01
 */
package org.nrg.net.xnat;

import org.nrg.net.RestServer;
import org.nrg.net.StringResponseProcessor;
import org.nrg.util.PrearchiveCode;

import java.util.concurrent.Callable;

public class ProjectPreArcCodeRetriever implements Callable<PrearchiveCode> {
    private final RestServer _xnat;
    private final String _path;

    public ProjectPreArcCodeRetriever(final RestServer xnat, final String project) {
        _xnat = xnat;
        _path = String.format("/data/archive/projects/%s/prearchive_code", project);
    }

    /*
      * (non-Javadoc)
      * @see java.util.concurrent.Callable#call()
      */
    @Override
    public PrearchiveCode call() throws Exception {
        final StringResponseProcessor processor = new StringResponseProcessor();
        _xnat.doGet(_path, processor);
        return PrearchiveCode.code(processor.toString());
    }
}
