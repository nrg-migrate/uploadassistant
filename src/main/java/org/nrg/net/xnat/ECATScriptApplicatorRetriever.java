/*
 * org.nrg.net.xnat.ECATScriptApplicatorRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net.xnat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.Callable;

import org.nrg.ecat.edit.ScriptApplicator;
import org.nrg.ecat.edit.ScriptEvaluationException;
import org.nrg.ecat.edit.ScriptFunction;
import org.nrg.net.RestServer;

public final class ECATScriptApplicatorRetriever
extends BaseScriptApplicatorRetreiver<ScriptApplicator>
implements Callable<ScriptApplicator> {
	public ECATScriptApplicatorRetriever(final RestServer xnat, final String project,
			final Map<String,? extends ScriptFunction> scriptFunctions) {
		super(xnat, buildFactory(scriptFunctions),
				"/REST/projects/" + project + "/resources/UPLOAD_CONFIG/files/ecat.eas");
	}
	
	private static ScriptApplicatorFactory<ScriptApplicator>
	buildFactory(final Map<String,? extends ScriptFunction> scriptFunctions) {
		return new ScriptApplicatorFactory<ScriptApplicator>() {
			public ScriptApplicator createScriptApplicator(final InputStream in)
			throws IOException,ScriptEvaluationException {
				return new ScriptApplicator(in, scriptFunctions);
			}
		};
	}
}
