/*
 * org.nrg.net.xnat.ProjectSessionLister
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 1/23/14 12:46 PM
 */
package org.nrg.net.xnat;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;

import org.json.JSONException;
import org.nrg.net.RestServer;

public final class ProjectSessionLister implements Callable<Map<String,String>> {
	private final RestServer xnat;
	private final String path;
	
	public ProjectSessionLister(final RestServer xnat, final String project) {
		this.xnat = xnat;

		final StringBuilder sb = new StringBuilder("/REST/projects/");
		sb.append(project);
		sb.append("/experiments?format=json");
		path = sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	public Map<String,String> call()
	throws IOException,JSONException {
		return xnat.getAliases(path, "label", "ID"); 
	}
}
