/*
 * org.nrg.net.xnat.PETTracerRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.net.xnat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.concurrent.Callable;

import org.nrg.net.RestServer;
import org.nrg.net.StringListConnectionProcessor;

import com.google.common.collect.Sets;

public final class PETTracerRetriever implements Callable<Set<String>> {
    private static final String DEFAULT_TRACERS_RESOURCE = "/PET-tracers.txt";
    private static final Set<String> defaultTracers = getDefaultTracers(DEFAULT_TRACERS_RESOURCE);

    private final RestServer xnat;
    private final String projectPath;
    private final String sitePath;

    public PETTracerRetriever(final RestServer xnat, final String project) {
        this.xnat = xnat;
        projectPath = "/REST/projects/" + project + "/config/tracers/tracers?contents=true";
        sitePath = "/REST/config/tracers/tracers?contents=true";
    }

    public Set<String> call() {
        final StringListConnectionProcessor processor = new StringListConnectionProcessor();
        try {
            // check to see if we got back a status page instead of a list of tracers
            // if so, there's no project specific list, so just get the site list instead
            try {
                xnat.doGet(projectPath, processor);
            } catch (Throwable t) {
                xnat.doGet(sitePath, processor);
            }
            return Sets.newLinkedHashSet(processor);
        } catch (Throwable t) {
            return getDefaultTracers();
        }
    }

    public static Set<String> getDefaultTracers() {
        return Sets.newLinkedHashSet(defaultTracers);
    }

    private static Set<String> getDefaultTracers(final String resource) {
        IOException ioexception = null;
        final InputStream in = PETTracerRetriever.class.getResourceAsStream(resource);
        if (null == in) {
            throw new RuntimeException("Unable to load default PET tracers");
        }
        try {
            return Sets.newLinkedHashSet(StringListConnectionProcessor.readStrings(in));
        } catch (IOException e) {
            ioexception = e;
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                ioexception = e;
            }
        }
        throw new RuntimeException("Unable to read default PET tracers", ioexception);
    }
}
