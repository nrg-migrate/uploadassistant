/*
 * org.nrg.net.EmptyRequest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import java.io.IOException;
import java.net.HttpURLConnection;


class EmptyRequest implements HttpURLConnectionProcessor {
	/*
	 * (non-Javadoc)
	 * @see org.nrg.net.HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
	 */
	public void prepare(final HttpURLConnection connection) {}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.net.HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
	 */
	public void process(final HttpURLConnection connection) throws IOException {
		try {
		} finally {
			connection.disconnect();
		}
	}
}