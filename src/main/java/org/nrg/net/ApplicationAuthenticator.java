/*
 * org.nrg.net.SwingAuthenticator
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import com.google.common.base.Joiner;
import org.apache.commons.lang.StringUtils;
import org.nrg.upload.ui.Constants;
import org.nrg.upload.ui.UploadAssistantApplication;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.*;
import java.util.prefs.Preferences;

public class ApplicationAuthenticator extends Authenticator {

    public enum SaveInfo {
        Nothing("Nothing"),
        Url("URL"),
        UrlAndUsername("URL and username");

        SaveInfo(final String display) {
            _display = display;
        }

        @Override
        public String toString() {
            return _display;
        }

        public static SaveInfo fromDisplay(final String display) {
            if (!_isInit) {
                cacheSaveInfo();
            }
            if (StringUtils.isBlank(display)) {
                return UrlAndUsername;
            }
            return _registry.get(display);
        }

        public static String[] options() {
            if (!_isInit) {
                cacheSaveInfo();
            }
            return _displays.toArray(new String[_displays.size()]);
        }

        private static void cacheSaveInfo() {
            if (_registry.isEmpty()) {
                synchronized (SaveInfo.class) {
                    for (final SaveInfo saveInfo : values()) {
                        _registry.put(saveInfo.toString(), saveInfo);
                        _displays.add(saveInfo.toString());
                    }
                }
                _isInit = true;
            }
        }

        private static boolean _isInit = false;
        private static final Map<String, SaveInfo> _registry = new HashMap<>();
        private static final SortedSet<String> _displays = new TreeSet<>();

        private String _display;
    }

    /**
     * Creates a new application authenticator.
     * @param parent         The parent application for the authenticator window.
     * @param preferences    The application preferences object.
     */
    public ApplicationAuthenticator(final UploadAssistantApplication parent, final Preferences preferences) {
        _parent = parent;
        _url = preferences.get(Constants.XNAT_URL, null);
        _username = preferences.get(Constants.XNAT_USER, null);
        _password = null;
        _saveInfo = SaveInfo.fromDisplay(preferences.get(Constants.XNAT_SAVE_INFO, null));
    }

    @SuppressWarnings("unused")
    public void reset() {
        _initialized = false;
        _url = null;
        _username = null;
        _password = null;
        _saveInfo = null;
    }

    public boolean getCredentials() {
        final JPanel contents = new JPanel(new GridBagLayout());

        contents.add(new JLabel("URL:"), labelConstraint);
        final JTextField urlField = new JTextField(16);
        if (StringUtils.isNotBlank(_url)) {
            urlField.setText(_url);
        }
        contents.add(urlField, fieldConstraint);

        contents.add(new JLabel("User:"), labelConstraint);
        final JTextField userField = new JTextField(16);
        if (StringUtils.isNotBlank(_username)) {
            userField.setText(_username);
        }
        contents.add(userField, fieldConstraint);

        contents.add(new JLabel("Password:"), labelConstraint);
        final JPasswordField passField = new JPasswordField(16);
        passField.setText("");
        contents.add(passField, fieldConstraint);

        contents.add(new JLabel("Save:"), labelConstraint);
        final JComboBox<String> saveInfoField = new JComboBox<>(SaveInfo.options());
        if (_saveInfo != null) {
            saveInfoField.setSelectedItem(_saveInfo.toString());
        }
        contents.add(saveInfoField, labelConstraint);

        final JOptionPane optionPane = new JOptionPane(contents, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
        final JDialog dialog = optionPane.createDialog(_parent, getRequestingPrompt());
        dialog.addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent e) {
                userField.requestFocusInWindow();
            }
        });

        Boolean finished;
        do {
            finished = showUntilValidOrCanceled(dialog, optionPane, urlField, userField, passField, saveInfoField);
        } while (finished == null);
        return finished;
    }

    public String getUrl() {
        return _url;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        if (_initialized || getCredentials()) {
            return new PasswordAuthentication(_username, _password);
        } else {
            return null;
        }
    }

    private Boolean showUntilValidOrCanceled(final JDialog dialog, final JOptionPane optionPane, final JTextField urlField, final JTextField userField, final JPasswordField passField, final JComboBox<String> saveInfoField) {
        dialog.setVisible(true);
        if (JOptionPane.OK_OPTION == (Integer) optionPane.getValue()) {
            _url = urlField.getText();
            _username = userField.getText();
            _password = passField.getPassword();

            final Set<String> errors = new HashSet<>();

            if (StringUtils.isBlank(_url)) {
                errors.add("URL");
            }
            if (StringUtils.isBlank(_username)) {
                errors.add("Username");
            }
            if (_password == null || StringUtils.isBlank(new String(_password))) {
                errors.add("Password");
            }

            if (errors.size() > 0) {
                JOptionPane.showMessageDialog(dialog, "You must provide a value for the following items: " + Joiner.on(", ").join(errors));
                return null;
            }

            _saveInfo = SaveInfo.fromDisplay(saveInfoField.getSelectedItem().toString());

            final Map<String, String> prefs = new HashMap<>();

            prefs.put(Constants.XNAT_USER, _saveInfo == SaveInfo.UrlAndUsername ? _username : null);
            prefs.put(Constants.XNAT_URL, _saveInfo != SaveInfo.Nothing ? _url : null);
            prefs.put(Constants.XNAT_SAVE_INFO, _saveInfo != SaveInfo.Nothing ? _saveInfo.toString() : null);

            _parent.setParameters(prefs);

            _initialized = true;

            return true;
        } else {
            return false;
        }
    }

    private final static GridBagConstraints labelConstraint = new GridBagConstraints();

    private final static GridBagConstraints fieldConstraint = new GridBagConstraints();

    static {
        labelConstraint.gridx = 0;
        labelConstraint.anchor = GridBagConstraints.LINE_START;
        fieldConstraint.gridx = 1;
        fieldConstraint.anchor = GridBagConstraints.LINE_END;
        fieldConstraint.fill = GridBagConstraints.HORIZONTAL;
        fieldConstraint.weightx = 1;
    }

    private final UploadAssistantApplication _parent;

    private boolean _initialized = false;
    private String _url;
    private String _username;
    private char[] _password;
    private SaveInfo _saveInfo;
}
