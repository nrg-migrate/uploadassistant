/*
 * org.nrg.net.SwingAuthenticator
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.net.Authenticator;
import java.net.PasswordAuthentication;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class AppletAuthenticator extends Authenticator {
    private final Component _parent;

    private final static GridBagConstraints labelConstraint = new GridBagConstraints();
    private final static GridBagConstraints fieldConstraint = new GridBagConstraints();

    static {
        labelConstraint.gridx = 0;
        labelConstraint.anchor = GridBagConstraints.LINE_START;
        fieldConstraint.gridx = 1;
        fieldConstraint.anchor = GridBagConstraints.LINE_END;
        fieldConstraint.fill = GridBagConstraints.HORIZONTAL;
        fieldConstraint.weightx = 1;
    }

    /**
     * Creates a new applet authenticator object.
     * @param parent    The parent component for the authenticator window.
     */
    public AppletAuthenticator(final Component parent) {
        _parent = parent;
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        final JPanel contents = new JPanel(new GridBagLayout());

        contents.add(new JLabel("User:"), labelConstraint);
        final JTextField userField = new JTextField(16);
        contents.add(userField, fieldConstraint);

        contents.add(new JLabel("Password:"), labelConstraint);
        final JPasswordField passField = new JPasswordField(16);
        contents.add(passField, fieldConstraint);

        final JOptionPane optionPane = new JOptionPane(contents,
                JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
        final JDialog dialog = optionPane.createDialog(_parent, getRequestingPrompt());
        dialog.addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent e) {
                userField.requestFocusInWindow();
            }
        });
        dialog.setVisible(true);
        if (JOptionPane.OK_OPTION == (Integer) optionPane.getValue()) {
            return new PasswordAuthentication(userField.getText(), passField.getPassword());
        } else {
            return null;
        }
    }
}
