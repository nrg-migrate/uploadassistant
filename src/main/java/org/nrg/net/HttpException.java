/*
 * org.nrg.net.HttpException
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import java.io.IOException;

public class HttpException extends IOException {
	private static final long serialVersionUID = 1L;
	private final int _responseCode;

	/**
	 * Creates a new exception object containing relevant data for the error.
	 * @param responseCode       The response code that occurred.
	 * @param responseMessage    The message that accompanied the response.
	 * @param text               The text for the exception.
	 */
	public HttpException(final int responseCode, final String responseMessage, final String text) {
		super(buildMessage(responseCode, responseMessage, text));
		_responseCode = responseCode;
	}
	
	public int getResponseCode() { return _responseCode; }
	
	private static String buildMessage(final int code, final String message, final String text) {
		final StringBuilder sb = new StringBuilder("HTTP status ");
		sb.append(code);
		if (null != message && ! "".equals(message)) {
			sb.append("(").append(message).append(")");
		}
		if (null != text && ! "".equals(text)) {
			sb.append(": ").append(text);
		}
		return sb.toString();
	}
}
