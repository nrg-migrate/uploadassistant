/*
 * org.nrg.net.JSONResultExtractor
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.nrg.net.RestServer.JSONDecoder;

class JSONResultExtractor implements HttpURLConnectionProcessor {
	private final JSONDecoder decoder;

	JSONResultExtractor(final JSONDecoder decoder) {
		this.decoder = decoder;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nrg.net.HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
	 */
	public void prepare(final HttpURLConnection connection) {}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.net.HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
	 */
	public void process(final HttpURLConnection connection) throws IOException, JSONException {
		final InputStream in = connection.getInputStream();
		try {
			final JSONArray entries = RestServer.extractResultFromEntity(RestServer.extractJSONEntity(in));
			for (int i = 0; i < entries.length(); i++) {
				decoder.decode(entries.getJSONObject(i));
			}
		} finally {
			in.close();
		}
	}
}
