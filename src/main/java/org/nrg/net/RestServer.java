/*
 * org.nrg.net.RestServer
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/12/14 4:41 PM
 */
package org.nrg.net;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.nrg.IOUtils;
import org.nrg.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.net.HttpURLConnection.*;

public class RestServer {
    private static final Pattern userInfoPattern = Pattern.compile("([^:@/]*):([^:@]*)");
    private static final String GET = "GET", PUT = "PUT", POST = "POST";
    private static final String AUTHORIZATION_HEADER = "Authorization";
    final static String TITLE;
    final static String VERSION;

    static {
        final Properties props = new Properties();
        final ClassLoader cl = RestServer.class.getClassLoader();
        try {
            props.load(cl.getResourceAsStream("META-INF/application.properties"));
        } catch (Throwable t) {
            LoggerFactory.getLogger(RestServer.class).error("Unable to load properties", t);
        }
        TITLE = props.getProperty("application.name");
        VERSION = props.getProperty("application.version");
    }

    private static final Map<String, String> defaultHeaders = ImmutableMap.of(
            "User-Agent", "XNATUploadAssistant/" + VERSION,
            "Accept", "*/*");

    private final Logger logger = LoggerFactory.getLogger(RestServer.class);
    private final URL _base;
    private final JSESSIONIDCookie _jsessionidCookie;
    private String _siteWideAnonScript = null;

    public RestServer(final String url, ApplicationAuthenticator authenticator) throws MalformedURLException {
        this(new URL(url), authenticator);
    }

    public RestServer(final URL url, ApplicationAuthenticator authenticator) {
        final StringBuilder sb = new StringBuilder(url.toString());
        for (int i = sb.length() - 1; '/' == sb.charAt(i); i--) {
            sb.deleteCharAt(i);
        }
        try {
            _base = new URL(sb.toString());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);    // can't imagine how this would happen
        }

        final Service service = new Service(url);
        final PasswordAuthentication auth = authenticator.getPasswordAuthentication();
        passStore.put(service, auth);

        _jsessionidCookie = new JSESSIONIDCookie("");
    }

    public RestServer(final URL url, final JSESSIONIDCookie jsessionidCookie) {
        final StringBuilder sb = new StringBuilder(url.toString());
        for (int i = sb.length() - 1; '/' == sb.charAt(i); i--) {
            sb.deleteCharAt(i);
        }
        try {
            _base = new URL(sb.toString());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);    // can't imagine how this would happen
        }

        final String userInfo = url.getUserInfo();
        if (null != userInfo) {
            final Matcher m = userInfoPattern.matcher(userInfo);
            if (m.matches()) {
                final Service service = new Service(url);
                final PasswordAuthentication auth = new PasswordAuthentication(m.group(1), m.group(2).toCharArray());
                passStore.put(service, auth);
            }
        }
        _jsessionidCookie = jsessionidCookie;
    }

    public RestServer(final String url, final JSESSIONIDCookie jsessionidCookie) throws MalformedURLException {
        this(new URL(url), jsessionidCookie);
    }

    private final Map<Service, PasswordAuthentication> passStore = Maps.newHashMap();
    private final Map<Service, String> descriptions = Maps.newHashMap();

    public Map<String, String> getConfigurationProperties(final String path, String... properties) throws IOException {
        try {
            final JSONConfigurationExtractor extractor = new JSONConfigurationExtractor();
            doGet(path, extractor);
            final Map<String, String> configuration = new HashMap<>(properties.length);
            for (final String property : properties) {
                if (extractor.containsKey(property)) {
                    configuration.put(property, extractor.get(property).toString());
                } else {
                    configuration.put(property, "");
                }
            }
            return configuration;
        } catch (HttpException e) {
            if (e.getResponseCode() == 404) {
                return null;
            }
            throw e;
        }
    }

    public LinkedHashMap<String, String> getSeriesImportFilter(final String path) throws IOException, JSONException {
        final String query;
        if (!path.contains("format=json")) {
            try {
                URI uri = new URI(path);
                if (StringUtils.isBlank(uri.getQuery())) {
                    query = "?format=json";
                } else {
                    query = "&format=json";
                }
            } catch (URISyntaxException ignored) {
                // Not going to work about this because it's internal use.
                throw new IOException("There was an error processing the series import filter path: " + path, ignored);
            }
        } else {
            query = "";
        }

        final JSONConfigurationExtractor extractor = new JSONConfigurationExtractor();
        doGet(path + query, extractor);
        final LinkedHashMap<String, String> filter = new LinkedHashMap<>();
        for (final String key : extractor.keySet()) {
            final Object value = extractor.get(key);
            filter.put(key, convertObjectToJSON(value));
        }
        return filter;
    }

    public boolean isSiteWideAnonScriptEnabled() throws IOException, JSONException {
        try {
            final JSONConfigurationExtractor extractor = new JSONConfigurationExtractor();
            extractor.put("status", "disabled");
            extractor.put("contents", "");
            doGet("/data/config/anon/script?format=json", extractor);
            boolean enabled = extractor.get("status").equals("enabled");
            if (enabled) {
                _siteWideAnonScript = (String) extractor.get("contents");
            }
            return enabled;
        } catch (HttpException e) {
            if (e.getResponseCode() == 404) {
                return false;
            }
            throw e;
        }
    }

    public String getSiteWideAnonScript() {
        if (_siteWideAnonScript == null) {
            return "";
        }
        return _siteWideAnonScript;
    }

    public InputStream getSiteWideAnonScriptAsStream() {
        return new ByteArrayInputStream(getSiteWideAnonScript().getBytes());
    }

    /**
     * This method converts a value of an unknown type into a valid JSON string. This works around issues where
     * calling the <b>toString()</b> method on, e.g., a map object resulted in invalid JSON.
     * @param value    The object to be converted to JSON.
     * @return A string representing a valid JSON serialization for the submitted object.
     */
    private static String convertObjectToJSON(final Object value) {
        if (value == null) {
            return "";
        }
        if (value instanceof String) {
            return (String) value;
        }
        if (value instanceof Map) {
            final Map map = (Map) value;
            final JSONObject jsonObject = new JSONObject();
            for (final Object key : map.keySet()) {
                jsonObject.put(key.toString(), map.get(key));
            }
            return jsonObject.toString();
        }
        if (value instanceof List) {
            return new JSONArray((List) value).toString();
        }
        // This tells you if you have an array of objects as opposed to an array of primitives
        // (which are a pain to work with and probably shouldn't even happen in this context,
        // so we're going to ignore them.
        if (value instanceof Object[] && value.getClass().isArray()) {
            return new JSONArray(Arrays.asList((Object[]) value)).toString();
        }
        // At this point, we have no idea what kind of nonsense someone threw over the fence
        // to us, so just give it back to them as a string.
        return value.toString();
    }

    public void setUrlCredentials() {
        synchronized (passStore) {
        }
    }

    private static final class Service {
        private final String protocol;
        private final String host;
        private final int port;

        Service(final String protocol, final String host, final int port) {
            this.protocol = protocol;
            this.host = host;
            this.port = port;
        }

        Service(final URL url) {
            this(url.getProtocol(), url.getHost(), url.getPort());
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        public int hashCode() {
            int result = 17;
            result = 37 * result + protocol.hashCode();
            result = 37 * result + host.hashCode();
            result = 37 * result + port;
            return result;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        public boolean equals(final Object o) {
            if (!(o instanceof Service)) return false;
            final Service other = (Service) o;
            return protocol.equals(other.protocol) &&
                    host.equals(other.host) &&
                    port == other.port;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        public String toString() {
            try {
                return new URL(protocol, host, port, "").toString();
            } catch (MalformedURLException e) {
                return MessageFormat.format("{0}://{1}:{2}", protocol, host, port);
            }
        }
    }

    /**
     * Get the logged in user from the server
     *
     * @return logged in user string
     * @throws IOException When error occurs during server I/O.
     */
    public String getUserAuthMessage() throws IOException {
        final StringResponseProcessor rp = new StringResponseProcessor();
        try {
            request("/data/auth", "GET", rp);
            return rp.toString();
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            final IOException ioe = new IOException();
            ioe.initCause(e);
            throw ioe;
        }
    }

    interface JSONDecoder {
        void decode(JSONObject o) throws JSONException;
    }

    private static final class JSONValuesExtractor implements JSONDecoder {
        private final Logger logger = LoggerFactory.getLogger(JSONValuesExtractor.class);
        private final Collection<Object> c;
        private final String key;

        JSONValuesExtractor(final Collection<Object> c, final String key) {
            this.c = c;
            this.key = key;
        }

        /*
         * (non-Javadoc)
         * @see org.nrg.net.RestOpManager.JSONDecoder#decode(org.json.JSONObject)
         */
        public void decode(final JSONObject o) throws JSONException {
            logger.trace("decoding {} from {}", key, o);
            if (o.has(key)) {
                c.add(o.get(key));
            }
        }

        public Collection<Object> getValues() {
            return c;
        }
    }

    private static boolean isNullOrEmpty(final String s) {
        return null == s || "".equals(s);
    }

    /**
     * Turns a JSON object from the XNAT configuration service into a map. Before calling the {@link #decode(org.json.JSONObject)}
     * method, you can call the {@link #put(Object, Object)} method to set default values for one or more fields in the
     * configuration.
     */
    private static final class JSONConfigurationExtractor extends HashMap<String, Object> implements JSONDecoder {

        @Override
        public void decode(final JSONObject json) throws JSONException {
            final Iterator keys = json.keys();
            while (keys.hasNext()) {
                final String key = (String) keys.next();
                put(key, json.get(key));
            }
            if (containsKey("status") && !get("status").equals("enabled")) {
                put("status", "disabled");
                return;
            }
            if (containsKey("contents")) {
                final String contents = (String) get("contents");
                try {
                    JSONObject translated = new JSONObject(new JSONTokener(contents));
                    Map<String, Object> decoded = new HashMap<>();
                    final Iterator contentKeys = translated.keys();
                    while(contentKeys.hasNext()) {
                        final String key = (String) contentKeys.next();
                        decoded.put(key, translated.has(key) ? translated.get(key) : "");
                    }
                    put("contents", decoded);
                } catch (JSONException ignored) {
                    // If we get a JSONException, then the contents isn't something we know how to work with.
                    // Leave the contents alone.
                }
            }
        }
    }

    private static final class JSONAliasesExtractor implements JSONDecoder {
        private final Logger logger = LoggerFactory.getLogger(JSONAliasesExtractor.class);
        private final Map<String, String> m;
        private final String aliasKey, idKey;

        JSONAliasesExtractor(final Map<String, String> m, final String aliasKey, final String idKey) {
            this.m = m;
            this.aliasKey = aliasKey;
            this.idKey = idKey;
        }

        /*
         * (non-Javadoc)
         * @see org.nrg.net.RestOpManager.JSONDecoder#decode(org.json.JSONObject)
         */
        public void decode(final JSONObject o) throws JSONException {
            logger.trace("decoding {} using {} -> {}", o, aliasKey, idKey);
            final String alias = o.has(aliasKey) ? o.getString(aliasKey) : null;
            final String id = o.has(idKey) ? o.getString(idKey) : null;
            if (!isNullOrEmpty(alias)) {
                m.put(alias, isNullOrEmpty(id) ? alias : id);
            } else if (!isNullOrEmpty(id)) {
                m.put(id, id);
            }
        }

        public Map<String, String> getAliases() {
            return m;
        }
    }


    static JSONObject extractJSONEntity(final InputStream in)
            throws IOException, JSONException {
        return new JSONObject(new JSONTokener(new InputStreamReader(in)));
    }

    static JSONArray extractResultFromEntity(final JSONObject entity)
            throws JSONException {
        try {
            return entity.getJSONObject("ResultSet").getJSONArray("Result");
        } catch (JSONException e) {
            final String message = e.getMessage();
            if (message.contains("JSONObject[\"ResultSet\"] not found.")) {
                final JSONArray array = new JSONArray();
                array.put(entity);
                return array;
            }
            throw e;
        }
    }

    private static String makeBasicAuthorization(final PasswordAuthentication auth) {
        return "Basic " + Base64.encode(auth.getUserName() + ":" + new String(auth.getPassword()));
    }

    private static void addBasicAuthorizationToHeaderMap(final Map<String, String> m,
            final PasswordAuthentication auth) {
        m.put(AUTHORIZATION_HEADER, makeBasicAuthorization(auth));
    }

    private static void addBasicAuthorization(final URLConnection conn, final PasswordAuthentication auth) {
        conn.addRequestProperty(AUTHORIZATION_HEADER, makeBasicAuthorization(auth));
    }


    private void doGet(final String path, final JSONDecoder decoder)
            throws IOException, JSONException {
        try {
            doGet(path, new JSONResultExtractor(decoder));
        } catch (IOException | RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void request(final String path, final String method, final HttpURLConnectionProcessor processor) throws Exception {

        final Map<String, String> headers = Maps.newLinkedHashMap(defaultHeaders);
        final StringBuilder sb = new StringBuilder(_base.toString());
        if ('/' != path.charAt(0)) {
            sb.append('/');
        }
        sb.append(path);
        final URL url = new URL(sb.toString());
        logger.trace("{} preparing request {}", this, url);
        if (logger.isTraceEnabled()) {
            final Callable<List<String>> callable = new Callable<List<String>>() {
                public List<String> call() throws IOException, URISyntaxException {
                    final CookieHandler ch = CookieHandler.getDefault();
                    final Map<String, List<String>> h = ch.get(url.toURI(), new HashMap<String, List<String>>());
                    return h.get("Cookie");
                }
            };
            final ExecutorService es = Executors.newSingleThreadExecutor();
            try {
                final List<String> cookies = es.invokeAny(Collections.singleton(callable), 10, TimeUnit.SECONDS);
                logger.trace("session cookies: {}", cookies);
            } catch (Exception e) {
                logger.error("Unable to query cookie store", e);
            }
        }

        final Service service = new Service(url);
        synchronized (passStore) {
            if (passStore.containsKey(service)) {
                addBasicAuthorizationToHeaderMap(headers, passStore.get(service));
            }
        }

        logger.trace("opening connection to {}", url);

        HttpURLConnection connection = null;
        int attempts = 0;
        TRY_PUT:
            for (; ; )
                try {
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod(method);
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.setUseCaches(false);
                    for (final Map.Entry<String, String> me : headers.entrySet()) {
                        if (!connection.getRequestProperties().containsKey(me.getKey())) {
                            connection.setRequestProperty(me.getKey(), me.getValue());
                        }
                    }
                    getJSESSIONIDCookie().setInRequestHeader(connection);
                    processor.prepare(connection);
                    final int responseCode = connection.getResponseCode();

                    switch (responseCode) {
                    case HTTP_ACCEPTED:
                    case HTTP_NOT_AUTHORITATIVE:
                    case HTTP_NO_CONTENT:
                    case HTTP_RESET:
                    case HTTP_PARTIAL:
                    case HTTP_MOVED_PERM:
                        logger.trace(connection.getRequestMethod() + " to {} returned "
                                + responseCode + " ({})",
                                url, connection.getResponseMessage());

                    case HTTP_OK:
                    case HTTP_CREATED:
                        processor.process(connection);
                        return;

                        // Handle 302, at least temporarily: Spring auth redirects to login page,
                        // so assume that's what's happened when we see a redirect at this point.
                    case HTTP_MOVED_TEMP:
                    case HTTP_UNAUTHORIZED:
                        if (logger.isDebugEnabled()) {
                            logger.debug("Received status code " + (responseCode == HTTP_MOVED_TEMP ? "302 (Redirect)" : "401 (Unauthorized)"));
                            for (final Map.Entry<String, List<String>> me : connection.getHeaderFields().entrySet()) {
                                logger.trace("Header {} : {}", me.getKey(), me.getValue());
                            }
                            logger.debug("Will request credentials for {}", url);
                        }
                        addBasicAuthorizationToHeaderMap(headers, getPasswordAuthentication(service, url));

                        if (attempts++ < 3) {
                            continue TRY_PUT;
                        }
                        throw new HttpException(responseCode, "Unable to connect to " + url.toString(), "Ugh");

                    case HTTP_CONFLICT:
                        throw new ConflictHttpException(getErrorEntity(connection));

                    default:
                        final StringBuilder message = new StringBuilder();
                        message.append(connection.getRequestMethod());
                        message.append(" to ").append(url).append(" failed ");
                        try {
                            final String entity = getErrorEntity(connection);
                            if (null != entity) {
                                message.append(": ").append(entity);
                            }
                        } catch (Throwable t) {
                            message.append(" - ").append(t.getMessage());
                        }
                        throw new HttpException(responseCode, connection.getResponseMessage(), message.toString());
                    }
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
    }

    private static String getErrorEntity(final HttpURLConnection connection) throws IOException {
        final InputStream errorStream = connection.getErrorStream();
        try {
            if (null != errorStream) {
                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                IOUtils.copy(stream, errorStream);
                if (stream.size() > 0) {
                    return stream.toString();
                }
            }
            return null;
        } finally {
            if (errorStream != null) {
                try {
                    errorStream.close();
                } catch (IOException ignored) {
                    // Just ignore this if it happens. This will allow exceptions from the top through and just not say
                    // anything if the close fails. Java gets unhappy about throwing exceptions from finally blocks.
                }
            }
        }
    }

    private void request(final String path, final String method) throws IOException {
        try {
            request(path, method, new EmptyRequest());
        } catch (IOException | RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void request(final String path, final String method,
            final InputStream in, final String mimeMediaType, final Integer contentLength,
            final ResultProgressHandle progress)
                    throws IOException {
        try {
            request(path, method, new StreamUploadProcessor(in, mimeMediaType, contentLength, progress));
        } catch (IOException | RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void request(final String path, final String method,
            final File f, final String mimeMediaType, final ResultProgressHandle progress)
                    throws IOException {
        request(path, method, new FileInputStream(f),
                mimeMediaType, Long.valueOf(f.length()).intValue(), progress);
    }

    public void doGet(final String path, final HttpURLConnectionProcessor processor)
            throws Exception {
        request(path, GET, processor);
    }

    public void doPost(final String path, final HttpURLConnectionProcessor processor)
            throws Exception {
        request(path, POST, processor);
    }

    @SuppressWarnings("unused")
    public void doPut(final String path, final HttpURLConnectionProcessor processor) throws Exception {
        request(path, PUT, processor);
    }


    /**
     * Performs a POST request with empty entity.
     *
     * @param path Path to POST resource.
     * @throws IOException When error occurs during server I/O.
     */
    @SuppressWarnings("unused")
    public void doPost(final String path) throws IOException {
        request(path, POST);
    }

    /**
     * Performs a PUT request with empty entity.
     *
     * @param path Path to PUT resource.
     * @throws IOException When error occurs during server I/O.
     */
    @SuppressWarnings("unused")
    public void doPut(final String path) throws IOException {
        request(path, PUT);
    }

    @SuppressWarnings("unused")
    public void doPut(final String path, final File f, final String mimeMediaType, final ResultProgressHandle progress) throws IOException {
        request(path, PUT, f, mimeMediaType, progress);
    }

    public Collection<Object> getValues(final String path, final String key)
            throws IOException, JSONException {
        final JSONValuesExtractor extractor = new JSONValuesExtractor(new LinkedHashSet<>(), key);
        doGet(path, extractor);
        return extractor.getValues();
    }

    public Map<String, String> getAliases(final String path, final String aliasKey, final String idKey) throws IOException, JSONException {
        final JSONAliasesExtractor extractor = new JSONAliasesExtractor(new LinkedHashMap<String, String>(), aliasKey, idKey);
        doGet(path, extractor);
        return extractor.getAliases();
    }

    public URL getURL() {
        return _base;
    }

    public JSESSIONIDCookie getJSESSIONIDCookie() {
        return _jsessionidCookie;
    }

    private PasswordAuthentication getPasswordAuthentication(final Service service, final URL url) {
        synchronized (passStore) {
            PasswordAuthentication auth = passStore.get(service);
            if (null == auth) {
                final String prompt = "Enter credentials for " +
                        (descriptions.containsKey(service) ? descriptions.get(service) : service);
                auth = Authenticator.requestPasswordAuthentication(url.getHost(),
                        null, url.getPort(), url.getProtocol(),
                        prompt, url.getProtocol());
                passStore.put(service, auth);            
            }
            return auth;
        }
    }

    private PasswordAuthentication getPasswordAuthentication(final URL url) {
        return getPasswordAuthentication(new Service(url), url);
    }

    /**
     * Adds an authorization-equivalent header to the provided request: a JSESSION cookie if
     * that's available, or Authorization: Basic otherwise.
     * @param connection The connection to which basic authentication should be added.
     * @return connection The initialized connection.
     */
    public URLConnection addAuthorization(final URLConnection connection) {
        if (StringUtils.isBlank(_jsessionidCookie.toString())) {
            addBasicAuthorization(connection, getPasswordAuthentication(connection.getURL()));
        } else {
            _jsessionidCookie.setInRequestHeader(connection);
        }
        return connection;
    }
}
