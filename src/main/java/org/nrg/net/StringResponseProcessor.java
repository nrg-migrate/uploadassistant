/*
 * org.nrg.net.StringResponseProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.netbeans.spi.wizard.ResultProgressHandle;
import org.nrg.IOUtils;
import org.nrg.upload.ui.ResultProgressListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringResponseProcessor implements HttpURLConnectionProcessor {
	private static final String CONTENT_TYPE_HEADER = "Content-Type";
	private static final int BUF_SIZE = 512;

	private final Logger logger = LoggerFactory.getLogger(StringResponseProcessor.class);
	private final InputStream in;
	private final String mimeMediaType;
	private final Integer contentLength;
	private final ResultProgressListener progress;
	private final StringBuilder sb = new StringBuilder();
	
	public StringResponseProcessor(final InputStream in, final String mimeMediaType, final Integer contentLength,
			final ResultProgressHandle progress) {
		this.in = in;
		this.mimeMediaType = mimeMediaType;
		this.contentLength = contentLength;
		if (null != contentLength && contentLength < 0) {
			throw new IllegalArgumentException("content length = " + contentLength +"; must be > 0");
		}
		this.progress = null == contentLength ? null : new ResultProgressListener(progress, 0, contentLength);
	}
	
	public StringResponseProcessor() {
		this(null, null, null, null);
	}

	/* (non-Javadoc)
	 * @see org.nrg.net.HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
	 */
	public void prepare(final HttpURLConnection connection) throws IOException {
		if (null == in) { return; }
		
		connection.setDoOutput(true);
		if (null != mimeMediaType) {
			connection.addRequestProperty(CONTENT_TYPE_HEADER, mimeMediaType);
		}
		if (null != contentLength) {
			connection.setFixedLengthStreamingMode(contentLength);
		}
		connection.connect();
		try {
			final OutputStream out = connection.getOutputStream();
			try {
				IOUtils.copy(out, in, progress);
				out.flush();
			} catch (Throwable t) {
				System.out.println("failure");
				t.printStackTrace();
				logger.debug("stream copy failed", t);
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
	}

	/* (non-Javadoc)
	 * @see org.nrg.net.HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
	 */
	public void process(final HttpURLConnection connection) throws IOException {
		final InputStream in = connection.getInputStream();
		try {
			final byte[] buf = new byte[BUF_SIZE];
			for (int n = in.read(buf); n > 0; n = in.read(buf)) {
				sb.append(new String(buf, 0, n));
			}
		} finally {
			in.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() { return sb.toString(); }
}
