/*
 * org.nrg.net.JSONRequestConnectionProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

public class JSONRequestConnectionProcessor implements HttpURLConnectionProcessor {
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String MEDIA_TYPE = "application/json";

    private final Logger logger = LoggerFactory.getLogger(JSONRequestConnectionProcessor.class);
    private final JSONObject request;
    private final ByteArrayOutputStream response = new ByteArrayOutputStream();

    public JSONRequestConnectionProcessor(final JSONObject request) {
        this.request = request;
    }

    /* (non-Javadoc)
     * @see org.nrg.net.HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
     */
    public void prepare(final HttpURLConnection connection) throws IOException {
        // TODO Auto-generated method stub
        connection.setDoOutput(true);
        connection.addRequestProperty(CONTENT_TYPE_HEADER, MEDIA_TYPE);

        connection.connect();
        final OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        try {
            request.write(writer);
        } catch (Throwable t) {
            System.out.println("failure");
            t.printStackTrace();
            logger.debug("stream copy failed", t);
        } finally {
            writer.close();
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.net.HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
     */
    public void process(HttpURLConnection connection) throws IOException {
        final InputStream in = connection.getInputStream();
        IOException ioexception = null;
        try {
            ByteStreams.copy(in, response);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                if (null == ioexception) {
                    throw e;
                } else {
                    logger.warn("Ignoring shadowed error closing HTTP response", e);
                    throw ioexception;
                }
            }
        }
    }
    
    /**
     * Returns the content of the response entity as a String
     * @return response entity content
     */
    public String getResponseEntity() {
        return new String(response.toByteArray(), Charset.defaultCharset());
    }
}
