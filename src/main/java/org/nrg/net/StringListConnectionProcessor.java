/*
 * org.nrg.net.StringListConnectionProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

public class StringListConnectionProcessor implements
HttpURLConnectionProcessor,Iterable<String> {
    private final List<String> items = Lists.newArrayList();

    /**
     * Get the Strings retrieved by this connection processor.
     * @return List of retrieved Strings.
     */
    @SuppressWarnings("unused")
    public List<String> getStrings() {
        return items;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    public Iterator<String> iterator() {
        return items.iterator();
    }

    /* (non-Javadoc)
     * @see org.nrg.net.HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
     */
    public void prepare(HttpURLConnection connection) {}

    /* (non-Javadoc)
     * @see org.nrg.net.HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
     */
    public void process(final HttpURLConnection connection) throws IOException {
        final InputStream in = connection.getInputStream();
        items.addAll(readStrings(in));
    }

    /**
     * Reads a list of newline-separated strings from the provided InputStream.
     * @param in InputStream from which strings will be read
     * @return A list of strings found in the input stream. Each line becomes a string.
     * @throws IOException When an error occurs reading from the input stream.
     */
    public static List<String> readStrings(final InputStream in) throws IOException {
        final List<String> items = Lists.newArrayList();
        final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        while ((line = reader.readLine()) != null) {
            if (StringUtils.isNotBlank(line)) {
                items.add(line.trim());
            }
        }
        return items;
    }
}
