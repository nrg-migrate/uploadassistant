/*
 * org.nrg.net.StreamUploadProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 12:40 PM
 */
package org.nrg.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.netbeans.spi.wizard.ResultProgressHandle;
import org.nrg.IOUtils;
import org.nrg.upload.ui.ResultProgressListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StreamUploadProcessor implements HttpURLConnectionProcessor {
	private static final String CONTENT_TYPE_HEADER = "Content-Type";

	private final Logger logger = LoggerFactory.getLogger(StreamUploadProcessor.class);
	private final InputStream in;
	private final String mimeMediaType;
	private final Integer contentLength;
	private final ResultProgressListener progress;
	
	public StreamUploadProcessor(final InputStream in, final String mimeMediaType, final Integer contentLength,
			final ResultProgressHandle progress) {
		this.in = in;
		this.mimeMediaType = mimeMediaType;
		this.contentLength = contentLength;
		if (null != contentLength && contentLength < 0) {
			throw new IllegalArgumentException("content length = " + contentLength +"; must be > 0");
		}
		this.progress = null == contentLength ? null : new ResultProgressListener(progress, 0, contentLength);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.net.HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
	 */
	public final void prepare(final HttpURLConnection connection)
	throws IOException {
		connection.setDoOutput(true);
		if (null != mimeMediaType) {
			connection.addRequestProperty(CONTENT_TYPE_HEADER, mimeMediaType);
		}
		if (null != contentLength) {
			connection.setFixedLengthStreamingMode(contentLength);
		}
		connection.connect();
		try {
			final OutputStream out = connection.getOutputStream();
			try {
				IOUtils.copy(out, in, progress);
				out.flush();
			} catch (Throwable t) {
				System.out.println("failure");
				t.printStackTrace();
				logger.debug("stream copy failed", t);
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.net.HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
	 */
	public final void process(final HttpURLConnection connection) {}
}
